import numpy as np  # 数値計算のためのパッケージ
import matplotlib.pyplot as plt  # 可視化を行うためのモジュール
from matplotlib.colors import ListedColormap  # カラーマップを作るためのクラス

def draw_boundary_map(df, model, scaler=None):
    """識別境界図の作成

    Args:
        df (DataFrame): 第１カラムを横軸、第２カラムを縦軸、targetカラムで散布図
        model (model): model.predict()が呼び出せるモデル
        scaler (scaler): scaler.transform()が呼び出せるスケーラ
    """
    X = df[df.columns[:2]].values
    y = df['target']
    
    markers = ('s', 'x', 'o')  # マーカーの準備
    colors = ('red', 'blue', 'lightgreen')  # 色の準備
    cmap = ListedColormap(colors[: len(np.unique(y))])  # カラーマップの色を設定
    
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1  # 横軸の範囲
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1  # 縦軸の範囲
    #xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, 0.02), np.arange(x2_min, x2_max, 0.02))  # 座標の作成
    xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, (x1_max-x1_min)/200), np.arange(x2_min, x2_max, (x2_max-x2_min)/200))  # 座標の作成
    
    points = np.array([xx1.ravel(), xx2.ravel()]).T
    if scaler is not None:
        points = scaler.transform(points)
        
    Z = model.predict(points)  # 各座標ごとの予測結果を格納
    Z = Z.reshape(xx1.shape)  # 予測結果を各座標の形に整形

    plt.contourf(xx1, xx2, Z, alpha=0.3, cmap=cmap)  # カラーマップを描画
    plt.xlim(xx1.min(), xx1.max())  # 横軸の描画範囲の設定
    plt.ylim(xx2.min(), xx2.max())  # 縦軸の描画範囲の設定

    # 予測値に対する散布図の描画
    for idx, cl in enumerate(np.unique(y)):
        plt.scatter(x=X[y == cl, 0], y=X[y == cl, 1],
                    alpha=0.8,
                    c=colors[idx],
                    marker=markers[idx],
                    label=cl,
                    edgecolors='black')

    plt.xlabel(df.columns[0])  # 横軸のラベル
    plt.ylabel(df.columns[1])  # 縦軸のラベル
    plt.legend(loc='upper right')  # 凡例の位置
    plt.show()  # グラフの表示

def draw_kmeans(X, model, centroids = None):
    color = ('red', 'blue', 'lightgreen')
    y = model.predict(X)
    cent = model.cluster_centers_
    if centroids is not None:
        cent = centroids
        
    plt.figure(figsize=(5,5))
    for idx, e in enumerate(np.unique(y)):
        plt.scatter(X[y==e, 0], X[y==e, 1], c=color[idx], label=f'cluster_{idx}')
    
    for idx, e in enumerate(cent):
        plt.scatter(e[0], e[1], c=color[idx], marker='x', s=100, label=f'centroid_{idx}')
    plt.legend(loc='upper left', bbox_to_anchor=(1, 1))    
    # plt.xlabel(cols[0])
    # plt.ylabel(cols[1])
    plt.show()