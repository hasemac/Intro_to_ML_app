# Correlation coefficient

## About Correlation Coefficients

A correlation coefficient is a coefficient that expresses the relationship between two variables.
The strength of the correlation is expressed as a number between 1 and -1.
When one increases, the other also increases, it is said that there is a positive correlation.
When one increases and the other decreases, there is said to be a negative correlation.

- Summer temperature and number of shaved ice sold. (positive correlation)
- Winter temperature and oil sales. (negative correlation)

![correlation](./images/correlation_coefficient.drawio.svg)

The correlation coefficient is given in the following formula:

Correlation coefficient $r_{xy}$

```math
r_{xy} = \frac{S_{xy}}{S_x S_y}
```

Covariance $S_{xy}$ of $x$ and $y$

```math
S_{xy} = \frac{1}{n} \sum_{k=1}^{n} (x_k - \bar{x})(y_k - \bar{y})
```

$S_x$: standard deviation of $x$  
$S_y$: standard deviation of $y$

## Correlation coefficient by Numpy

(以降のデータは、[文部科学省 パブリックユースデータ](https://www.mext.go.jp/a_menu/shotou/gakuryoku-chousa以降のnota/1404609.htm)を用いています。)

Read the csv data of the test results and extract the data from the '正答数_国A' and '正答数_算A' columns.

'正答数'：number of correct answers  
'国A', '算A': subject name

```python
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('z1_mxt_jidou.csv', encoding='shift-jis')
correct_kokua = df['正答数_国A'].values
correct_sana = df['正答数_算A'].values
```

Let's display it as a scatter plot.

```python
plt.scatter(correct_kokua, correct_sana)
plt.xlabel('正答数_国A')
plt.ylabel('正答数_算A')
plt.show()
```

![correlation](./images/correlation_color.png)

Calculate the correlation coefficient.

```python
np.corrcoef(correct_kokua, correct_sana)
```

```python
array([[1.        , 0.54001709],
       [0.54001709, 1.        ]])
```

The correlation coefficient for the previous scatter plot was 0.54.

Correlation coefficients can also be obtained for multiple sets of data at once.  

- '正答数_国A'
- '正答数_算A'
- '正答数_理'

For the three sets of data above, we can also find the correlation coefficients for each, collectively.

```python
correct_ri = df['正答数_理'].values
np.corrcoef([correct_kokua, correct_sana, correct_ri])
```

## Correlation coefficient by Pandas

Pandas can also calculate the correlation coefficient.
Let's calculate each correlation coefficient for the next column.

- '正答数_国A'
- '正答数_国B'
- '正答数_算A'
- '正答数_算B'
- '正答数_理'
- '正答数_理A'
- '正答数_理B'

Extract the prescribed column and create a new DataFrame, df0.

```python
cn = ['正答数_国A',
 '正答数_国B',
 '正答数_算A',
 '正答数_算B',
 '正答数_理',
 '正答数_理A',
 '正答数_理B',]
df0 = df[cn]
```

Calculate the correlation coefficient of each column.

```python
df0.corr()
```

```python
	正答数_国A	正答数_国B	正答数_算A	正答数_算B	正答数_理	正答数_理A	正答数_理B
正答数_国A	1.000000	0.565331	0.540017	0.517687	0.548927	0.427883	0.515439
正答数_国B	0.565331	1.000000	0.540053	0.518414	0.585527	0.434481	0.562931
正答数_算A	0.540017	0.540053	1.000000	0.575524	0.613008	0.460417	0.586033
正答数_算B	0.517687	0.518414	0.575524	1.000000	0.610296	0.459502	0.582769
正答数_理	0.548927	0.585527	0.613008	0.610296	1.000000	0.792719	0.931075
正答数_理A	0.427883	0.434481	0.460417	0.459502	0.792719	1.000000	0.515687
正答数_理B	0.515439	0.562931	0.586033	0.582769	0.931075	0.515687	1.000000
```

Let's visualize each relationship using Python's visualization library Seaborn.

```python
import seaborn as sns
df_corr = df0.corr()
sns.heatmap(df_corr, vmax=1, vmin=-1, center=0)
```

![heatmap](./images/heatmap.png)