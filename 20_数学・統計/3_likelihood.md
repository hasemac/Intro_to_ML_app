# Likelihood and Maximum Likelihood Estimation

**Maximum Likelihood Estimation** is the estimation of the probability (or parameters of a probability distribution) that the data will follow from given data.

For example, if a coin is tossed six times with a probability of $\theta$ (not necessarily 0.5) that the result is "front, back, front, front, back, front", then the probability $L(\theta)$ of this event occurring is obtained by the following formula.

```math
\begin{align}
L(\theta) &= \theta (1-\theta) \theta \theta (1-\theta) \theta \notag \\
&= \theta^4 (1-\theta)^2 \notag
\end{align}
```

The data given here are "front, back, front, front, back, front", and estimating the probability means finding a plausible $\theta$.  

$L(\theta)$ is called the **likelihood function**.

When $\theta = 1/2$, $L(1/2)=0.0156 \cdots$  
When $\theta = 1/3$, $L(1/3)=0.0054 \cdots$

The value $L$ is called the **likelihood**, and since $L(1/2) > L(1/3)$, $\theta=1/2$ is more plausible than $\theta=1/3$.

Maximum likelihood estimation is to find the $\theta$ that maximizes $L(\theta)$ and use that $\theta$ as the estimated value.  
Differentiate $L(\theta)$ by $\theta$ to find the $\theta$ where $L$ is maximal.  

```math
\frac{d L(\theta)}{d \theta}= 2 \theta^3 (\theta - 1)(3\theta - 2)
```

From this, $\theta=2/3$ is the **maximum likelihood estimator** and $L(\theta)$ is the maximum value, which is $L(2/3)=0.0219 \cdots$.
($L$ is zero when $\theta=0,1$.)

Since the given data was "front, back, front, front, back, front" a total of 4 out of 6 times, the probability can also be $\theta = 2/3\:(= 4/6)$.

## log-likelihood function

When the likelihood is obtained by repeating independent trials, the likelihood function $L(\theta)$ takes the form of a product of probabilities. Since it is mathematically inconvenient to take this derivative, we often use the **log-likelihood function**, which is logarithmic to $\log L(\theta)$.

Define the log-likelihood function.

```math
\log L(\theta) = 4 \log \theta + 2 \log (1-\theta)
```

Differentiate this.

```math
\frac{d \log L(\theta)}{d \theta} = \frac{4}{\theta} - \frac{2}{1-\theta} 
=2 \frac{(2-3 \theta)}{\theta (1-\theta)}
```

Taking logarithms makes it easier to calculate the derivative, and we can see that we get the same value as $\theta = 2/3$.

Here it was an optimization problem to find $\theta$ that maximizes $\log L(\theta)$, but in many cases optimization problems are minimization problems. Therefore, in most cases, a **negative log-likelihood function** multiplied by a minus sign is used to make it a minimization problem.

negative log-likelihood function：$-\log L(\theta)$