# Statistics

(以降のデータは、[文部科学省 パブリックユースデータ](https://www.mext.go.jp/a_menu/shotou/gakuryoku-chousa以降のnota/1404609.htm)を用いています。)

It is difficult to grasp the whole picture even if you just look at a lot of data. So we try to get a complete picture by summarizing the data into representative values. This value is called a summary statistic.  
Here, we will actually calculate some statistics using Python.

First, load the child's test result data into Pandas and extract the number of correct answers for Japanese A.  

```python
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('z1_mxt_jidou.csv', encoding='shift-jis')
correct_answers = df['正答数_国A'].values
print("データ数：", correct_answers.size)
```

Displays the number of correct answers for Japanese A on a histogram.  

```python
plt.xlabel('正答数')
plt.ylabel('度数')
plt.hist(correct_answers, bins=np.arange(1.5,17.5,1))
plt.show()
```

![ヒストグラム](./images/hist.png)

From this histogram, predict what the next value will be.

- Maximum value
- Minimum value
- Total number of correct answers
- Average value

  ```math
  \bar{x}=\frac{x_{1} + x_{2} + \cdots + x_{n}}{n}
  ```

- Median  
  When the values ​​are arranged in ascending order, it is the value that is exactly in the middle.

- Standard deviation

  ```math
  S = \sqrt{\frac{
    (x_{1}-\bar{x})^{2}+(x_{2}-\bar{x})^{2}+ \cdots +(x_{n}-\bar{x})^{2}}
    {n}}  
  ```

- Variance  
  square of standard deviation：$S^{2}$  
- Mode  
  This is the value that appears most frequently.

Let's actually calculate using numpy.  

```python
print('最大値: ', np.max(correct_answers))
print('最小値: ', np.min(correct_answers))
print('総正答数: ', np.sum(correct_answers))
print('平均値: ', np.mean(correct_answers))
print('平均値: ', np.average(correct_answers)) # 加重平均の計算が可能
print('中央値: ', np.median(correct_answers))
print('標準偏差: ', np.std(correct_answers)) # standard deviation
print('分散: ', np.var(correct_answers)) # variance
# 最頻値の算出
unique, freq = np.unique(correct_answers, return_counts=True)
mode = unique[np.argmax(freq)]
print('最頻値：', mode)
```

- Percentiles and Quartiles  
  **Percentile** represents the percentage of a given piece of data when it is arranged in ascending order. So, for example, it would be as follows.  
  Minimum: 0 percentiles  
  Median：50 percentiles  
  Maximum：100 percentiles  
  
  **Quartile** is the three dividing points when dividing the data into four. Therefore, it becomes as follows.  
  1st quartile：25 percentiles  
  2nd quartile：50 percentiles (Median)  
  3rd quartile：75 percentiles  

  ![percentile](./images/percentile.drawio.svg)

```python
# パーセンタイル
print('0 percentile：', np.percentile(correct_answers, 0))
print('50 percentile：', np.percentile(correct_answers, 50))
print('100 percentile：', np.percentile(correct_answers, 100))
# 四分位数
print('1st quartile：', np.percentile(correct_answers, 25))
print('2nd quartile：', np.percentile(correct_answers, 50))
print('3rd quartile：', np.percentile(correct_answers, 75))
```

Question:
The test deviation score is given by the following formula:
Find the deviation score when the number of correct answers is 14.

（deviation score）=（Personal score - Average score） ÷ （standard deviation）× 10 + 50