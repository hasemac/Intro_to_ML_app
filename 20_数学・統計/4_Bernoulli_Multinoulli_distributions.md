# Bernoulli and Multinoulli distributions

内容：

- [ベルヌーイ分布とマルチヌーイ分布](#ベルヌーイ分布とマルチヌーイ分布)
  - [ベルヌーイ分布](#ベルヌーイ分布)
    - [対数尤度関数](#対数尤度関数)
  - [二項分布](#二項分布)
  - [マルチヌーイ分布](#マルチヌーイ分布)

## Bernoulli distribution

"The Bernoulli distribution is a discrete probability distribution in mathematics that takes 1 with probability p and 0 with probability q = 1 - p." ([Wikipedia](https://ja.wikipedia.org/wiki/%E3%83%99%E3%83%AB%E3%83%8C%E3%83%BC%E3%82%A4%E5%88%86%E5%B8%83))

If $p$ is the probability of a coin toss yielding the front (1), the probability of the back (0) is given by $1-p$.

```math
P(X=1)=p, \: P(X=0)=1-p, \: 0 \leq p \leq 1 
```

In summary, the following equation is obtained. In this case, we can see that the equation is indeed consistent with the above equation.

```math
P(X=x) = p^x (1-p)^{1-x}
```

Note, $x=0, 1$.
This $P(X=x)$ is called the **Bernoulli distribution**.

Expected value：$E(X) = 1 \cdot p+0 \cdot (1-p)=p$
Variance：$V(X)=E(X^2)-E(X)^2=1^2 \cdot p+0^2 \cdot (1-p) - p^2=p(1-p)$

### Log-likelihood function

In particular, the probability of having n data $x_1, x_2, \cdots , x_n$ and $X=\boldsymbol{x}$ $(=x_1, x_2, \cdots , x_n)$ is obtained by the following formula

```math
P(X=\boldsymbol{x})= \prod_{x_i} p^{x_i} (1-p)^{1-x_i}
```

Note, $x_i=0, 1$です。
The log-likelihood function $-\log P$ then becomes

```math
-\log P = - \sum_i \left( x_i \log p +(1-x_i) \log (1-p) \right)
```

Note, $x_i=0, 1$.

## binomial distribution

One attempt at a coin toss is the Bernoulli distribution, but $n$ attempts is the **binomial distribution**.
If the number of times the front (1) comes out is $x$ times after $n$ trials, the number of times the back (0) comes out is $n-x$ times. The probability function for $x$ times is given by

```math
P(X=x) = {}_n C_x p^x(1-p)^{n-x}
```

Expected value：

```math
\begin{align}
E(X) &= \sum_{x=0}^n x \cdot {}_n C_x p^x(1-p)^{n-x} \notag \\
&= \sum_{x=1}^n \frac{n!}{(x-1)!(n-x)!} p^x(1-p)^{n-x} \notag \\
\end{align}
```

Here, if $l=x-1$, we obtain the following equation.  

```math
\begin{align}
E(X) &= \sum_{l=0}^{n-1} \frac{n (n-1)!}{l!((n-1)-l)!} p \cdot p^l(1-p)^{(n-1)-l} \notag \\
&= n p \sum_{l=0}^{n-1} {}_{n-1} C_l p^l(1-p)^{(n-1)-l} \notag \\
&= np (p+(1-p))^{n-1} \notag \\
&= np \notag \\
\end{align}
```

Expected value：$E(X) = np$
Variance：$V(X)=n p (1-p)$

Question：
Explain that the variance is $V(X)=n p (1-p)$.

## Martinoulli distribution

The binomial distribution deals with two events, 0 or 1, while the **Martinoulli distribution** (also called **multinomial distribution**) deals with $k$ events. Suppose you have $n$ trials.

Probability of the $i$-th event: $p_i$.  
Number of times the $i$-th event occurs: $X_i$.

The following equation then holds
$p_1 + p_2 + \cdots + p_k = 1$, 
$X_1 + X_2 + \cdots + X_k = n$

Define $\boldsymbol{X} = (X_1, X_2, \cdots , X_k)$, $\boldsymbol{x} = (x_1, x_2, \cdots, x_k)$.

Letting $P(\boldsymbol{X} = \boldsymbol{x})$ denote the probability that each event occurs $x_1, x_2, \cdots, x_k$ times, the following equation is given

```math
P(\boldsymbol{X} = \boldsymbol{x}) = 
\frac{n!}{x_1! x_2! \cdots x_k!} p_1^{x_1} p_2^{x_2} \cdots p_k^{x_k}
```

Expected value：$E(X_i) = np_i$
Variance：$V(X_i)=n p_i (1-p_i)$