# Logistic regression gradient

In logistic regression, we want to find the weight $w_i$ so that the probability $p$ of a prediction with the correct label is high. To find this $w_i$, we need to find the slope of $w_i$ in the evaluation function.

## Negative log-likelihood function

Assuming $p$ as the probability that $t$ is 1, when there are n data $t_1, t_2, \cdots , t_n$ ($t_i=0, 1$), the probability that $X=\boldsymbol{t}$ $(=(t_1, t_2, \cdots , t_n))$ is obtained as in Bernoulli distribution with the following formula: 

```math
P(X=\boldsymbol{t})= \prod_{i} p_i^{t_i} (1-p_i)^{1-t_i}
```

The negative log-likelihood function $L=-\log P$ then becomes

```math
L = -\log P = - \sum_i \left( t_i \log p_i +(1-t_i) \log (1-p_i) \right)
```

Note: $t_i=0, 1$です。  

Finding a plausible $p$ here means finding the weight that maximizes $P(X=\boldsymbol{t})$ or minimizes $L$.

## Calculation of gradient

In logistic regression, $p$ is a sigmoid function, as follows

```math
p_i=\frac{1}{1+e^{-z_i}}
```

There are m features $x_1, x_2, \cdots , x_m$ and $z_i=w_0+x_{i1}w_1 + x_{i2} w_2 + \cdots + x_{im} w_m$.  
The subscript $i$ in $x_{ij}$ means the $i$th data, and the subscript $j$ means the $j$th feature.  
To find the $w_i$ that minimizes $L(w_i)$, we only need to know its slope $-\partial L/\partial w_i$. Since $L$ is a function of $z$ and $z$ is a function of $w_i$, the following equation holds.

```math
\frac{\partial L}{\partial w_i} = \sum_k \frac{\partial L}{\partial z_k} \frac{\partial z_k}{\partial w_i}
```

Compute the $\partial L/\partial z_k$ term.

```math
\begin{align}
\frac{\partial L}{\partial z_k} 
&= -\frac{\partial}{\partial z_k} \sum_i \left( t_i \log p_i +(1-t_i) \log (1-p_i) \right) \notag \\
&= - \frac{\partial}{\partial z_k} \left( t_k \log p_k +(1-t_k) \log (1-p_k) \right) \notag \\

&= - \left( \frac{t_k}{p_k} \frac{d p_k}{dz_k} -\frac{1-t_k}{1-p_k} \frac{dp_k}{dz_k} \right) \notag
\end{align}
```

The second equation shows that it is zero when $i \neq k$.
Since $p$ is a sigmoid function and $dp/dz=p(1-p)$ holds, we can transform the expression as follows.

```math
\begin{align}
\frac{\partial L}{\partial z_k} 
&= -(t_k (1-p_k)-(1-t_k)p_k) \notag \\
&= -(t_k-p_k) \notag \\ 
\end{align}
```

The following equation also holds

```math
\frac{\partial z_k}{\partial w_i} = x_{ki}
```

Eventually, the following equation was obtained to find the gradient

```math
-\frac{\partial L}{\partial w_i} = \sum_k (t_k-p_k) x_{ki}
```

If we can find the gradient, we can find the weight $w_i$ that minimizes the negative log-likelihood function.  

Question:
Show that the sigmoid function $p$ satisfies $dp/dz=p(1-p)$.

```math
p=\frac{1}{1+e^{-z}}
```
