# Decision Tree Analysis

This section introduces Decision Tree Analysis (DTA), one of the supervised algorithms.

## About Decision Tree Analysis

Decision tree analysis is an algorithm that classifies data by asking a question about a single feature and repeatedly branching off based on whether or not the condition is satisfied.  

An example of a decision tree is when a person is classified as coming or not coming to the golf course based on the weather, humidity, and wind conditions of the day.([wikipedia](https://ja.wikipedia.org/wiki/%E6%B1%BA%E5%AE%9A%E6%9C%A8))

![golf](./images/decision_tree_golf.png)

The part of the tree that asks the question is called the **node** (rounded square or rectangle in the figure), and the part that represents the answer at the end is called the **terminal node** ( rectangle in the figure).  
The decision tree will find conditionals for the features in such a way as to maximally reduce **impurity** within the node.  
**Gini impurity** is often used as an indicator of impurity.  

The Gini impurity at node $t$ as class number $c$ is defined by the following equation

```math
I_G(t)=\sum_{i=1}^{c}p(i|t)(1-p(i|t))=1-\sum_{i=1}^{c}p(i|t)^2
```

$p(i|t)$: Percentage of samples belonging to class $i$ at node $t$.

Zero impurity: Only one type of data in the node  
High impurity: Multiple types of data in a node

Classification:

1. calculate the impurity of the parent node
2. compute the impurity of the two child nodes
3. compute the weighted average of the impurity of the two child nodes by the number of data
4. Calculate the information gain [(impurity of parent node) - (weighted average of child nodes)

The classification is made so that the information gain is greater (lower impurity of the child node).

### Advantages and disadvantages of decision trees

Advantages

- The importance of features can be checked as numerical values, making interpretation easy.
  （The importance of a feature can be checked with the "feature_importances_" of the classifier, which makes the interpretation easy.)
- No need for normalization as data preprocessing, since each feature is a branch.

Disadvantages.

- Not very good at linearly separable problems.
  (Cannot draw diagonal boundaries on boundary maps.)
- Prone to over-fitting

The depth of the decision tree may be specified to prevent over-fitting.
(Can be specified as "DecisionTreeClassifier (max_depth=2)" etc.)

## Classify iris species using decision tree

We consider the classification of iris species using a decision tree.
Here we use sklearn's [DecisionTreeClassifier](https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html).

Import the module, and obtain the features and targets.
Here, the two below are used as features.

- petal width (cm)
- petal length (cm)

```python
from sklearn import datasets
import pandas as pd
import sub
from sklearn.tree import DecisionTreeClassifier # 決定木分析を行うためのクラス

# irisのデータセットを取得
iris = datasets.load_iris() # irisのデータを辞書型で取得
# 辞書型の変数からDataFrameを作成
df = pd.DataFrame(data = iris['data'], columns=iris['feature_names'])
df['target'] = iris['target']

# 特徴量とターゲットを取得
features = ['petal width (cm)', 'petal length (cm)']
X = df[features].values
y = df['target']
```

Learning by decision tree.  

```python
# 決定木を用いて学習
dtc = DecisionTreeClassifier(max_depth=2, random_state=0)
dtc = dtc.fit(X, y)
```

Check the classification results.

```python
# 分類結果の確認
y_pred = dtc.predict(X) # 分類結果
judge = y_pred-y.values
print("正解した個数：", len(judge[judge == 0]))
print('正解率：', len(judge[judge == 0])/len(judge))
print('正解率：', dtc.score(X,y)) # score()を使っても計算できます。
```

Draw the boundary diagram.

```python
# 識別境界の図描画
df1 = df[features +  ['target']]
sub.draw_boundary_map(df1, dtc)
```

![decision_tree](./images/decision_tree_boundary.png)

The package [Graphiz](https://graphviz.org/) can be used to visualize the data as follows.
To use Graphiz, you need to install the "pydotplus" module for Python and download and set up Graphviz from the official website.  

![decision_tree](./images/decision_tree.png)

### Question

Try to draw the discriminant boundary when the depth of the decision tree is increased to 3, 4, 5, ... and so on.  
You will see that the classification conditions become more complex.  
