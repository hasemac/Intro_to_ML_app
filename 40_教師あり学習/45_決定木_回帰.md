# 決定木分析

## 決定木分析(回帰)

決定木分析は回帰にも使えます。
sklearnの[糖尿病データセット](https://scikit-learn.org/stable/datasets/toy_dataset.html#diabetes-dataset)を用いて、一年後の疾患進行の定量的尺度を予測してみます。
説明によれば、特徴量などは次の通りです。

ターゲット

- a quantitative measure of disease progression one year after baseline
  (一年後の疾患進行の定量的尺度)

特徴量

- age: age in years
- sex
- bmi: body mass index
- bp: average blood pressure
- s1 tc: total serum cholesterol
- s2 ldl: low-density lipoproteins
- s3 hdl: high-density lipoproteins
- s4 tch: total cholesterol / HDL
- s5 ltg: possibly log of serum triglycerides level
- s6 glu: blood sugar level

ここではsklearnの[DecisionTreeRegressor](https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeRegressor.html)を用います。

まず、モジュールをインポートして、特徴量とターゲットを取得します。

```python
from sklearn import datasets
import pandas as pd
from sklearn.tree import DecisionTreeRegressor # 決定木分析(回帰)を行うためのクラス
from sklearn.model_selection import train_test_split

# diabetes
diabetes = datasets.load_diabetes()
# 辞書型の変数からDataFrameを作成
df = pd.DataFrame(data = diabetes['data'], columns=diabetes['feature_names'])
df['target'] = diabetes['target']

features = df.columns[:-1] # 最後のカラム以外を特徴量とする
# 特徴量とターゲットを取得
X = df[features].values 
y = df['target']
```

今回は訓練データとテストデータに分離します。  
8割の訓練データと２割のテストデータです。

```python
# 訓練データをテスト(検証)データに分割
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)
```

学習します。決定木の深さは２にしています。

```python
# 決定木を用いて学習
DTR = DecisionTreeRegressor(max_depth=2, random_state=0)
DTR = DTR.fit(X_train, y_train)
```

可視化します。

![dtr](./images/decision_tree_reg.png)

これによれば、訓練データにおいて、まずbmiの値で分類され、その後、それぞれs5の値、blood pressure (bp) の値に応じて分類されていること分かります。

次に訓練データとテストデータのそれぞれに対して、RMSEおよびR^2を算出して性能評価を行います。

```python
from sklearn.metrics import r2_score, mean_squared_error
y_train_pred = DTR.predict(X_train) # 訓練データの予測値
y_test_pred = DTR.predict(X_test) # テストデータの予測値

# 平均平方二乗誤差(RMSE)
rmse_train = mean_squared_error(y_train, y_train_pred)
rmse_test = mean_squared_error(y_test, y_test_pred)
print(f'RMSE 訓練: {rmse_train:.2f}, テスト： {rmse_test:.2f}')

# 決定係数(R^2)
r2_train = r2_score(y_train, y_train_pred)
r2_test = r2_score(y_test, y_test_pred)
print(f'R^2 訓練：{r2_train:.4f}, テスト：{r2_test:.4f}')
```

### 問題

決定木の深さをmax_depth=2, 3, 4, ・・と深くしていったときに、性能評価は良くなっていくか悪くなっていくかを確認してください。

