# Support Vector Machine

内容：

- [Support Vector Machine](#support-vector-machine)
  - [Linear Support Vector Machine](#linear-support-vector-machine)
  - [SVM with linear kernel](#svm-with-linear-kernel)
  - [Support vector machine (kernel method)](#support-vector-machine-kernel-method)
  - [With other kernels](#with-other-kernels)
    - [polynomial kernel](#polynomial-kernel)
    - [RBF kernel](#rbf-kernel)
    - [sigmoid kernel](#sigmoid-kernel)

## Linear Support Vector Machine

When considering the classification of different types of data by a straight line (or hyperplane in high-dimensional space) boundary, a linear support vector machine is one that determines the boundary in such a way as to maximize the margin. The margin is the distance between the data closest to the boundary line and the boundary line.

![svm_margin](./images/svm_margin.drawio.svg)

The boundary is determined as shown in the figure on the right so that the margin is the largest.

"The feature that differentiates SVM from other algorithms is that it does not just end up looking for a hyperplane that separates some points, but it looks for the maximum-margin hyperplane among many candidate planes that can separate some points."([Wikipedia](https://ja.wikipedia.org/wiki/%E3%82%B5%E3%83%9D%E3%83%BC%E3%83%88%E3%83%99%E3%82%AF%E3%82%BF%E3%83%BC%E3%83%9E%E3%82%B7%E3%83%B3))

For a detailed description of support vector machines, please refer to [Support Vector Machines](https://scikit-learn.org/stable/modules/svm.html) on scikit-learn or [Support Vector Machines](https://ja.wikipedia.org/wiki/%E3%82%B5%E3%83%9D%E3%83%BC%E3%83%88%E3%83%99%E3%82%AF%E3%82%BF%E3%83%BC%E3%83%9E%E3%82%B7%E3%83%B3) on wikipedia.

## SVM with linear kernel

Use [SVC](https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html) in scikit-learn.

Retrieve the wine dataset.

```python
from sklearn import datasets
import pandas as pd

# wine
wine = datasets.load_wine() # wineのデータを辞書型で取得
# 辞書型の変数からDataFrameを作成
df_original = pd.DataFrame(data = wine['data'], columns=wine['feature_names'])
df_original['target'] = wine['target']
```

The features and targets are obtained by setting the two features of interest as `proline` and `hue`.  

```python
# 特徴量とターゲットの設定
#features = ['alcohol', 'malic_acid']
features = ['proline', 'hue']

df = df_original[features+['target']]
X = df[features].values # 特徴量の設定
y = df['target'] # ターゲットの設定
```

Split data into training and validation data according to the ratio of test_size.  

```python
# 訓練データと検証データに分割
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0, test_size=0.2)
```

Standardize the data.  
Note that the statistics of the training data are used to standardize the data for validation.  

```python
# データの標準化 （平均値ゼロ、標準偏差１）
from sklearn.preprocessing import StandardScaler
SS = StandardScaler() # インスタンスを作成
SS.fit(X_train) # 訓練データの統計情報をセット
X_train_ss = SS.transform(X_train) # 訓練データの標準化
X_test_ss = SS.transform(X_test) # 検証用データの標準化
```

It is trained with `linear` kernels.  

```python
# 学習
from sklearn.svm import SVC # サポートベクトルマシンを用いるためのクラス
SVM = SVC(kernel='linear', random_state=0)
SVM.fit(X_train_ss, y_train)
y_train_predict = SVM.predict(X_train_ss)
```

Create a DataFrame with training data and draw a boundary diagram.  

```python
# 境界線図の描画
import sub
# 訓練データのデータフレームを作成
df_train = pd.DataFrame(X_train, columns=features)
df_train['target'] = y_train
sub.draw_boundary_map(df, SVM, SS)
```

![svm_linear](./images/svm_linear.png)

Find the percentage of correct answers for each of the training and validation data.  

```python
# 正解率
print('訓練データの正解率: ', SVM.score(X_train_ss, y_train))
print('検証データの正解率: ', SVM.score(X_test_ss, y_test))
```

## Support vector machine (kernel method)

The kernel method is "to transfer data to another feature space so that linear regression can be performed.  
By using the kernel method, even data that cannot be linearly separated can be separated by the support vector machine.  

"Kernel functions provide a means** to compute the inner product in feature space directly from the data, without going through an explicit computation of the coordinates of the data in feature space. Using kernel functions to evaluate inner products is often less computationally intensive than going through an explicit computation of coordinates.
The approach of using kernel functions to extend inner-product based analysis methods to higher dimensional feature spaces without increasing computational complexity is commonly referred to as the **kernel trick**."(wikipedia[kernel method](https://ja.wikipedia.org/wiki/%E3%82%AB%E3%83%BC%E3%83%8D%E3%83%AB%E6%B3%95))

[![kernel](https://upload.wikimedia.org/wikipedia/commons/f/fe/Kernel_Machine.svg)](https://upload.wikimedia.org/wikipedia/commons/f/fe/Kernel_Machine.svg)

Kernel machines are used to compute non-linearly separable functions into a higher dimension linearly separable function. ([wikipedia](https://ja.wikipedia.org/wiki/%E3%82%AB%E3%83%BC%E3%83%8D%E3%83%AB%E6%B3%95))

## With other kernels

A description of kernels that can be used with scikit-learn can be found in [Kernel functions](https://scikit-learn.org/stable/modules/svm.html#kernel-functions).

- linear: $<x, x'>$
- polynomial: $(\gamma <x, x'>+r)^d$, where $d$ is specified by parameter `degree`, $r$ by `coef0`.
- rbf: $exp(-\gamma ||x-x'||^2)$, where $\gamma$ is specified by parameter `gamma`, must be greater than 0.
- sigmoid: $tanh(\gamma <x, x'>+r)$, where $r$ is specified by `coef0`.

### polynomial kernel

```python
# 多項カーネル
SVM = SVC(kernel='poly', random_state=0) # デフォルトは3次の次数 (degree)
SVM.fit(X_train_ss, y_train)
sub.draw_boundary_map(df, SVM, SS)
```

![svm_poly](./images/svm_poly.png)

### RBF kernel

RBF (Radial Basis Function)

```python
# RBFカーネル
SVM = SVC(kernel='rbf', random_state=0)
SVM.fit(X_train_ss, y_train)
sub.draw_boundary_map(df, SVM, SS)
```

![svm_rbf](./images/svm_rbf.png)

### sigmoid kernel

```python
# シグモイドカーネル
SVM = SVC(kernel='sigmoid', random_state=0)
SVM.fit(X_train_ss, y_train)
sub.draw_boundary_map(df, SVM, SS)
```

![svm_rbf](./images/svm_sigmoid.png)
