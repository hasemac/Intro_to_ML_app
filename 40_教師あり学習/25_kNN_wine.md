# k-nearest neighbor method

In the k-nearest neighbor method, distances are calculated to search for nearby points.  
When the scales of the feature data are very different, the appropriate distance may not be calculated.  
In this section, we will try to classify wines in the following two cases using two features of the wine dataset, 'proline' and 'hue'.  

- Without Standardization
- With Standardization

## Obtain a dataset of wine and check for scale

```python
from sklearn import datasets
import pandas as pd

# wine
wine = datasets.load_wine() # wineのデータを辞書型で取得
# 辞書型の変数からDataFrameを作成
df = pd.DataFrame(data = wine['data'], columns=wine['feature_names'])
df['target'] = wine['target']
```

```python
# 最小値と最大値の確認
import numpy as np
print('proline: ', np.min(df['proline']), np.max(df['proline']))
print('hue: ', np.min(df['hue']), np.max(df['hue']))
```

The two features are found to be approximately in the following ranges.  
'proline': 300～1700
'hue': 0.5～1.7

## Without Standardization

```python
# 特徴量とターゲットを取得
#features = ['alcohol', 'hue']
features = ['proline', 'hue']
# 新しいデータフレーム作成
dfn = df[features+["target"]]

X = dfn[features].values
y = dfn['target']
```

```python
from sklearn.neighbors import KNeighborsClassifier # k近傍法のクラス

# 学習
kNC = KNeighborsClassifier(n_neighbors=3) # 近傍点を3個にしてインスタンス作成
kNC.fit(X, y) # k近傍法の学習

y_pred = kNC.predict(X) # 予測の取得
pd.DataFrame({'y':y, 'y_pred':y_pred})[::20] # 予測値の確認
```

```python
# 標準化しない場合の正解率
kNC.score(X, y)
```

The percentage of correct answers without standardization was 0.85.  
Draw the boundary diagram.  

```python
import sub 
# 境界線図を描画
sub.draw_boundary_map(dfn, kNC)
```

![kNN_wind](./images/kNN_wine.png)

It can be seen that even if there is a variation in 'hue', almost no change occurs in the classification.  

## With Standardization

```python
# 標準化
from sklearn.preprocessing import StandardScaler

SS = StandardScaler() # インスタンスを作成
X_ss = SS.fit_transform(X) # データXを標準化

# 学習
kNC_ss = KNeighborsClassifier(n_neighbors=3) # 近傍点を3個にしてインスタンス作成
kNC_ss.fit(X_ss, y) # k近傍法の学習

y_ss_pred = kNC_ss.predict(X_ss) # 予測の取得
```

```python
# 標準化した場合の正解率
kNC_ss.score(X_ss, y)
```

The percentage of correct answers is 0.92, indicating an improvement.  
Let's draw a boundary diagram.  

```python
# 標準化したデータに対してデータフレーム作成
dfn_ss = pd.DataFrame(X_ss, columns=['proline_ss', 'hue_ss'])
dfn_ss['target'] = dfn['target']
# 境界線図を描画
sub.draw_boundary_map(dfn_ss, kNC_ss)
```

![kNN_wine_ss](./images/kNN_wine_ss.png)

We can see that the values of proline and hue are adjusted so that the mean value is zero and the standard deviation is 1.  
We can also see that when there is a change in hue, the class to be classified also changes.  

To check, re-scale to the original scale and draw a boundary diagram.  

```python
# 逆変換したデータに対して境界線図を描画
sub.draw_boundary_map(dfn, kNC_ss, scaler=SS)
```

![kNN_ss_inv](./images/kNN_wine_ss_inv.png)