# Logistic regression

Although logistic regression has the word "regression" in it, it is actually applied to classification problems.

## Algorithm

For n features $x_1, x_2, \cdots , x_n$, the probability $p$ of being classified as class A or B is given by the following equation when considering 2-class classification:

```math
p=\frac{1}{1+e^{-z}}
```

Note: $z=w_0+w_1 x_1+w_2 x_2+ \cdots + w_n x_n$

In learning, the weights $w_i$ are found so that the probability $p$ of prediction with the correct label is high. (See the following file for how to obtain the weights.)  
The expression for the probability $p$ is called the **sigmoid function**.  
Since the value of the sigmoid function ranges from 0 to 1, it can be understood as a probability.

![sigmoid](./images/sigmoid.png)

For multi-valued classifications of two or more values, use the **softmax function**.  
The probability $p_i$ of being classified into class $i$ is given by  

```math
p_i = \frac{e^{z_i}}{\sum_{k=1}^{n}e^{z_k}}
```

Note: $z_k=w_{k0}+w_{k1} x_1+w_{k2} x_2+ \cdots + w_{kn} x_n$

Taking the sum of the probabilities for all classes, we find that the sum is 1.  

## Iris Classification

For the three types of irises, we classify them based on the two characteristic quantities `petal width (cm)` and `petal length (cm)`.  
[LogisticRegression](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html) from [scikit-learn](https://scikit-learn.org/stable/index.html) is used as the classifier.  

Import the module to obtain the iris data set.  

```python
from sklearn import datasets
from sklearn.linear_model import LogisticRegression
import pandas as pd
import sub 
# iris
iris = datasets.load_iris() # irisのデータを辞書型で取得
# 辞書型の変数からDataFrameを作成
df = pd.DataFrame(data = iris['data'], columns=iris['feature_names'])
df['target'] = iris['target']
```

The data is formatted and classified by logistic regression.  

```python
# 特徴量とターゲットを取得
features = ['petal width (cm)', 'petal length (cm)']
# 新しいデータフレーム作成
dfn = df[features+["target"]]

X = dfn[features].values
y = dfn['target']

# 学習
LR = LogisticRegression(random_state=0) # ロジスティック回帰モデルのインスタンスを作成
LR.fit(X, y) # ロジスティック回帰モデルの重みを学習

y_pred = LR.predict(X) # 予測値の取得
y_pred_proba = LR.predict_proba(X) # 各クラスに属する確率

print("prediction: ", y_pred[::20])
print('probability')
print(y_pred_proba[::20])
```

Draw a boundary diagram.  

```python
# 境界線図を描画
sub.draw_boundary_map(dfn, LR)
```

![logistic](./images/logistic_regression.png)

Question:
Try different combinations of features and create a boundary map.  
