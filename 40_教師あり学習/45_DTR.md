# Dcision Tree Analysis

## Decision Tree Analysis (Regression)

Decision tree analysis can also be used for regression.
Using sklearn's [diabetes dataset](https://scikit-learn.org/stable/datasets/toy_dataset.html#diabetes-dataset), we try to predict quantitative measures of disease progression after one year.
According to the description, the features and other information are as follows

target

- a quantitative measure of disease progression one year after baseline

features

- age: age in years
- sex
- bmi: body mass index
- bp: average blood pressure
- s1 tc: total serum cholesterol
- s2 ldl: low-density lipoproteins
- s3 hdl: high-density lipoproteins
- s4 tch: total cholesterol / HDL
- s5 ltg: possibly log of serum triglycerides level
- s6 glu: blood sugar level

Here we use sklearn's [DecisionTreeRegressor](https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeRegressor.html).

First, import the module, adn obtain the features and targets.

```python
from sklearn import datasets
import pandas as pd
from sklearn.tree import DecisionTreeRegressor # 決定木分析(回帰)を行うためのクラス
from sklearn.model_selection import train_test_split

# diabetes
diabetes = datasets.load_diabetes()
# 辞書型の変数からDataFrameを作成
df = pd.DataFrame(data = diabetes['data'], columns=diabetes['feature_names'])
df['target'] = diabetes['target']

features = df.columns[:-1] # 最後のカラム以外を特徴量とする
# 特徴量とターゲットを取得
X = df[features].values 
y = df['target']
```

This time we will separate the data into two parts: 80% training data and 20% test data.  

```python
# 訓練データをテスト(検証)データに分割
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)
```

Learning. The depth of the decision tree is set to 2.

```python
# 決定木を用いて学習
DTR = DecisionTreeRegressor(max_depth=2, random_state=0)
DTR = DTR.fit(X_train, y_train)
```

Visualization.

![dtr](./images/decision_tree_reg.png)

It shows that in the training data, the patients are first classified according to the bmi value, and then according to the s5 value and blood pressure (bp) value, respectively.

The RMSE and R^2 are then calculated for each of the training and test data to evaluate performance.

```python
from sklearn.metrics import r2_score, mean_squared_error
y_train_pred = DTR.predict(X_train) # 訓練データの予測値
y_test_pred = DTR.predict(X_test) # テストデータの予測値

# 平均平方二乗誤差(RMSE)
rmse_train = mean_squared_error(y_train, y_train_pred)
rmse_test = mean_squared_error(y_test, y_test_pred)
print(f'RMSE 訓練: {rmse_train:.2f}, テスト： {rmse_test:.2f}')

# 決定係数(R^2)
r2_train = r2_score(y_train, y_train_pred)
r2_test = r2_score(y_test, y_test_pred)
print(f'R^2 訓練：{r2_train:.4f}, テスト：{r2_test:.4f}')
```

### Question

As you increase the depth of the decision tree, max_depth=2, 3, 4, ..., see if the performance evaluation gets better or worse.
