# Support Vector Regression

We have done classification with support vector machines, but regression can also be performed.  

- It is suitable for multivariate nonlinear regression problems because it estimates the regression curve without assuming a function form.
- The results are difficult to interpret because they involve nonlinear mapping to a high-dimensional feature space.

Contents:

- [Support Vector Regression](#support-vector-regression)
  - [Regression with linear kernel (plane)](#regression-with-linear-kernel-plane)
  - [Regression with linear kernel (curved surface)](#regression-with-linear-kernel-curved-surface)
  - [Regression with RBF kernel (curved surface)](#regression-with-rbf-kernel-curved-surface)

Here we will perform regression for two cases, one for flat data and the other for curved data, for different kernels.  
We will use sklearn's [sklearn.svm.SVR](https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVR.html) to perform support vector regression.  

## Regression with linear kernel (plane)

Import modules and create data for plane.

```python
import numpy as np
import pandas as pd
import random

# データの作成
leng = 1000
ax = [random.uniform(-1, 1) for e in range(leng)]
ay = [random.uniform(-1, 1) for e in range(leng)]
az = [3*x + 2*y + 1 for x, y in zip(ax, ay)] # 平面のデータを作成: z = 3x + 2y + 1
dat = np.array([ax, ay, az]).transpose()
df = pd.DataFrame(dat, columns=['ax', 'ay', 'az'])
```

Draw a 3D scatter plot to check the data of the plane we have created.  

```python
# 3D散布図を作成してデータを確認
import plotly.express as px
px.scatter_3d(df, x='ax', y='ay', z='az', size = [1]*len(df), size_max=10)
```

If you do not have the 'plotly' module, install it accordingly.

`pip install plotly`

![svr_plane](./images/svr_plane.png)

Features and targets are obtained and support vector regression is performed using a linear kernel.  

```python
# 特徴量とターゲットを取得
X = df[['ax', 'ay']].values
y = df['az']

# 線形カーネルによるSVRで学習、スコアの確認
from sklearn.svm import SVR
SVRi = SVR(kernel='linear')
SVRi.fit(X, y)
print('スコア： ', SVRi.score(X, y))
```

The score is greater than 0.99, indicating that the regression is well performed.  

Create a data frame to draw the predicted value at the same time.  

```python
# 予測のデータを同時に描画するためのデータフレーム作成
df['color'] = 0 # 元データの色は0にセットする。

df_pred = df.copy()
df_pred['color'] = 1 # 予測データの色は1にする。
df_pred['az'] = SVRi.predict(X) # 予測データを算出

# 上下方向にデータフレームを追加
df_all = pd.concat([df, df_pred], ignore_index=True)
```

```python
# 3D散布図で確認
px.scatter_3d(df_all, x='ax', y='ay', z='az', color='color', size = [1]*len(df_all), size_max=10)
```

![svr_plane_pred](./images/svr_plane_pred.png)

As the score confirms, the regression is well performed.

## Regression with linear kernel (curved surface)

Let's try to run the regression on curved surface data instead of flat data.  
Create data for curved surfaces.  

```python
az = [x**2 + 0.5*y**3 + 1 for x, y in zip(ax, ay)] # 曲面のデータを作成: z = x^2 + 0.5y^3 + 1
dat = np.array([ax, ay, az]).transpose()
df = pd.DataFrame(dat, columns=['ax', 'ay', 'az'])
```

Create a 3D scatter plot to verify the data.

```python
# 3D散布図を作成してデータを確認
px.scatter_3d(df, x='ax', y='ay', z='az', size = [1]*len(df), size_max=10)
```

![svr_curv](./images/svr_curv.png)

Support vector regression is performed using a linear kernel to check the scores.  

```python
# 特徴量とターゲットを取得
X = df[['ax', 'ay']].values
y = df['az']

# 線形カーネルによるSVRで学習、スコアの確認
SVRi = SVR(kernel='linear')
SVRi.fit(X, y)
print('スコア： ', SVRi.score(X, y))
```

The score is not even 0.2, indicating that the regression was not performed well.  

Let us plot the original and predicted data on the same 3D scatter plot.

```python
# 予測のデータを同時に描画するためのデータフレーム作成
df['color'] = 0 # 元データの色は0にセットする。

df_pred = df.copy()
df_pred['color'] = 1 # 予測データの色は1にする。
df_pred['az'] = SVRi.predict(X) # 予測データを算出

# 上下方向にデータフレームを追加
df_all = pd.concat([df, df_pred], ignore_index=True)
```

```python
# 3D散布図で確認
px.scatter_3d(df_all, x='ax', y='ay', z='az', color='color', size = [1]*len(df_all), size_max=10)
```

![svr_curv_linear](./images/svr_curv_linear.png)

We can see that it is not a successful regression.

## Regression with RBF kernel (curved surface)

Let's do a support vector regression using the RBF kernel on the data from the previous surface.  

```python
# RBFカーネルによるSVRで学習、スコアの確認
SVRi = SVR(kernel='rbf')
SVRi.fit(X, y)
print('スコア： ', SVRi.score(X, y))
```

The score is above 0.9, indicating that we have a reasonable regression.

Let us plot the original and predicted data on the same 3D scatter plot.  

```python
# 予測のデータを同時に描画するためのデータフレーム作成
df['color'] = 0 # 元データの色は0にセットする。

df_pred = df.copy()
df_pred['color'] = 1 # 予測データの色は1にする。
df_pred['az'] = SVRi.predict(X) # 予測データを算出

# 上下方向にデータフレームを追加
df_all = pd.concat([df, df_pred], ignore_index=True)
```

```python
# 3D散布図で確認
px.scatter_3d(df_all, x='ax', y='ay', z='az', color='color', size = [1]*len(df_all), size_max=10)
```

![svr_curv_rbf](./images/svr_curv_rbf.png)

We can see that the regression is relatively well done.
