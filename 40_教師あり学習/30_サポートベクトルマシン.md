# サポートベクトルマシン

内容：

- [サポートベクトルマシン](#サポートベクトルマシン)
  - [線形サポートベクトルマシン](#線形サポートベクトルマシン)
  - [線形のカーネルを用いた場合のSVM](#線形のカーネルを用いた場合のsvm)
  - [サポートベクトルマシン (カーネル法)](#サポートベクトルマシン-カーネル法)
  - [他のカーネルを用いた場合](#他のカーネルを用いた場合)
    - [多項カーネル](#多項カーネル)
    - [RBFカーネル](#rbfカーネル)
    - [シグモイドカーネル](#シグモイドカーネル)

## 線形サポートベクトルマシン

異なる種類のデータを直線 (高次元空間では超平面) の境界線で分類するという事を考えた時に、線形サポートベクトルマシンはマージンを最大化するように境界線を決定するものです。  ここでマージンとは、境界線に最も近いデータと、境界線との距離を指します。

![svm_margin](./images/svm_margin.drawio.svg)

マージンが最も大きくなるように右の図のように境界線が決定されます。

"SVMが他のアルゴリズムと差別化される特徴は、ただいくつかの点を分離する超平面を捜すことで終わるのではなく、いくつかの点を分離することができる幾多の候補平面の中でマージンが最大になる超平面 (maximum-margin hyperplane) を探す点にある。"([Wikipedia](https://ja.wikipedia.org/wiki/%E3%82%B5%E3%83%9D%E3%83%BC%E3%83%88%E3%83%99%E3%82%AF%E3%82%BF%E3%83%BC%E3%83%9E%E3%82%B7%E3%83%B3))

サポートベクトルマシンの詳細な説明は、scikit-learnの[Support Vector Machines](https://scikit-learn.org/stable/modules/svm.html)やwikipediaの[サポートベクターマシン](https://ja.wikipedia.org/wiki/%E3%82%B5%E3%83%9D%E3%83%BC%E3%83%88%E3%83%99%E3%82%AF%E3%82%BF%E3%83%BC%E3%83%9E%E3%82%B7%E3%83%B3)をご参照ください。

## 線形のカーネルを用いた場合のSVM

scikit-learnの[SVC](https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html)を使います。

ワインのデータセットを取得します。

```python
from sklearn import datasets
import pandas as pd

# wine
wine = datasets.load_wine() # wineのデータを辞書型で取得
# 辞書型の変数からDataFrameを作成
df_original = pd.DataFrame(data = wine['data'], columns=wine['feature_names'])
df_original['target'] = wine['target']
```

着目する特徴量を`proline`(プロリン)と`hue`(色相)の二つとして特徴量とターゲットを取得します。  

```python
# 特徴量とターゲットの設定
#features = ['alcohol', 'malic_acid']
features = ['proline', 'hue']

df = df_original[features+['target']]
X = df[features].values # 特徴量の設定
y = df['target'] # ターゲットの設定
```

test_sizeの比に従ってデータを訓練データと検証データに分割します。  

```python
# 訓練データと検証データに分割
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0, test_size=0.2)
```

データの標準化を行います。  
訓練データの統計量を使って、検証用データの標準化を行うことに注意してください。  

```python
# データの標準化 （平均値ゼロ、標準偏差１）
from sklearn.preprocessing import StandardScaler
SS = StandardScaler() # インスタンスを作成
SS.fit(X_train) # 訓練データの統計情報をセット
X_train_ss = SS.transform(X_train) # 訓練データの標準化
X_test_ss = SS.transform(X_test) # 検証用データの標準化
```

カーネルを`linear`(線形)にして学習します。  

```python
# 学習
from sklearn.svm import SVC # サポートベクトルマシンを用いるためのクラス
SVM = SVC(kernel='linear', random_state=0)
SVM.fit(X_train_ss, y_train)
y_train_predict = SVM.predict(X_train_ss)
```

訓練データでのDataFrameを作成して、境界線図を描画してみます。  

```python
# 境界線図の描画
import sub
# 訓練データのデータフレームを作成
df_train = pd.DataFrame(X_train, columns=features)
df_train['target'] = y_train
sub.draw_boundary_map(df, SVM, SS)
```

![svm_linear](./images/svm_linear.png)

訓練データと検証データのそれぞれについて正解率を求めてみます。  

```python
# 正解率
print('訓練データの正解率: ', SVM.score(X_train_ss, y_train))
print('検証データの正解率: ', SVM.score(X_test_ss, y_test))
```

## サポートベクトルマシン (カーネル法)

「データを別の特徴空間に移すことによって線形回帰を行えるようにする」のがカーネル法です。  
線形分離不可能なデータであっても、カーネル法を用いることによりサポートベクトルマシンで分離できるようになります。

"カーネル関数は、特徴空間中のデータの座標の明示的な計算を経由せずに、**特徴空間における内積をデータから直接計算する手段**を与える。内積を評価するためにカーネル関数を使うと、明示的な座標の計算を経るよりもしばしば計算量が少なくて済む。
カーネル関数を使って、計算複雑度の増大を抑えつつ内積にもとづく解析手法を高次元特徴空間へ拡張するアプローチを、一般に**カーネルトリック**と呼ぶ。"(wikipediaの[カーネル法](https://ja.wikipedia.org/wiki/%E3%82%AB%E3%83%BC%E3%83%8D%E3%83%AB%E6%B3%95)より)

[![kernel](https://upload.wikimedia.org/wikipedia/commons/f/fe/Kernel_Machine.svg)](https://upload.wikimedia.org/wikipedia/commons/f/fe/Kernel_Machine.svg)

Kernel machines are used to compute non-linearly separable functions into a higher dimension linearly separable function. ([wikipedia](https://ja.wikipedia.org/wiki/%E3%82%AB%E3%83%BC%E3%83%8D%E3%83%AB%E6%B3%95)より)

## 他のカーネルを用いた場合

scikit-learnで使えるカーネルの説明が[Kernel functions](https://scikit-learn.org/stable/modules/svm.html#kernel-functions)にあります。

- linear: $<x, x'>$
- polynomial: $(\gamma <x, x'>+r)^d$, where $d$ is specified by parameter `degree`, $r$ by `coef0`.
- rbf: $exp(-\gamma ||x-x'||^2)$, where $\gamma$ is specified by parameter `gamma`, must be greater than 0.
- sigmoid: $tanh(\gamma <x, x'>+r)$, where $r$ is specified by `coef0`.


### 多項カーネル

```python
# 多項カーネル
SVM = SVC(kernel='poly', random_state=0) # デフォルトは3次の次数 (degree)
SVM.fit(X_train_ss, y_train)
sub.draw_boundary_map(df, SVM, SS)
```

![svm_poly](./images/svm_poly.png)

### RBFカーネル

RBF (Radial Basis Function, 放射基底関数)

```python
# RBFカーネル
SVM = SVC(kernel='rbf', random_state=0)
SVM.fit(X_train_ss, y_train)
sub.draw_boundary_map(df, SVM, SS)
```

![svm_rbf](./images/svm_rbf.png)

### シグモイドカーネル

```python
# シグモイドカーネル
SVM = SVC(kernel='sigmoid', random_state=0)
SVM.fit(X_train_ss, y_train)
sub.draw_boundary_map(df, SVM, SS)
```

![svm_rbf](./images/svm_sigmoid.png)
