# k-nearest neighbor method

The k-Nearest Neighbor method (kNN) is a classification algorithm.  
This method extracts k data points in the neighborhood of the data point of interest, determines the class to which the k data points belong, makes a majority vote, and designates the result as the class of the data point of interest.
In the figure below, Class A: Class B: Class C = 0: 1: 2 for the three points in the neighborhood of the data point of interest, so the data point of interest is judged to be Class C.

![knn](./images/kNN.drawio.svg)

The distance used to determine the neighborhood, in addition to the commonly used [Euclidean distance](https://ja.wikipedia.org/wiki/%E3%83%A6%E3%83%BC%E3%82%AF%E3%83%AA%E3%83%83%E3%83%89%E8%B7%9D%E9%9B%A2)($L^2$ norm), there is also [Manhattan distance](https://ja.wikipedia.org/wiki/%E3%83%9E%E3%83%B3%E3%83%8F%E3%83%83%E3%82%BF%E3%83%B3%E8%B7%9D%E9%9B%A2)($L^1$ norm).  

- Learning does not take much time as it only involves memorizing data points.
- When forecasting, it takes time to calculate the distance to all data points.
- Data preprocessing is required to ensure that the proper distances are calculated.
- Results may vary depending on the number of neighbor points considered.

## Iris Classification

In the iris dataset, we try to classify with two features, 'sepal length (cm)' and 'sepal width (cm)'. For this classification, we use [KNeighborsClassifier](https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html) in scikit-learn.

Retrieve the iris data set.  

```python
from sklearn import datasets
import pandas as pd

# iris
iris = datasets.load_iris() # irisのデータを辞書型で取得
# 辞書型の変数からDataFrameを作成
df = pd.DataFrame(data = iris['data'], columns=iris['feature_names'])
df['target'] = iris['target']
```

Obtain feature and target data.

```python
# 特徴量とターゲットを取得
#features = ['petal width (cm)', 'petal length (cm)']
features = ['sepal length (cm)', 'sepal width (cm)']
# 新しいデータフレーム作成
dfn = df[features+["target"]]

X = dfn[features].values
y = dfn['target']
```

Classify using the k-nearest neighbor method.

```python
from sklearn.neighbors import KNeighborsClassifier # k近傍法のクラス

# 学習
kNC = KNeighborsClassifier(n_neighbors=3) # 近傍点を3個にしてインスタンス作成
kNC.fit(X, y) # k近傍法の学習

y_pred = kNC.predict(X) # 予測の取得
pd.DataFrame({'y':y, 'y_pred':y_pred})[::20] # 予測値の確認
```

Draws a boundary diagram.  

```python
import sub 
# 境界線図を描画
sub.draw_boundary_map(dfn, kNC)
```

![kNN](./images/kNN.png)

Because of the majority classification, the boundary diagram is quite complex.

Question：  
Try changing the number of neighbor points considered and the combination of features.  
Draw a boundary diagram.  
