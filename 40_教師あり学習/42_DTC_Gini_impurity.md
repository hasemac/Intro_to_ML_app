# Gini impurity of the decision tree

Let us denote the case where the number of data classified as Class 1 is 50 and the number of data classified as Class 2 is 40 as (50, 40).  
Let us calculate the Gini impurity and information gain for each of Case A and Case B in the figure below to see which case classification is adopted.

![Gini_impurity](./images/decision_tree_gini.drawio.svg)

## Impurity and information gain in Cases A and B

### Case A

Gini impurity of A0

```math
\begin{align}
I_{G\_A0} &= 1-\left[ 
    \left( \frac{50}{90} \right)^2 
    +\left( \frac{40}{90} \right)^2 
    \right] \notag \\
&= 0.494 \notag \\
\end{align}
```

Gini impurity of A1

```math
\begin{align}
I_{G\_A1} &= 1-\left[ 
    \left( \frac{40}{50} \right)^2 
    +\left( \frac{10}{50} \right)^2 
    \right] \notag \\
&= 0.320 \notag \\
\end{align}
```

Gini impurity of A2

```math
\begin{align}
I_{G\_A2} &= 1-\left[ 
    \left( \frac{10}{40} \right)^2 
    +\left( \frac{30}{40} \right)^2 
    \right] \notag \\
&= 0.375 \notag \\
\end{align}
```

Information gain in Case A

```math
\begin{align}
IG\_A &= I_{G\_A0}-\left[ \frac{50}{90} I_{G\_A1} + \frac{40}{90} I_{G\_A2}\right] \notag \\
&= 0.149 \notag
\end{align}
```

## Case B

Gini impurity of B0

```math
I_{G\_B0} = I_{G\_A0} = 0.494
```

Gini impurity of B1

```math
\begin{align}
I_{G\_B1} &= 1-\left[ 
    \left( \frac{30}{30} \right)^2 
    +\left( \frac{0}{30} \right)^2 
    \right] \notag \\
&= 0.000 \notag \\
\end{align}
```

Gini impurity of B2

```math
\begin{align}
I_{G\_B2} &= 1-\left[ 
    \left( \frac{20}{60} \right)^2 
    +\left( \frac{40}{60} \right)^2 
    \right] \notag \\
&= 0.444 \notag \\
\end{align}
```

Information gain in Case B

```math
\begin{align}
IG\_B &= I_{G\_B0}-\left[ \frac{30}{90} I_{G\_B1} + \frac{60}{90} I_{G\_B2}\right] \notag \\
&= 0.198 \notag
\end{align}
```

### Information Gain Comparison

Information gain
Case A: 0.149
Case B: 0.198
Since Case B has greater information gain, Case B is adopted in the classification.  
