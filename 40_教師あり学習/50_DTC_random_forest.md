# Random Forest

## What is Random Forest?

Random forest is a method in which multiple decision trees are created and the analysis results are obtained from the predictions of each decision tree.

- With a classification model, the majority vote in each decision tree yields the result of the analysis.  
- In a regression model, the results of the analysis are obtained by the respective means.  

The data and features used to create each decision tree are as follows  

- Data: **sampling with replacement** (data extraction allowing duplicates) from original data
- Features: **sampling without replacement** (extraction without allowing duplicates) from original features

Random forests use **ensemble learning** and **bugging**.

---

- **Sapmling with replacement**  
  Extracting data that allows duplicates
- **Sampling without replacement**  
  Extracting data without allowing duplicates
- **Ensemble learning**  
  A method that combines multiple machine learning models to obtain more accurate answers
- **Bagging**  
  A method of training multiple models using some data from the overall data

## Random forests in [scikit-learn](https://scikit-learn.org/stable/)

scikit-learn provides a function [RandomForestClassifier](https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html) for random forests, which can be given several options.  
For example, the following options are available

- **n_estimators: _int, default=100_**  
  Number of decision tree models
- **max_depth: _int, default=None_**  
  Depth of Decision Tree Model
- **min_samples_split: _int or float, default=2_**  
  Minimum number of samples when splitting a node
- **min_samples_leaf: _int or float, default=1_**  
  Minimum number of samples at the terminal node (leaf)
- **max_samples: _int or float, default=None_**  
  Number of samples of data in each decision tree when bootstrap is enabled (<total data).  
  If None, a number equal to the number of all data is extracted.  

## Example usage of [RandomForestClassifier](https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html)

Let's actually try using [RandomForestClassifier](https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html).  
Let's try to classify wines using [Wine dataset](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.load_wine.html#sklearn.datasets.load_wine) provided by scikit-learn.

Import the module, load the wine dataset, and get the features and targets.  

```python
from sklearn import datasets
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier # 決定木分析(分類)を行うためのクラス
from sklearn.ensemble import RandomForestClassifier # ランダムフォレストを実行するためのクラス

# wine
wine = datasets.load_wine() # wineのデータを辞書型で取得
# 辞書型の変数からDataFrameを作成
df = pd.DataFrame(data = wine['data'], columns=wine['feature_names'])
df['target'] = wine['target']

features = df.columns[:-1] # 最後のカラム以外を特徴量とする
# 特徴量とターゲットを取得
X = df[features].values 
y = df['target']
```

Split the data into training data and test (verification) data.  

```python
# 訓練データとテスト(検証)データに分割
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)
```

Learn and calculate the percentage of correct answers for each of the training and validation data.

```python
# ランダムフォレストを用いて学習
RFC = RandomForestClassifier(max_depth=2, random_state=0)
RFC.fit(X_train, y_train) 
 
# 訓練データに対する分類結果の確認
y_train_pred = RFC.predict(X_train) # 分類の予測結果
print('正解率(訓練)：', RFC.score(X_train, y_train)) # 正解率

# 検証データに対する分類結果の確認
y_test_pred = RFC.predict(X_test) # 分類の予測結果
print('正解率(検証)：', RFC.score(X_test, y_test)) # 正解率
```

```python
正解率(訓練)： 1.0
正解率(検証)： 0.9722222222222222
```

## Comparison with Decision Trees

Let's try classification using decision trees for the same training data.

```python
# 決定木を用いて学習
DTC = DecisionTreeClassifier(max_depth=2, random_state=0)
DTC = DTC.fit(X_train, y_train)

# 訓練データに対する分類結果の確認
y_train_pred = DTC.predict(X_train) # 分類の予測結果
print('正解率(訓練)：', DTC.score(X_train, y_train)) # 正解率

# 検証データに対する分類結果の確認
y_test_pred = DTC.predict(X_test) # 分類の予測結果
print('正解率(検証)：', DTC.score(X_test, y_test)) # 正解率
```

```python
正解率(訓練)： 0.9295774647887324
正解率(検証)： 0.8611111111111112
```

For the same calculation conditions, Random Forest certainly has a higher percentage of correct answers.

## Visualization and confirmation

Visualize the features as 'alcohol' and 'malic_acid'.  

Retrieve the features and targets.

```python
# 特徴量をalcoholとmalic_acidの二つのみを用いた場合で分類して視覚化してみます。

# 特徴量とターゲットを取得
df_feature2 = df[['alcohol', 'malic_acid', 'target']]
X = df_feature2[df_feature2.columns[:-1]].values 
y = df_feature2['target']
```

Draw a boundary diagram of the random forest.

```python
import sub
# ランダムフォレストを用いて学習
RFC = RandomForestClassifier(max_depth=2, random_state=0, verbose=1)
RFC = RFC.fit(X, y)
sub.draw_boundary_map(df_feature2, RFC)
```

![rand_forest](./images/rand_forest.png)

Draws a boundary diagram of the decision tree.

```python
import sub
# 決定木を用いて学習
DTC = DecisionTreeClassifier(max_depth=2, random_state=0)
DTC = DTC.fit(X, y)
sub.draw_boundary_map(df_feature2, DTC)
```

![tree](./images/tree.png)

It can be seen that the Random Forest is a more complex boundary diagram because it uses multiple decision trees.

## Question

Check how the boundary diagram changes by increasing or decreasing the number of decision tree models in the random forest.  
Also check out the other options.  
