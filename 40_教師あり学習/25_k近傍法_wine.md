# k近傍法

k近傍法では、近傍の点を探索するために距離が計算されています。  
特徴量のデータのスケールが大きく異なるとき、適切な距離が算出されない場合があります。  
ここでは、wineのデータセットのうち、'proline（プロリン）', 'hue（色相）'の二つの特徴量を用いて、

- 標準化を行わない場合
- 標準化を行う場合

の二つの場合で、wineの分類を行ってみます。  

## wineのデータセットの取得とスケールの確認

```python
from sklearn import datasets
import pandas as pd

# wine
wine = datasets.load_wine() # wineのデータを辞書型で取得
# 辞書型の変数からDataFrameを作成
df = pd.DataFrame(data = wine['data'], columns=wine['feature_names'])
df['target'] = wine['target']
```

```python
# 最小値と最大値の確認
import numpy as np
print('proline: ', np.min(df['proline']), np.max(df['proline']))
print('hue: ', np.min(df['hue']), np.max(df['hue']))
```

二つの特徴量はおよそ次の範囲であることが分かります。
'proline': 300～1700
'hue': 0.5～1.7

## 標準化をしない場合

```python
# 特徴量とターゲットを取得
#features = ['alcohol', 'hue']
features = ['proline', 'hue']
# 新しいデータフレーム作成
dfn = df[features+["target"]]

X = dfn[features].values
y = dfn['target']
```

```python
from sklearn.neighbors import KNeighborsClassifier # k近傍法のクラス

# 学習
kNC = KNeighborsClassifier(n_neighbors=3) # 近傍点を3個にしてインスタンス作成
kNC.fit(X, y) # k近傍法の学習

y_pred = kNC.predict(X) # 予測の取得
pd.DataFrame({'y':y, 'y_pred':y_pred})[::20] # 予測値の確認
```

```python
# 標準化しない場合の正解率
kNC.score(X, y)
```

標準化しない場合の正解率は、0.85でした。  
境界線図を描画してみます。  

```python
import sub 
# 境界線図を描画
sub.draw_boundary_map(dfn, kNC)
```

![kNN_wind](./images/kNN_wine.png)

'hue'に変動があっても、分類にほぼ変化が起こらないことが分かります。  

## 標準化する場合

```python
# 標準化
from sklearn.preprocessing import StandardScaler

SS = StandardScaler() # インスタンスを作成
X_ss = SS.fit_transform(X) # データXを標準化

# 学習
kNC_ss = KNeighborsClassifier(n_neighbors=3) # 近傍点を3個にしてインスタンス作成
kNC_ss.fit(X_ss, y) # k近傍法の学習

y_ss_pred = kNC_ss.predict(X_ss) # 予測の取得
```

```python
# 標準化した場合の正解率
kNC_ss.score(X_ss, y)
```

正解率は0.92であって、改善していることが分かります。  
境界線図を描画してみます。

```python
# 標準化したデータに対してデータフレーム作成
dfn_ss = pd.DataFrame(X_ss, columns=['proline_ss', 'hue_ss'])
dfn_ss['target'] = dfn['target']
# 境界線図を描画
sub.draw_boundary_map(dfn_ss, kNC_ss)
```

![kNN_wine_ss](./images/kNN_wine_ss.png)

平均値がゼロ、標準偏差が１になるようにprolineとhueの値が調整されていることがわかります。  
また、hueに変動があった場合に、分類されるクラスも変動していることが分かります。  

確認のため、元のスケールに直して、境界線図を描画します。  

```python
# 逆変換したデータに対して境界線図を描画
sub.draw_boundary_map(dfn, kNC_ss, scaler=SS)
```

![kNN_ss_inv](./images/kNN_wine_ss_inv.png)