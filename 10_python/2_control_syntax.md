# control syntax

内容

- [control syntax](#control-syntax)
  - [if](#if)
  - [for](#for)
  - [while](#while)
  - [break and continue](#break-and-continue)
  - [Exceptional treatment 'try'](#exceptional-treatment-try)

## if

Achieves branching processing, such as executing process A when a certain conditional expression is true, and executing process B otherwise.  
Conditional expressions and processing expressions are written using indentation.

```python
# key_if

a = 0.5

# [0, 1]の範囲にあるかを判定
print('1st')
if a >= 0.0 and a <= 1.0:
    print('inside')
else:
    print('outside')

print('\n2nd')
# [0, 1]の範囲にあるかを判定（その２）
# 条件式をまとめて書くことも可能です。
if 0.0 <= a <= 1.0:
    print('inside')
else:
    print('outside')

print('\n3rd')
# [0, 1]の範囲にあるかを判定（その３）
# elifを使って判定
if a < 0.0:
    print('outside')
elif a <= 1.0:
    print('inside')
else:
    print('outside')
```

```python
# key_if_q
# Question
# Judge the value of (x, y), 
# and when it is in the first quadrant, print out '1', 
# if in the second quadrant, print out '2".
# please make a such judgment.

x, y = 1, -1
```

![quad](./images/quad.drawio.svg)

## for

Execute repeated processing for iterable variables.
Iterable variable is an object that can be repeated.

```python
# key_for1
fruits = ['apple', 'pineapple', 'melon', 'orange', 'banana', 'lemon']
value = [150, 500, 700, 100, 200, 200]

# 単純にfruitsを表示する場合
for f in fruits:
    print(f)
    
# インデックスを付加する場合
print('\nwith index')
for i, f in enumerate(fruits):
    print(i, f)
    
# 価格も合わせて表示する場合
print('\nwith zip')
for f, v in zip(fruits, value):
    print(f, v)
```

The dictionary type variables are also itelable, so they can be used in 'for'.

```python
# key_for2
person = {'name': 'Mike', 
          'age': 18, 
          'height': 180.0, 
          'blood_type': 'AB', 
          'age': 25} # If there is a duplicated key, the one defined behind will be valid.
for e in person:
    print(f'key: {e}, value:{person[e]}')
```

## while

It is a syntax that repeats the processing between True.

```python
# key_while1

# while 式:
#    　処理
#      ...
# フィボナッチ数列を1000を超えるまで求める。
f = [0, 1]
while f[-1] < 1000:
    f.append(f[-1] + f[-2])
print(f)
```

## break and continue

In the loop of 'for' and 'while', if you have a 'break', get out of the entire loop, and if it is 'continue', pass through the loop.

```python
# key_break_cont
for i in range(10):
    if i == 4:
        continue
    if i == 8:
        break
    print(i)
```

## Exceptional treatment 'try'

It is a mechanism to branch the process according to the error (exception) that occurred during execution.
For example.  

```python
try:
    # processA（Processing that may cause exceptions）
    ...
except 例外B:
    # processB（Processing when exception B occurs）
    ...
except 例外C:
    # processC（Processing when exception C occurs）
    ...
else:
    # Processing when no exception occurs
    ...
finally:
    # Processing to execute in any case
    ...
```

```python
# key_try
v = 'test_value'
v = 2
try:
    v = v*v
except Exception as e:
    print(e)
else:
    print('no error')
finally:
    print('finish')
```

If there is no ELSE or FINALLY processing, you do not need to describe it.
