# Numpy function

There are only a few introductions here. There are many other functions.
For more information, please refer to [Numpy User Guide](https://numpy.org/doc/stable/user/index.html#user).

内容：

- [Numpy function](#numpy-function)
  - [Functions related to statistics](#functions-related-to-statistics)
  - [Maximum and minimum indexes: argmax(), argmin()](#maximum-and-minimum-indexes-argmax-argmin)
  - [Cumulative sum: cumsum()](#cumulative-sum-cumsum)
  - [Remove duplicates: unique()](#remove-duplicates-unique)
  - [Determinant and inverse matrix: det(), inv()](#determinant-and-inverse-matrix-det-inv)
  - [Transpose of the matrix: transpose(), T](#transpose-of-the-matrix-transpose-t)
  - [Eigen values and eigen vectors: eig()](#eigen-values-and-eigen-vectors-eig)

## Functions related to statistics

Numpy has a function that calculates various statistics.

```python
import numpy as np
num = [np.random.randn() for e in range(10)] # Generate 10 random numbers with average zero standard deviation 1
num = np.array(num) # Convert to NUMPY array

Print ("average value:", np.mean(num)) #Average
Print ("Median:", np.median(num)) # Median
Print ("Maximum:", np.max(num)) # Maximum value
Print ("Minimum:", np.min(num)) # Minimum value
Print ("Sum:", np.sum(Num)) # Sum
Print ("Variance:", np.var(NUM)) # Variance
Print ("Standard deviation:", np.std(Num)) # Standard deviation
Print ()

# It can be represented by the following method.
print("Average:", num.mean())
#print("中央値:", num.median()) # 中央値 # この属性は無いです。
print("Maximum:", num.max()) 
print("Minimum:", num.min())
print("Sum:", num.sum())
print("Variance:", num.var()) 
print("Standard deviation:", num.std()) 
```

Question:  
Make an increase in the number of random numbers generated, and make sure that the average zero, and standard deviation is 1.

## Maximum and minimum indexes: argmax(), argmin()

```python
# Index that takes the maximum value
print('i_max: ', num.argmax())
print(num[num.argmax()])

# Index that takes the minimum price
print('i_min: ', num.argmin())
print(num[num.argmin()])
```

## Cumulative sum: cumsum() 

```python
# Cumulative sum
a = np.arange(10)
print('a: ', a)
print('Cumulative sum：', a.cumsum())
```

## Remove duplicates: unique() 

```python
# Remove duplicates
a = np.array([1,2,7,3,3,4,5,5,6,7,8,9,3,10])
print('a: ', a)
print('Remove duplicates：', np.unique(a))
```

## Determinant and inverse matrix: det(), inv()

```python
# Determinant and inverse matrix
m0 = [np.random.rand() for e in range(9)]
m1 = np.array(m0)
m2 = m1.reshape(3, 3) # Create 3 rows and 3 rows

print('m2: ')
print(m2)
print()
print('determinant of M2: ', np.linalg.det(m2))
print()
m3 = np.linalg.inv(m2) # m2 inverse matrix
print('inverse of m2: ', m3)
print()
print('Confirm that it is a inverse matrix：')
m4 = np.dot(m2, m3)
print(m4)
print()

# Definition of functions that ignore small values
def round_np(nparray):
    sh = nparray.shape
    res = np.array( # Round to 10 decimal places
        [round(e, 10) for e in nparray.reshape(-1)]
        )
    return res.reshape(sh)

# 微小な値を無視
print('Inverted matrix (ignoring small values)：')
print(round_np(m4))
```

## Transpose of the matrix: transpose(), T

```python
# Transpose of the matrix
m = np.array([[1,2,3],
              [4,5,6],
              [7,8,9]]
             )
print('m')
print(m)
print()
print('Transpose1')
print(m.transpose())
print()
print('Transpose2')
print(m.T)
```

## Eigen values and eigen vectors: eig()

```python
# Eigen values and eigen vectors
m = np.array([[1,2,3],[1,4,2],[3,1,2]]) # Creation of a matrix
eig_val, eig_vec = np.linalg.eig(m) # Eigen values and eigen vectors

print('m: ')
print(m)
print()
print('Eigen values：', eig_val)
print('Eigen vectors')
print(eig_vec)
```

```python
m: 
[[1 2 3]
 [1 4 2]
 [3 1 2]]

Eigen values： [ 6.37575386 -1.44214617  2.06639231]
Eigen vectors
[[-0.53726405 -0.73277291  0.26526281]
 [-0.66397205 -0.112016   -0.75606366]
 [-0.52008505  0.67119019  0.59833384]]
```

Note:  
For example, that the eigenvector for the first eigenvalue `6.376` is not the first row of the eigenvectors, but the first column `(-0.537, -0.664, -0.520)`.

Check that the eigenvalues ​​and eigenvectors.

```python
# Checking for eigenvalues ​​and eigenvectors
v1 = eig_vec[:, 0] # Get the first column which is the first eigenvector
print('matrix and eigenvector:', m.dot(v1))
print('eigenvalue and eigenvector:', eig_val[0]*v1) 
```

Let's set $\lambda_i$ as the eigenvalues of matrix $M$, and $\bold{e}_i$ as eigenvectors, and $N=(\bold{e}_1 \: \bold{e}_2 \: \cdots \: \bold{e}_n)$ as a matrix with eigenvectors.

At this time, the following formula is established.  

```math
\begin{align}
MN &=(\lambda_1 \bold{e}_1 \: 
\lambda_2 \bold{e}_2 \: 
\cdots \: 
\lambda_n \bold{e}_n) \\
&=(\bold{e}_1 \: \bold{e}_2 \: \cdots \: \bold{e}_n)
\begin{pmatrix}
\lambda_1 & 0 & \cdots & 0 \\
0 & \lambda_2 & \ddots & \vdots \\
\vdots & \ddots & \ddots & 0 \\
0 & \cdots & 0 & \lambda_n
\end{pmatrix}
\end{align}
```

Therefore, diagonalization can be performed.

```math
N^{-1}MN = 
\begin{pmatrix}
\lambda_1 & 0 & \cdots & 0 \\
0 & \lambda_2 & \ddots & \vdots \\
\vdots & \ddots & \ddots & 0 \\
0 & \cdots & 0 & \lambda_n
\end{pmatrix}
```

Let's diagonalize the matrix $M$.

```python
# Diagonal
ml = np.linalg.inv(eig_vec).dot(m).dot(eig_vec)
print(round_np(ml)) # 小さな値は無視して表示
```

You can see that the diagonal component are eigenvalues.
