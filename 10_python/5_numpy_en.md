# numpy

"NumPy (pronounced /ˈnʌmpaɪ/ NUM-py) is a library for the Python programming language, adding support for large, multi-dimensional arrays and matrices, along with a large collection of high-level mathematical functions to operate on these arrays."([Wikipedia](https://en.wikipedia.org/wiki/NumPy))

The functions introduced here are only a small part. Please refer [NumPy user guide](https://numpy.org/doc/stable/user/index.html#user) in detail.

内容：

- [numpy](#numpy)
  - [Module import](#module-import)
  - [Creating an array](#creating-an-array)
    - [Creation by conversion of the list](#creation-by-conversion-of-the-list)
    - [Creation with the chopped width](#creation-with-the-chopped-width)
    - [Creation with the number of elements](#creation-with-the-number-of-elements)
    - [Created by specifying dimensions](#created-by-specifying-dimensions)
  - [Check the shape and size of the array](#check-the-shape-and-size-of-the-array)
  - [Data extraction](#data-extraction)
  - [Array shaping](#array-shaping)

## Module import

```python
import numpy as np
```

Make sure you can call the Numpy function with `np`.

## Creating an array

Here are some of the methods of creating Numpy arrays.

### Creation by conversion of the list

```python
# Convert the list to Numpy array
a = [[1, 2, 3],
     [4, 5, 6]]
b = np.array(a)
print('type a:', type(a))
print('type b:', type(b))
```

### Creation with the chopped width

```python
# Array creation with chopped width
np.arange(-1.5, 1.5, 0.1)
```

The Start is included, the stop is not included in the creation of `np.arange(Start, Stop, Step)`.

### Creation with the number of elements

```python
# Creation with the element number
np.linspace(-1.5, 1.5, 30)
```

Creating an array with `np.linspace(START, STOP, Num)`, including Start and STOP, has the number of elements.
Please note that if you make a mistake in the value of num, the expected step width may not be obtained.  

### Created by specifying dimensions

```python
# Array in 4 lines and 2 rows with 1
np.ones((4,2))
```

```python
# Array with 3 lines and 4 rows with 0
np.zeros((3,4))
```

## Check the shape and size of the array

To check the shape of the numpy array, it is convenient to use `shape` to see all dimensions.
It is possible to use `len()`, but you can see only one dimension size.

```python
# Check the size of numpy array
a = [[1, 2, 3],
     [4, 5, 6]]
b = np.array(a)

print('shape: ', b.shape)
print('len1: ', len(b), ' len2: ', len(b[0]))
```

By the way, there is no `shape` attribute on the list.The following command will be an error:  

```python
a.shape
```

The size (total number of elements) of numpy array can be obtained with the `size` attribute.

```python
# Confirmation of size (total number of elements)
# B is 2 lines and 3 rows, so the total number of elements is 6
b.size
```

The dimension of numpy array can be obtained with the `ndim` attribute.

```python
# Confirmation of dimension
# b has 2 rows and 3 rows, so the dimension is 2
b.ndim
```

## Data extraction

You can use the slice like a list.

```python
# Extracting value by slice
a = np.arange(10)
print('a: ', a)

print('a[2]: ', a[2])

print('index 1から5未満まで', a[1:5])

print('偶数小さい順: ', a[::2])

print('奇数大きい順: ', a[::-2])
```

条件を指定して抽出することも可能です。

```python
# Extracting values by condition_ Example 1
m = np.arange(10)
print(m)
print(m[m % 2 == 0]) # Extracts only even values
print(m % 2 == 0) # Confirmation of conditional expressions
```

You can extract TRUE and FALSE's bool arrangements, so another array may be a condition.
Of course, the number of elements must be the same.

```python
# Extracting values by condition_ Example 2
m = np.arange(10)
n = m/10
print('m: ', m)
print('n: ', n)
print(n[m % 2 == 0]) # Extract n in a place where m is even number
```

## Array shaping

For example, the dimensions of the array are often changed, such as converting 2D image data into a 1D array, processing it, and then converting it back to 2D to create an image.  
To change the shape of array, use `reshape()`.  

If -1 is specified for the number of elements, the value determined from other factors is used.  

```python
# Array shaping
m = np.arange(64) # One-dimensional arrangement with 64 elements
print(m.reshape(4, 16)) # 4 lines and 16 rows
print(m.reshape(-1, 16)) # Shaping in 16 columns
print(m.reshape(4, -1)) # Shaping on 4 lines
```

```python
# Array shaping part 2
m = np.arange(64) # One-dimensional arrangement with 64 elements
print(m.reshape(4, 4, 4)) # 3D array of elements 4
print(m.reshape(-1, 4, 4)) # Specify -1 for the number of elements
```
