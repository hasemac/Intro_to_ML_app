# class

- [class](#class)
  - [What is a class](#what-is-a-class)
  - [Procedure to use class](#procedure-to-use-class)
    - [Definition of class](#definition-of-class)
    - [Creating an instance](#creating-an-instance)
    - [Using method](#using-method)

Python is a language that can be written in various ways, such as "procedure type", "object orientation", and "function type", so there is no need to define a class, but **by coding using classes, you can greatly improve the readability and reusability of your code**. For this reason, the advantage can be demonstrated in large-scale design, development with multiple people, and development of reuse.

## What is a class

The Python class is like a prototype or model for generating objects. The class is a mechanism that describes the contents of the object, and provides an area for writing **data and process**.

- Data can be retained in the class.
- The processing using that data, etc. can be performed.

## Procedure to use class

1. Definition of class
2. Creating an instance
3. Using method

### Definition of class

Here, as an example of a class, we will define a class that expresses company employees.

```python
# Definition of classes expressing company employees
class CompanyEmployee: # Definition of class name

    # Variables used in class
    # It is not necessary to write it here,
    # but, writing improves readability.

    id = None # employee number
    name = None # Employee name
    age = None # Employee age
    price_info = None # Price information for goods

    # Definition of constructor
    # This function is executed when instance is created.
    # Initialization function for the instance
    def __init__(self, id, name):
        self.id = id
        self.name = name

    # Definition of methods
    def set_age(self, age):
        self.age = age
    
    def print_information(self):
        print(f'ID: {self.id}')
        print(f'Name: {self.name}')
        print(f'Age: {self.age}')

    def teach_price(self, prices):
        self.price_info = prices

    def ask_price(self, product_name):
        print(f'Product: {product_name}')
        print(f'Price: {self.price_info[product_name]}')
```

According to the PEP naming rules, it is as follows.  
Class name: CapWords format (connect words of capital letters only)  
Method name: Use lowercase letters and under score as needed.  

- In the method definition, you need to specify `self` for the first argument.
- In order to refer to the in-class variables, you need to append `self`.

### Creating an instance

We will create two instances (entity), Yamada-san and Suzuki-san.

```python
# Creating instance
ce_yamada = CompanyEmployee(id=1, name='Yamada')
ce_suzuki = CompanyEmployee(id=2, name='Suzuki')
```

### Using method

Display the information of Yamada-san and Suzuki-san by making Yamada's age 30 years old.

```python
# Method execution
ce_yamada.set_age(30) # Set Yamada's age to 30 years old.
ce_yamada.print_information()
ce_suzuki.print_information()
```

Suzuki's age is not set.

```python
ID: 1
Name: Yamada
Age: 30
ID: 2
Name: Suzuki
Age: None
```

Tell Mr. Yamada the price of the fruit, and then ask about the price of the fruit.  

```python
# Method execution
# Tell the price of the fruit to Yamada-san.
fruit = {'apple': 150, 'orange': 100, 'melon': 750, 'peach': 250}
ce_yamada.teach_price(fruit)
```

```python
# Method execution
# Yamada-san has now been able to respond to the price of fruits.
ce_yamada.ask_price('melon')
```

```python
Product: melon
Price: 750
```

Suzuki-san can't answer the price of the fruit because we don't teach Suzuki-san.
If you execute the next, you will get an error.

```python
# Method execution
# Suzuki-san can't answer because we didn't teach Suzuki.
ce_suzuki.ask_price('melon')
```
