# クラス

- [クラス](#クラス)
  - [クラスとは](#クラスとは)
  - [クラスの使用手順](#クラスの使用手順)
    - [クラスの定義](#クラスの定義)
    - [インスタンスの作成](#インスタンスの作成)
    - [メソッドの実行](#メソッドの実行)
  - [クラス変数とインスタンス変数](#クラス変数とインスタンス変数)
  - [クラスの使用例](#クラスの使用例)

Python は「手続き型」「オブジェクト指向」「関数型」など様々な書き方ができる言語なので、必ずしもクラスを使用する必要はありませんが、**クラスを用いてコーディングすることで、コードの可読性や再利用のしやすさを大きく向上させる**ことができます。このため、大規模な設計、多人数による開発、再利用前提の開発にてそのメリットを発揮できます。

## クラスとは

"クラスはデータと機能を組み合わせる方法を提供します。 新規にクラスを作成することで、新しいオブジェクトの型を作成し、その型を持つ新しいインスタンスが作れます。 クラスのそれぞれのインスタンスは自身の状態を保持する属性を持てます。 クラスのインスタンスは、その状態を変更するための (そのクラスが定義する) メソッドも持てます。"([Pythonチュートリアル](https://docs.python.org/ja/3/tutorial/classes.html))

- クラス（インスタンス）内にデータを保持できる。
- そのデータ等を用いた処理を実行できる。

## クラスの使用手順

1. [クラス（ひな型）を定義する。](#クラスの定義)
2. [クラス（ひな型）からインスタンス（実体）を作成](#インスタンスの作成)
3. [インスタンスのメソッドを介して処理を実行](#メソッドの実行)

### クラスの定義

`TestClass`というクラス名のクラスを定義してみます。

```python
class TestClass: # クラス名称
    
    number = 0 # クラス変数
    
    # コンストラクタ
    def __init__(self):
        # インスタンス生成時の初期化処理をここに記述します。
        self.test_value = 0 # インスタンス変数
        
    # メソッド
    def test_method(self, int_value):
        self.test_value += int_value
        print(f'test value:{self.test_value}')
```

### インスタンスの作成

インスタンス(実体)は、次のようにして作成することができます。

```python
test_instance = TestClass()
```

### メソッドの実行

`TestClass`で定義されているメソッド`test_method`を２回ほど、実行してみます。

```python
test_instance.test_method(5)
```

```python
test value:5
```

```python
test_instance.test_method(5)
```

```python
test value:10
```

インスタンス変数`test_value`に引数の値を加算し、その値を表示するメソッド`test_method`ですが、インスタンス変数の値が0, 5, 10と変化していっていることがわかります。  

## クラス変数とインスタンス変数


**クラス変数：全てのインスタンスで共通の変数**  
**インスタンス変数：インスタンス毎に異なる変数**  

下の例のように定義された変数はクラス変数です。

```python
class TestClass: # クラス名称
    
    number = 0 # クラス変数
```

クラス変数は`（クラス名）.（クラス変数名）`という形で参照できます。上の例で言うと`TestClass.number`ということになります。  

インスタンス変数は`（インスタンス名）.（インスタンス変数名）`または`self.（インスタンス変数名）`という形式で参照できます。
先の例でいうと次のような名前で参照可能です。  

`test_instance.test_value`
`self.test_value`

## クラスの使用例

ここではクラスの例として、会社従業員を表現するクラスを定義してみます。

```python
# 会社従業員を表現するクラスの定義
class CompanyEmployee: # クラス名称の定義

    last_id = 0 # 最後の従業員番号

    def __init__(self, name):
        CompanyEmployee.last_id += 1
        self.id = CompanyEmployee.last_id # 従業員番号
        self.name = name # 従業員の名前
        self.age = None # 従業員の年齢
        self.price_info = None # 物品の価格情報

    def set_age(self, age):
        self.age = age
    
    def print_information(self):
        print(f'ID: {self.id}')
        print(f'Name: {self.name}')
        print(f'Age: {self.age}')

    def teach_price(self, prices):
        self.price_info = prices

    def ask_price(self, product_name):
        print(f'Product: {product_name}')
        print(f'Price: {self.price_info[product_name]}')
```

PEPの命名規則によれば次の通りです。  
クラス名：CapWords形式（先頭だけ大文字の単語を繋げる）  
メソッド名：小文字、必要に応じてアンダースコアを使用。  

- メソッドの定義では、最初の引数に`self`を指定する必要があります。
- インスタンス変数を参照するには`self`を修飾する必要があります。

山田さん、鈴木さんの２名のインスタンス（実体）を作成します。

```python
# インスタンスの作成
ce_yamada = CompanyEmployee(name='Yamada')
ce_suzuki = CompanyEmployee(name='Suzuki')
```

山田さんの年齢を３０歳にして、山田さん、鈴木さんの情報を表示してみます。

```python
# メソッドの実行
ce_yamada.set_age(30) # 山田さんの年齢を30才に設定します。
ce_yamada.print_information()
ce_suzuki.print_information()
```

鈴木さんの年齢は設定されていません。  
また従業員番号は全てのインスタンスで共通のクラス変数`last_id`から求められています。  

```python
ID: 1
Name: Yamada
Age: 30
ID: 2
Name: Suzuki
Age: None
```

山田さんに果物の価格を教えて、その後に果物の価格を尋ねます。  

```python
# メソッドの実行
# 山田さんに果物の価格を教えます。
fruit = {'apple': 150, 'orange': 100, 'melon': 750, 'peach': 250}
ce_yamada.teach_price(fruit)
```

```python
# メソッドの実行
# 山田さんは果物の価格をこたえられるようになりました。
ce_yamada.ask_price('melon')
```

```python
Product: melon
Price: 750
```

鈴木さんには教えていないので果物の価格を答えることができません。
次を実行するとエラーになります。

```python
# メソッドの実行
# 鈴木さんには教えていないので答えられません。
ce_suzuki.ask_price('melon')
```
