# numpyの四則演算

内容：

- [numpyの四則演算](#numpyの四則演算)
  - [リストの演算との違い](#リストの演算との違い)
  - [アレイとスカラーの四則演算](#アレイとスカラーの四則演算)
  - [同じ形状のアレイ同士の四則演算](#同じ形状のアレイ同士の四則演算)
  - [異なる形状のアレイ同士の四則演算](#異なる形状のアレイ同士の四則演算)
    - [一行と一列](#一行と一列)
    - [同一列数で片方が一行](#同一列数で片方が一行)
    - [同一行数で片方が一列](#同一行数で片方が一列)

## リストの演算との違い

**注意:**
同じ計算式であっても、リストの場合と、numpyのアレイとで計算結果が異なることがあります。  
アレイを扱う際には、それは**リストなのかnumpyのアレイなのかを注意**するようにしてください。  

例を示します。

リストの変数`a`, `b`と、それをnumpyアレイに変換した変数`c`, `d`を用意します。

```python
import numpy as np

a, b = [1, 2, 3, 4, 5], [6, 7, 8]
c = np.array(a)
d = np.array(b)

print('type a: ', type(a))
print('type b: ', type(b))
print('type c: ', type(c))
print('type d: ', type(d))
```

リストの加算ですが、これはエラーが出ません。
リストに新たな要素が追加されます。

```python
# リストの加算
a + b
```

numpyアレイの加算ですが、これはエラーが発生します。  
要素数が同じでないことが原因です。

```python
# numpyアレイの加算
c + d
```

numpyアレイの加算で、要素数が同じ場合をしてみます。
新たな要素が追加されるのではなくて、要素同士で加算が行われていることが分かります。

```python
# numpyアレイの加算（その２）
c + c
```

## アレイとスカラーの四則演算

この場合は、アレイのすべての要素とスカラー(ただ一つの値からなる数値)に対して四則演算されます。  
計算の例：  

```math
\begin{pmatrix}
x_{11}&x_{12} \\
x_{21}&x_{22}
\end{pmatrix}
+a
=
\begin{pmatrix}
x_{11}+a&x_{12}+a \\
x_{21}+a&x_{22}+a
\end{pmatrix}
```

```math
a/
\begin{pmatrix}
x_{11}&x_{12} \\
x_{21}&x_{22}
\end{pmatrix}
=
\begin{pmatrix}
a/x_{11}&a/x_{12} \\
a/x_{21}&a/x_{22}
\end{pmatrix}
```

```python
# スカラーとの加減
m = np.array([[1,2,3], [4,5,6], [7,8,9]])
print('m')
print(m)
print()
print('m+10')
print(m+10)
print()
print('10-m')
print(10-m)
```

```python
# スカラーとの乗除
print('m*2')
print(m*2)
print()
print('m/10')
print(m/10)
print()
print('10/m')
print(10/m)
```

## 同じ形状のアレイ同士の四則演算

この場合は同じ成分同士で四則演算が行われます。  
計算の例：

```math
\begin{pmatrix}
x_{11}&x_{12} \\
x_{21}&x_{22}
\end{pmatrix}
-
\begin{pmatrix}
y_{11}&y_{12} \\
y_{21}&y_{22}
\end{pmatrix}
=
\begin{pmatrix}
x_{11}-y_{11}&x_{12}-y_{12} \\
x_{21}-y_{21}&x_{22}-y_{22}
\end{pmatrix}
```

```math
\begin{pmatrix}
x_{11}&x_{12} \\
x_{21}&x_{22}
\end{pmatrix}
*
\begin{pmatrix}
y_{11}&y_{12} \\
y_{21}&y_{22}
\end{pmatrix}
=
\begin{pmatrix}
x_{11}y_{11}&x_{12}y_{12} \\
x_{21}y_{21}&x_{22}y_{22}
\end{pmatrix}
```

これは乗除の場合、[アダマール積](https://ja.wikipedia.org/wiki/%E3%82%A2%E3%83%80%E3%83%9E%E3%83%BC%E3%83%AB%E7%A9%8D)になります。  

```python
# 同じ形状のアレイ同士の加減
m = np.array([[1,2,3], [4,5,6], [7,8,9]])
n = np.array([[10,20,30], [40,50,60], [70,80,90]])
print('m')
print(m)
print()
print('n')
print(n)
print()
print('m+n')
print(m+n)
print()
print('n-m')
print(n-m)
```

```python
# 同じ形状のアレイ同士の乗除
print('m*n')
print(m*n)
print()
print('m/n')
print(m/n)
```

いわゆる内積、行列積の演算を行いたい場合はnumpyの`dot()`関数を用います。  

```python
# ベクトルの内積
v1 = np.array([1,2,3])
v2 = np.array([4,5,6])
print(v1.dot(v2))
print(np.dot(v1, v2))
```

```python
# 行列積
m1 = np.array([[1,2],[3,4]])
m2 = np.array([[5,6], [7,8]])
print('m1.dot(m2)')
print(m1.dot(m2))
print()
print('m2.dot(m1)') # 行列積は非可換
print(m2.dot(m1))
print()
print('np.dot(m1,m2)')
print(np.dot(m1,m2))
```

## 異なる形状のアレイ同士の四則演算

異なる形状であっても次の場合は四則演算が可能です。

### 一行と一列

```math
\begin{pmatrix}
x_{1}&x_{2}&x_3
\end{pmatrix}
+
\begin{pmatrix}
y_{1}\\y_{2}
\end{pmatrix}
=
\begin{pmatrix}
x_1+y_1&x_2+y_1&x_3+y_1\\
x_1+y_2&x_2+y_2&x_3+y_2
\end{pmatrix}
```

```python
# 一行と一列
m1 = np.array([1, 2, 3]) # １行
m2 = np.array([[10], [20]]) # １列
print("m1+m2")
print(m1+m2)
```

### 同一列数で片方が一行

```math
\begin{pmatrix}
x_{1}&x_{2}&x_3
\end{pmatrix}
+
\begin{pmatrix}
y_{11}&y_{12}&y_{13}\\
y_{21}&y_{22}&y_{23}\\
\end{pmatrix}
=
\begin{pmatrix}
x_1+y_{11}&x_2+y_{12}&x_3+y_{13}\\
x_1+y_{21}&x_2+y_{22}&x_3+y_{23}
\end{pmatrix}
```

```python
# 同一列数で片方が一行
m1 = np.array([1, 2, 3]) # １行
m2 = np.array([[10, 20, 30], [40, 50, 60]]) # ２行３列
print("m1+m2")
print(m1+m2)
```

### 同一行数で片方が一列

```math
\begin{pmatrix}
x_1\\x_2\\x_3
\end{pmatrix}
+
\begin{pmatrix}
y_{11}&y_{12}\\
y_{21}&y_{22}\\
y_{31}&y_{32}\\
\end{pmatrix}
=
\begin{pmatrix}
x_1+y_{11}&x_1+y_{12}\\
x_2+y_{21}&x_2+y_{22}\\
x_3+y_{31}&x_3+y_{32}\\
\end{pmatrix}
```

```python
# 同一行数で片方が一列
m1 = np.array([[1], [2], [3]]) # １列
m2 = np.array([[10, 20], [30, 40], [50, 60]]) # ３行２列
print("m1+m2")
print(m1+m2)
```