# Numpy four arithmetic operations

内容：

- [Numpy four arithmetic operations](#numpy-four-arithmetic-operations)
  - [Differences from list operations](#differences-from-list-operations)
  - [Array and scalar four arithmetic operations](#array-and-scalar-four-arithmetic-operations)
  - [Calculation of arrays of the same shape](#calculation-of-arrays-of-the-same-shape)
  - [arithmetic operation of arrays of different shapes](#arithmetic-operation-of-arrays-of-different-shapes)
    - [One line and a row](#one-line-and-a-row)
    - [One line in the same number of rows](#one-line-in-the-same-number-of-rows)
    - [One row in the same number of lines](#one-row-in-the-same-number-of-lines)

## Differences from list operations

**Note:**  
Even with the same formula, the calculation results may be different between the list and the array of Numpy.  When dealing with arrays, be careful whether it is **list or Numpy array**.

For example.

Prepare a list variable `a`, `b` and a variable `c`,`d` that converted it to Numpy Array.

```python
import numpy as np

a, b = [1, 2, 3, 4, 5], [6, 7, 8]
c = np.array(a)
d = np.array(b)

print('type a: ', type(a))
print('type b: ', type(b))
print('type c: ', type(c))
print('type d: ', type(d))
```

The list is added, but this does not get an error.  
New elements are added to the list.

```python
# List addition
a + b
```

Numpy array added, but this causes an error.  
It is because the number of elements is not the same.

```python
# Numpy array addition
c + d
```

Let's do the addition when the number of elements are same in the Numpy array.  
You can see that not a new element is added, but the addition is performed between the elements.

```python
# Numpy array addition (Part 2)
c + c
```

## Array and scalar four arithmetic operations

In this case, it is calculated for all the elements of the arrays and the scalar (a value composed of one value).

Calculation example:

```math
\begin{pmatrix}
x_{11}&x_{12} \\
x_{21}&x_{22}
\end{pmatrix}
+a
=
\begin{pmatrix}
x_{11}+a&x_{12}+a \\
x_{21}+a&x_{22}+a
\end{pmatrix}
```

```math
a/
\begin{pmatrix}
x_{11}&x_{12} \\
x_{21}&x_{22}
\end{pmatrix}
=
\begin{pmatrix}
a/x_{11}&a/x_{12} \\
a/x_{21}&a/x_{22}
\end{pmatrix}
```

```python
# addition and subtraction to scalar
m = np.array([[1,2,3], [4,5,6], [7,8,9]])
print('m')
print(m)
print()
print('m+10')
print(m+10)
print()
print('10-m')
print(10-m)
```

```python
# Multiplication and division to scalar
print('m*2')
print(m*2)
print()
print('m/10')
print(m/10)
print()
print('10/m')
print(10/m)
```

## Calculation of arrays of the same shape

In this case, the calculation is performed between the same position elements.  

Calculation example:

```math
\begin{pmatrix}
x_{11}&x_{12} \\
x_{21}&x_{22}
\end{pmatrix}
-
\begin{pmatrix}
y_{11}&y_{12} \\
y_{21}&y_{22}
\end{pmatrix}
=
\begin{pmatrix}
x_{11}-y_{11}&x_{12}-y_{12} \\
x_{21}-y_{21}&x_{22}-y_{22}
\end{pmatrix}
```

```math
\begin{pmatrix}
x_{11}&x_{12} \\
x_{21}&x_{22}
\end{pmatrix}
*
\begin{pmatrix}
y_{11}&y_{12} \\
y_{21}&y_{22}
\end{pmatrix}
=
\begin{pmatrix}
x_{11}y_{11}&x_{12}y_{12} \\
x_{21}y_{21}&x_{22}y_{22}
\end{pmatrix}
```

In the case of multiplication and division, this is the [Hadamard product](https://ja.wikipedia.org/wiki/%E3%82%A2%E3%83%80%E3%83%9E%E3%83%BC%E3%83%AB%E7%A9%8D).

```python
# Addition and subtraction between arrays of the same shape
m = np.array([[1,2,3], [4,5,6], [7,8,9]])
n = np.array([[10,20,30], [40,50,60], [70,80,90]])
print('m')
print(m)
print()
print('n')
print(n)
print()
print('m+n')
print(m+n)
print()
print('n-m')
print(n-m)
```

```python
# Multiplication and division between arrays of the same shape
print('m*n')
print(m*n)
print()
print('m/n')
print(m/n)
```

If you want to perform so-called inner product or matrix product operations, use numpy's `dot()` function.

```python
# Vector's inner product
v1 = np.array([1,2,3])
v2 = np.array([4,5,6])
print(v1.dot(v2))
print(np.dot(v1, v2))
```

```python
# matrix product
m1 = np.array([[1,2],[3,4]])
m2 = np.array([[5,6], [7,8]])
print('m1.dot(m2)')
print(m1.dot(m2))
print()
print('m2.dot(m1)') # 行列積は非可換
print(m2.dot(m1))
print()
print('np.dot(m1,m2)')
print(np.dot(m1,m2))
```

## arithmetic operation of arrays of different shapes

Even if it has a different shape, the arithmetic operation are possible in the following cases.

### One line and a row

```math
\begin{pmatrix}
x_{1}&x_{2}&x_3
\end{pmatrix}
+
\begin{pmatrix}
y_{1}\\y_{2}
\end{pmatrix}
=
\begin{pmatrix}
x_1+y_1&x_2+y_1&x_3+y_1\\
x_1+y_2&x_2+y_2&x_3+y_2
\end{pmatrix}
```

```python
# One line and a row
m1 = np.array([1, 2, 3]) # １line
m2 = np.array([[10], [20]]) # １row
print("m1+m2")
print(m1+m2)
```

### One line in the same number of rows

```math
\begin{pmatrix}
x_{1}&x_{2}&x_3
\end{pmatrix}
+
\begin{pmatrix}
y_{11}&y_{12}&y_{13}\\
y_{21}&y_{22}&y_{23}\\
\end{pmatrix}
=
\begin{pmatrix}
x_1+y_{11}&x_2+y_{12}&x_3+y_{13}\\
x_1+y_{21}&x_2+y_{22}&x_3+y_{23}
\end{pmatrix}
```

```python
# One line in the same number of rows
m1 = np.array([1, 2, 3]) # １line
m2 = np.array([[10, 20, 30], [40, 50, 60]]) # ２lines３rows
print("m1+m2")
print(m1+m2)
```

### One row in the same number of lines

```math
\begin{pmatrix}
x_1\\x_2\\x_3
\end{pmatrix}
+
\begin{pmatrix}
y_{11}&y_{12}\\
y_{21}&y_{22}\\
y_{31}&y_{32}\\
\end{pmatrix}
=
\begin{pmatrix}
x_1+y_{11}&x_1+y_{12}\\
x_2+y_{21}&x_2+y_{22}\\
x_3+y_{31}&x_3+y_{32}\\
\end{pmatrix}
```

```python
# One row in the same number of lines
m1 = np.array([[1], [2], [3]]) # １row
m2 = np.array([[10, 20], [30, 40], [50, 60]]) # ３lines２rows
print("m1+m2")
print(m1+m2)
```