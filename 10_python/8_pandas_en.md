# Pandas

[Pandas](https://pandas.pydata.org/) is a library that provides functions useful for data analysis such as data operation and analysis.  
Handle data using an object called a data frame.

Only the very basic parts can be introduced here.  
For other detailed functions, please refer to [Pandas](https://pandas.pydata.org/).  

Contents:

- [Pandas](#pandas)
  - [Creating data frames](#creating-data-frames)
  - [Data frame structure](#data-frame-structure)
  - [Data extraction](#data-extraction)
    - [Extracting rows by specified index](#extracting-rows-by-specified-index)
    - [Extract of columns by column specified](#extract-of-columns-by-column-specified)
    - [Data extraction by conditional specification](#data-extraction-by-conditional-specification)
  - [Read and write CSV file](#read-and-write-csv-file)
    - [writing](#writing)
    - [Reading](#reading)

## Creating data frames

- Module import

  ```python
  import pandas as pd
  ```

- Prepare data and column names
  
  ```python
    cols = ["氏名", "性別", "血液型", "身長", "体重", "役職"]
    dat = [
        ["田中太郎", "男性", "A型", 175.0, 70.1, "部長"], 
        ["山田花子", "女性", "B型", 162.5, 55.6, "課長"], 
        ["鈴木次郎", "男性", "O型", 180.2, 80.9, "プロジェクトマネージャー"], 
        ["小林さちこ", "女性", "AB型", 168.7, 63.4, "デザイナー"],
        ["伊藤一郎", "男性", "A型", 170.3, 68.5, "エンジニア"], 
        ["中村知美", "女性", "O型", 165.9, 58.7, "アシスタント"]
    ]
  ```

- Creating data frames

  ```python
    df = pd.DataFrame(data=dat, columns=cols)
  ```

Let's display the created data frame.

```python
# データフレームの表示
print(df)
```

```python
      氏名  性別  血液型     身長    体重            役職
0   田中太郎  男性   A型  175.0  70.1            部長
1   山田花子  女性   B型  162.5  55.6            課長
2   鈴木次郎  男性   O型  180.2  80.9  プロジェクトマネージャー
3  小林さちこ  女性  AB型  168.7  63.4         デザイナー
4   伊藤一郎  男性   A型  170.3  68.5         エンジニア
5   中村知美  女性   O型  165.9  58.7        アシスタント
```

As a different way of creating, for example, you can create an empty data frame first and add data later.

```python
# Create an empty data frame
df1 = pd.DataFrame()
```

```python
# Added name column
df1['氏名'] = ["田中太郎", "山田花子", "鈴木次郎", "小林さちこ", "伊藤一郎", "中村知美"]
print(df1)
```

```python
      氏名
0   田中太郎
1   山田花子
2   鈴木次郎
3  小林さちこ
4   伊藤一郎
5   中村知美
```

## Data frame structure

Data frames include index, columns and data.

![structure_df](./images/structure_df.png)

<span style="color: red">Red line: Index</span>
<span style="color: blue">Blue line: column name</span>
<span style="color: orange">Yellow Line: Data</span>

Let's extract the values of each place.
Each value can be extracted with `df.index`, `df.columns`, `df.values`.  

```python
print('index')
print(df.index)
print('')
print('column')
print(df.columns)
print('')
print('data')
print(df.values)
```

```python
index
RangeIndex(start=0, stop=6, step=1)

column
Index(['氏名', '性別', '血液型', '身長', '体重', '役職'], dtype='object')

data
[['田中太郎' '男性' 'A型' 175.0 70.1 '部長']
 ['山田花子' '女性' 'B型' 162.5 55.6 '課長']
 ['鈴木次郎' '男性' 'O型' 180.2 80.9 'プロジェクトマネージャー']
 ['小林さちこ' '女性' 'AB型' 168.7 63.4 'デザイナー']
 ['伊藤一郎' '男性' 'A型' 170.3 68.5 'エンジニア']
 ['中村知美' '女性' 'O型' 165.9 58.7 'アシスタント']]
```

The index does not need to be a positive integer.
Let's create data of Sin wave and Cos wave with the index as time.

```python
import numpy as np
cols = ['time', 'sin', 'cos']
dat = [[t, np.sin(t), np.cos(t)] for t in np.linspace(-1.0, 1.0, 20)]
dfn = pd.DataFrame(dat, columns=cols)
```

```python
# Display only the first part
dfn.head()
```

![dfn0](./images/dfn0.png)

Change the Time column to an index.

```python
# set time column as index.
# Do not create new data frames using inplace.
dfn.set_index('time', inplace=True)
```

```python
# Display only the first part
dfn.head()
```

![dfn1](./images/dfn1.png)

You can see that TIME columns, including negative numbers, are indexed.

## Data extraction

Data can be extracted according to the conditions.

### Extracting rows by specified index

It is possible to use slice (DF[Start: Stop: Step]).

```python
# Extract from index 1 to less than 5
df[1:5]
```

```python
  氏名  性別  血液型  身長  体重  役職
1 山田花子  女性  B型 162.5 55.6  課長
2 鈴木次郎  男性  O型 180.2 80.9  プロジェクトマネージャー
3 小林さちこ  女性  AB型  168.7 63.4  デザイナー
4 伊藤一郎  男性  A型 170.3 68.5  エンジニア
```

```python
# Extract by flying from the index 1 to less than 5
df[1:5:2]
```

```python
	氏名	性別	血液型	身長	体重	役職
1	山田花子	女性	B型	162.5	55.6	課長
3	小林さちこ	女性	AB型	168.7	63.4	デザイナー
```

```python
# Extract from the end to the third to the end with the index
df[-3:]
```

```python
	氏名	性別	血液型	身長	体重	役職
3	小林さちこ	女性	AB型	168.7	63.4	デザイナー
4	伊藤一郎	男性	A型	170.3	68.5	エンジニア
5	中村知美	女性	O型	165.9	58.7	アシスタント
```

Slice can be used even if the index is decimal.

```python
# Extract by slice when the index is decimal
dfn[-0.9:-0.5]
```

![dfn2](./images/dfn2.png)

However, in this case, the step cannot be specified.
For example, executing the following command will cause an error.

```python
# Extract by slice when the index is decimal
dfn[-0.9:-0.5:0.1]
```

### Extract of columns by column specified

When extracting one column, you can do it like `df['column name 1']` .

```python
# Extracting one column
df['氏名']
```

```python
0     田中太郎
1     山田花子
2     鈴木次郎
3    小林さちこ
4     伊藤一郎
5     中村知美
Name: 氏名, dtype: object
```

The object output is `pandas.core.series.series`.

```python
# Confirmation of output object
type(df['氏名'])
```

```python
pandas.core.series.Series
```

When extracting multiple columns, use a double square parentheses like `df[['column name 1', 'column name 2' ,,,,]]`.

```python
# Extracting multiple columns
df[['氏名', '性別']]
```

```python
    氏名  性別
0 田中太郎  男性
1 山田花子  女性
2 鈴木次郎  男性
3 小林さちこ  女性
4 伊藤一郎  男性
5 中村知美  女性
```

The object output is `pandas.core.frame.dataFrame`.

```python
# Confirmation of output object
type(df[['氏名', '性別']])
```

```python
pandas.core.frame.DataFrame
```

**Note:**  
Note that `df['Name']` and `df[['Name']]` both extract the 'Name' column, but the output objects are different. The former is a `Series` and the latter is a `DataFrame`.

### Data extraction by conditional specification

By using `df[conditional expression]`, you can extract data that matches the condition.

```python
# Extract data with male gender
df[df['性別'] == '男性']
```

```python
    氏名  性別  血液型  身長  体重  役職
0 田中太郎  男性  A型 175.0 70.1  部長
2 鈴木次郎  男性  O型 180.2 80.9  プロジェクトマネージャー
4 伊藤一郎  男性  A型 170.3 68.5  エンジニア
```

```python
# Extract data where the gender is male and the height is less than 180cm
df[(df['性別'] == '男性') & (df['身長'] < 180)]
```

```python
  氏名	性別	血液型	身長	体重	役職
0	田中太郎	男性	A型	175.0	70.1	部長
4	伊藤一郎	男性	A型	170.3	68.5	エンジニア
```

## Read and write CSV file

A csv (comma separated values) file is a file whose data is separated by commas (,).
Normally, reading and writing a file requires a steps such as opening, manipulating, and closing the file, as shown below.

```python
f = open('test.txt', 'w')
f.write('This is test.')
f.close()
```

If you use pandas DataFrame, you can easily read and write csv files using the `to_csv()` and `read_csv()` commands.  
Here, we will manipulate csv files using pandas DataFrame.

### writing

Use `to_csv ()` to write to CSV.

```python
# Output to CSV file
df.to_csv('persons_with_index.csv')
```

The `persons_with_index.csv` file will be created in the same directory as the execution directory.  
Please open the file and check the contents.

The data is separated by comma(,).
The index is also output to the CSV file.

If you do not output the index, make an option as follows.

```python
# Output to CSV file _index does not output
df.to_csv('persons_no_index.csv', index=False)
```

`to_csv ()` has many options.
For more information, see ["Pandas.dataFrame.to_csv"](https://pandas.pydata.org/docs/reference/pandas.dataFrame.to_csv.html).

### Reading

Use `read_csv ()` to read the CSV file.

When reading a CSV file containing the index, specify the column number for the index.

```python
# When reading files including index
# Specify the index column
pd.read_csv('persons_with_index.csv', index_col=0)
```

```python
	氏名	性別	血液型	身長	体重	役職
0	田中太郎	男性	A型	175.0	70.1	部長
1	山田花子	女性	B型	162.5	55.6	課長
2	鈴木次郎	男性	O型	180.2	80.9	プロジェクトマネージャー
3	小林さちこ	女性	AB型	168.7	63.4	デザイナー
4	伊藤一郎	男性	A型	170.3	68.5	エンジニア
5	中村知美	女性	O型	165.9	58.7	アシスタント
```

If it does not include an index, it will be as follows.

```python
# When reading files that do not include index
pd.read_csv('persons_no_index.csv')
```

`Read_csv ()` also has many options. 
For more information, please refer to [Pandas.Read_csv](https://pandas.pydata.org/docs/reference/api/pandas.Read_csv.html).  
