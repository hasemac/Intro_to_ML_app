# Variable and data structure

内容

- [Variable and data structure](#variable-and-data-structure)
  - [variable](#variable)
    - [Bool](#bool)
    - [numbers](#numbers)
      - [integer](#integer)
      - [float](#float)
      - [complex](#complex)
    - [string](#string)
      - [Connection and length](#connection-and-length)
      - [extracting character and strings](#extracting-character-and-strings)
  - [Type of data structure](#type-of-data-structure)
    - [List](#list)
    - [Tupple](#tupple)
    - [set](#set)
    - [dict](#dict)
  - [Operation of data structure](#operation-of-data-structure)
    - [Created (list comprehension)](#created-list-comprehension)
    - [extraction (index and slice)](#extraction-index-and-slice)
    - [Search (operator: in, not in)](#search-operator-in-not-in)
    - [Copy (Copy, DeepCopy)](#copy-copy-deepcopy)

## variable

The variables used in Python include bools, numbers, and strings.

### Bool

The bool type takes either True or False.

```python
#Key_bool
A, B, C = True, False, False
Print ('type (a)', type (a))
Print ('and:', a and b and c)
Print ('or', a or b or c)
```

```python
#KEY_BOOL_Q
# Problem: Please check the result of the following equation by execution.
A, B, C = True, False, False
# Q1: (a and b) or c
# Q2: (a or b) and not c
```

### numbers

Variables that can be added, subtracted, multiplied and divided.

#### integer

It is a numerical value without a decimal part. Int type variables.
Execute the code below to make sure that Type is int.

```python
# key_int
a, b = 1, 2
print(f'a: {a}, b: {b}')
print(type(a))
```

#### float

It is a numerical value with a decimal part. It is a float type variable.  
a = 1  
a = 1.0  
Then, depending on whether or not each has a decimal point, a different type, such as int type and float type, is defined.  
Execute the code below to make sure that Type is Float.  

```python
# key_float
a, b = 1.0, 2.1
print(f'a: {a}, b: {b}')
print(type(a))
```

#### complex

By attaching j at the end of the numerical values, you can represent imaginary numbers.
Execute the code below and make sure that Type is complex.

```python
# key_complex
a, b = 3j, 2.0+4.0j
print(f'a: {a}, b: {b}')
print(type(a), type(b))
```

### string

When defining a string, enclose it with '(single quotation) or "(double quotation).  
Execute the code below and make sure that Type is string.

```python
# key_str
s, t = "Apple", 'Banana'
print(f's: {s}, t: {t}')
print(type(s), type(t))
```

#### Connection and length

Use '*' or '+' when connecting the string.  
Use len() to get the length of the string.  
Execute the code below to make sure that the string has been connected and the length has been obtained.  

```python
# key_str_cl
s, t = "Apple", 'Banana'
u = s*2 + t
print(u)
print(len(u))
```

#### extracting character and strings

Use the index to extract a character, and use the slice to extract the partial string from the string.  
Execute the code below to make sure the text and character strings are extracted.

```python
# key_str_slice
s = "pineapple juice"
print('s[1]:', s[1]) # Extraction by index
print('s[4:9]', s[4:9]) # Extraction by slice
```

## Type of data structure

There are several types of data structure that can manage multiple values together.

### List

- The list is a data structure that can store multiple elements and enclosed in brackets'[ ]'.
- Each element can have a different type.
- Index and slice can be used.
- Element **can be** added, inserted, and deleted in list (**mutable**, can be changed).
- The connection of lists can be performed with +, +=, etc.

```python
# key_list
l = [1, 2, 3.0, 4.0, "apple", "banana"] # Definition of mixed types list
print(l)
print(type(l))

print('index', l[1]) # extraction by index
print('slice', l[2:5]) # extraction by slice

l.append('lemon') # Adding elements
print('append', l)

l.insert(4, 5.0) # Insert element
print('insert', l)

l.remove('apple') # Element deletion
print('remove', l)

print('len', len(l)) # List length

m = ['red', 'green']
n = l + m # List connection
print('conjunction', n)
```

### Tupple

- Thu is a data structure that can store multiple elements, and enclosed in brackets '( )'.
- Each element can have a different type.
- Index and slice can be used.
- Element **can not be** added, inserted, and deleted in list  (**Imutable**, cannot be changed).
- You can use a pack and an unpack.

```python
# key_tupple
t = (1, 2, 1.0, 2.0, 'apple', 'banana') # Definition of tuple with mixed types
print(t)
print('タイプ', type(t))

print('index', t[1]) # extraction by index
print('slice', t[2:5]) # extraction by slice

t2 = 1, 2, 3 # pack
print(t2)
print('タイプ', type(t2))

n1, n2, f1, f2 = (1, 2, 'apple', 'banana') # unpack
print('n1', n1, 'n2', n2, 'f1', f1, 'f2', f2)
```

### set

- The set is a data structure that can store multiple elements, and enclosed in brackets '{ }'.
- Each element can have a different type.
- **Duplicate elements cannot be stored**.
- **The order of the element is not guaranteed** (index, slices cannot be used).
- Element **can be** added, inserted, and deleted in set (**mutable**, can be changed).
- **Calculation for set** can be performed (intersection, union, difference, target difference).

```python
# key_set
s = {1, 2, 1.0, 2.0, 'apple', 'banana'} # 1, 1.0, 2 and 2.0 are considered to be duplicated.
print('set:', s)
print(type(s))

s |= {'lemon', 3} # Adding elements
print('app:', s)

s -= {'apple', 1}
print('subt:', s)

s1 = {1, 2, 3, 4, 5}
s2 = {3, 4, 5, 6, 7}

print('intersection:', s1 & s2) # intersection
print('union:', s1 | s2) # unioin
print('difference:', s1-s2) # difference
print('target_dif:', s1 ^ s2) # target difference
```

![set](./images/set.drawio.svg)

Tips: If you want to delete duplicate elements in the list, you can realize by converting them into a set.

```python
# key_set_tip
a = [1, 2, 3, 4, 5, 6, 2, 3, 4]
print(a)
b = list(set(a))
print(b)
```

### dict

- The elements are pairs of a key and a value, the key and the value are separated by ':', and enclosed in brackets '{ }'.
- Each element can have a different type.
- A elements that overlap the key cannot be stored.
- The order of the element is not guaranteed (index or slice cannot be used).
- Element **can be** added, inserted, and deleted in dict (**mutables**, can be changed).

```python
# key_dict
person = {'name': 'Mike', 
          'age': 18, 
          'height': 180.0, 
          'blood_type': 'AB', 
          'age': 25} # If there is a duplicated key, the one defined behind will be valid.
print('person:', person)
print('personのkey一覧:', person.keys()) # Display of key list

person['age'] = 20 # Change element
person['weight'] = 70 # Adding elements
print('person2:', person)

del person['height']
print('person3:', person)
```

## Operation of data structure

Here, the data structure is created (list comprehension), extraction (index and slice), search (operator: in, not in).

### Created (list comprehension)

By using the list comprehension, the data structure can be created simply.
The description methods are:

- ['element' for 'variable' in 'iterable]  
  <br>
- ['element' for 'variable' in 'iterable if 'evaluation']  
  When "evaluation" is true, "element" is added.  
  <br>
- ['element1' if 'evaluation' else 'element2' for 'variable' in 'iterable]  
  When "evaluation" is true, "element1" is added, or else "element2" is added.  
  "Ternary operator" is used.

```python
# key_listcomp

# Create elements in sequential
a = []
for e in range(10):
    a.append(2*e+1)
    
# create with list comprehension
b = [e for e in range(10)]

# print
print("a", a)
print("b", b)

# ifを用いた内包表記

# 偶数のみリストにする。
c = [e for e in range(10) if e % 2 == 0]
print('c', c)

# 偶数はそのまま、奇数の個所は'odd'とする。
d = [e if e % 2 ==0 else 'odd' for e in range(10)]
print('d', d)
```

### extraction (index and slice)

- Use index to extract the specified position element.
- Use slice to extract the specified several elements.
- Index and slice can be used for those that are in order, such as strings, lists, and tuples.
  
Index and slice numbers can take **negative values**.  
With a negative value, the index is counted from behind.  
Please refer to the usage example for details.  

```python
# key_slice

v = [1, 3, 5, 7, 9, 11, 13, 15]
# インデクスによる値の取り出し
print('v[0]:', v[0]) 
print('v[2]:', v[2]) 

# スライスによる値の取り出し
print('\nスライス')
print('v[2:4]:', v[2:4]) # インデクス２番目から３番目（４番目でない）まで取り出し
print('v[2:]:', v[2:]) # インデクス２番目から最後まで取り出し
print('v[:4]:', v[:4]) # インデクス最初から３番目（４番目でない）まで取り出し

# スライスでマイナスの値を使用して取り出し
print('\nスライスでマイナスの値')
print('v[2:-2]', v[2:-1]) # インデクス２番目から、最後から２番目（１番目でない）まで取り出し
print('v[-5:]', v[-5:]) # 最後から５番目から、最後まで取り出し

# スライスで次に取り出す要素を指定
print('\nスライスでとびとびの値を指定')
print('v[2::2]', v[2::2]) # インデクス２番目から最後まで一つ飛びで取り出し
# リストの反転
print('v[-1::-1]', v[-1::-1]) # インデクス最後から最初まで、ひとつづつ前に戻る。
```

```python
# key_slice_q
# 問題
v = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, ]
# 問題１：スライスを使って偶数を小さい順に抜き出してください。
# 問題２：スライスを使って奇数を大きい順に抜き出してください。
```

### Search (operator: in, not in)

You can check whether or not the specified element exists in the data structure using an operator.

```python
# key_in_notin
# listの場合
fruits = ['apple', 'pineapple', 'melon', 'orange', 'banana', 'lemon']
print('banana:', 'banana' in fruits)
print('banana:', 'banana' not in fruits)

print('peach:', 'peach' in fruits)
print('peach:', 'peach' not in fruits)

# 文字列の場合
print('\n文字列の場合')
s = 'This melon is sweet and delicious. Lemons are sour.'
print('melon:', 'melon' in s)
print('apple:', 'apple' in s)
print('That:', 'That' in s)
```

### Copy (Copy, DeepCopy)

Be careful when copying the data structure.
In the next case, you can see that changing fr1 has an effect on the first variable fr0.

```python
# key_copy1
fr0 = ['apple', 'pineapple', 'melon']
fr1 = fr0
fr1 += ['orange']

print(fr0)
print(fr1)
```

Use the copy method to avoid this.

```python
# key_copy2
fr0 = ['apple', 'pineapple', 'melon']
fr1 = fr0.copy()
fr1 += ['orange']

print(fr0)
print(fr1)
```

However, if the original data has a deep structure, it may still be affected as below.

```python
# key_copy3
fr0 = [['apple', 100], ['pineapple', 500], ['melon', 700]]
fr1 = fr0.copy()
fr1[2][1] = 600

print(fr0)
print(fr1)
```

To avoid this, use the DeepCopy method of the Copy module.

```python
# key_copy4
import copy # copyモジュールのインポート

fr0 = [['apple', 100], ['pineapple', 500], ['melon', 700]]
fr1 = copy.deepcopy(fr0)
fr1[2][1] = 600

print(fr0)
print(fr1)
```

As described above, when copying the data structure, it is necessary to use them in some cases.  
