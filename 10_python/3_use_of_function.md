# Use of functions

内容：

- [Use of functions](#use-of-functions)
  - [What is function?](#what-is-function)
  - [Definition of function](#definition-of-function)
  - [Importance of functions](#importance-of-functions)
  - [How to call the function](#how-to-call-the-function)
  - [Functions with default values in arguments](#functions-with-default-values-in-arguments)
  - [Note: Operation of arguments within the function](#note-operation-of-arguments-within-the-function)

## What is function?

A function is a set of processes that can be called.  
Functions are used to perform specific tasks or operations and help structure code, increase reusability, and improve maintainability.

## Definition of function

```python
def function_name(parameter1, parameter2, ...):
    # body of function（Processing implementation）
    # ...
    # Return the result as needed
    return result
```

- def  
  A keyword for defining functions.
- function_name  
Function name, use this name to call the function. Usually, make a function name that shows what kind of processing the function will do.
- parameter1, parameter2, ...  
  The argument of the function, the value passed to the function.
- body of function  
  With an indent, the code that the function processes is described.
- return result  
  Returns the result value of the function.

Create a function that returns the sum, receiving the two numbers arg1 and arg2, and call the function.  

```python
def add(arg1, arg2):
    answer = arg1 + arg2
    return answer

print('5+3: ', add(5, 3))
```

**Question**  
Receive the two string str1 and str2, create a function that returns a string connected to the string, and call the function.

## Importance of functions

Coding using a function is important from the following perspective.

- Reusability  
  By using a function, the same processing can be reused without writing many times.

- Maintainability  
  By using a function, the code is more structured, easier to change and modify, and improve maintenance.

- Readability  
  By giving proper names to function names and arguments, the code becomes closer to a more natural language, the purpose of processing becomes clearer, and the person who reads the code makes it easier to understand.

- A abstraction and hidden  
  Writing a series of processing is worse. Providing only the minimum necessary information to those who read code using functions can lead to improved readability.
  For example, suppose you only need to understand the process of "buying a book.". "Take out your wallet from your pocket.", "Open your wallet and check how much money you have.", "Take out the money you need to pay for the book.", "Take the change and put it in your wallet.", "Put your wallet in your pocket." Like these, if it is written in small chunks, it will be difficult to read.

## How to call the function

The function can be called without specifying the name of the argument, or with specifying the name of the argument.  

- When calling without specifying  
  The order of the argument must be in the order defined by the function.
- When calling with specifying  
  The order of the argument can be arbitrarily.

```python
def meal(food, drink, fruits):
    print('food', food)
    print('drink', drink)
    print('fruits', fruits)

# If you do not specify the argument name, 
# you need to make the same order as the definition of the function.
meal('bread', 'coffee', 'lemon')

# When specifying an argument name, the order is optional.
print('\n')
print('with name')
meal(fruits='lemon', food='bread', drink='coffee')

# All argument names must be given.
print('\n')
print('lost')
meal(fruits='lemon', food='bread')
```

## Functions with default values in arguments

You can set the default value for the argument as follows.  

```python
# When giving the default value to the argument
# The arguments with the default value should be
# in the order behind the arguments that are not held.
def meal(drink, fruits, food='bread'):
    print('food', food)
    print('drink', drink)
    print('fruits', fruits)

# The argument with the default value 
# can be executed without specifying the value.
meal('coffee', 'melon')
```

## Note: Operation of arguments within the function

Note that when composite objects (objects that include other objects such as lists and class instances) are used as an argument of a function, if the argument is operated within the function, it may change the value of the caller.

The following example is a function to set zero at first elements, but has changed to the `list1` value of the caller.  

```python
# return the value with the first element zero
def set_first_element_to_zero(param1):
    param2 = param1
    param2[0] = 0
    return param2

list1 = [[1,2,3], [4,5,6]]

print('1st: ', list1)
print('2nd: ', set_first_element_to_zero(list1))
print('3rd: ', list1)
```

To prevent the value of the caller from changing, it is necessary to operate after taking the argument Deepcopy.

```python
# return a value with a deep copy

import copy
def set_first_element_to_zero(param1):
    param2 = copy.deepcopy(param1)    
    param2[0] = 0
    return param2

list1 = [[1,2,3], [4,5,6]]

print('1st: ', list1)
print('2nd: ', set_first_element_to_zero(list1))
print('3rd: ', list1)
```
