# numpyの関数

ここでの紹介はごく一部です。他に、多くの関数があります。  
詳細は、[NumPy user guide](https://numpy.org/doc/stable/user/index.html#user)などをご参照ください。

内容：

- [numpyの関数](#numpyの関数)
  - [統計量に関する関数](#統計量に関する関数)
  - [最大・最小となるインデックス argmax(), argmin()](#最大最小となるインデックス-argmax-argmin)
  - [累積和 cumsum()](#累積和-cumsum)
  - [重複削除 unique()](#重複削除-unique)
  - [行列式と逆行列 det(), inv()](#行列式と逆行列-det-inv)
  - [行列の転置 transpose(), T](#行列の転置-transpose-t)
  - [固有値と固有ベクトル eig()](#固有値と固有ベクトル-eig)

## 統計量に関する関数

numpyには各種統計量を算出する関数が用意されています。

```python
import numpy as np
num = [np.random.randn() for e in range(10)] # 平均ゼロ標準偏差１の乱数を10個生成
num = np.array(num) # numpyアレイに変換

print("平均値:", np.mean(num)) # 平均値
print("中央値:", np.median(num)) # 中央値
print("最大値:", np.max(num)) # 最大値
print("最小値:", np.min(num)) # 最小値
print("合計:", np.sum(num)) # 合計
print("分散:", np.var(num)) # 分散
print("標準偏差:", np.std(num)) # 標準偏差
print()

# 次でも表せます。
print("平均値:", num.mean()) # 平均値
#print("中央値:", num.median()) # 中央値 # この属性は無いです。
print("最大値:", num.max()) # 最大値
print("最小値:", num.min()) # 最小値
print("合計:", num.sum()) # 合計
print("分散:", num.var()) # 分散
print("標準偏差:", num.std()) # 標準偏差
```

問題：
発生させる乱数の個数を増やしていき、平均ゼロ標準偏差１になっていくことを確認してください。

## 最大・最小となるインデックス argmax(), argmin()

```python
# 最大値をとるインデックス
print('i_max: ', num.argmax())
print(num[num.argmax()])

# 最小値をとるインデックス
print('i_min: ', num.argmin())
print(num[num.argmin()])
```

## 累積和 cumsum() 

```python
# 累積和
a = np.arange(10)
print('a: ', a)
print('累積和：', a.cumsum())
```

## 重複削除 unique() 

```python
# 重複削除
a = np.array([1,2,7,3,3,4,5,5,6,7,8,9,3,10])
print('a: ', a)
print('重複削除：', np.unique(a))
```

## 行列式と逆行列 det(), inv()

```python
# 行列式と逆行列
m0 = [np.random.rand() for e in range(9)]
m1 = np.array(m0)
m2 = m1.reshape(3, 3) # ３行３列の行列作成

print('m2: ')
print(m2)
print()
print('m2の行列式: ', np.linalg.det(m2))
print()
m3 = np.linalg.inv(m2) # m2の逆行列
print('m2の逆行列: ', m3)
print()
print('逆行列であることの確認：')
m4 = np.dot(m2, m3)
print(m4)
print()

# 微小な値を無視する関数の定義
def round_np(nparray):
    sh = nparray.shape
    res = np.array( # 小数点10桁で四捨五入
        [round(e, 10) for e in nparray.reshape(-1)]
        )
    return res.reshape(sh)

# 微小な値を無視
print('逆行列(微小な値無視)：')
print(round_np(m4))
```

## 行列の転置 transpose(), T

```python
# 行列の転置
m = np.array([[1,2,3],
              [4,5,6],
              [7,8,9]]
             )
print('m')
print(m)
print()
print('転置その１')
print(m.transpose())
print()
print('転置その２')
print(m.T)
```

## 固有値と固有ベクトル eig()

```python
# 固有値と固有ベクトル
m = np.array([[1,2,3],[1,4,2],[3,1,2]]) # ３行３列の行列作成
eig_val, eig_vec = np.linalg.eig(m) # 固有値eig_valと固有ベクトルeig_vecを取得

print('m: ')
print(m)
print()
print('固有値：', eig_val)
print('固有ベクトル')
print(eig_vec)
```

```python
m: 
[[1 2 3]
 [1 4 2]
 [3 1 2]]

固有値： [ 6.37575386 -1.44214617  2.06639231]
固有ベクトル
[[-0.53726405 -0.73277291  0.26526281]
 [-0.66397205 -0.112016   -0.75606366]
 [-0.52008505  0.67119019  0.59833384]]
```

このとき例えば、最初の固有値`6.376`に対する固有ベクトルは、固有ベクトルの最初の行ではなくて、最初の列`(-0.537, -0.664, -0.520)`であることに注意してください。  

固有値と固有ベクトルであることを確認します。

```python
# 固有値と固有ベクトルであることの確認
v1 = eig_vec[:, 0] # 最初の固有ベクトルである最初の列を取得
print('行列と固有ベクトル:', m.dot(v1))
print('固有値と固有ベクトル:', eig_val[0]*v1) 
```

行列$M$の固有値を$\lambda_i$、固有ベクトルを$\bold{e}_i$として、固有ベクトルを並べた行列$N=(\bold{e}_1 \: \bold{e}_2 \: \cdots \: \bold{e}_n)$とします。  
このとき、次の式が成り立ちます。  

```math
\begin{align}
MN &=(\lambda_1 \bold{e}_1 \: 
\lambda_2 \bold{e}_2 \: 
\cdots \: 
\lambda_n \bold{e}_n) \\
&=(\bold{e}_1 \: \bold{e}_2 \: \cdots \: \bold{e}_n)
\begin{pmatrix}
\lambda_1 & 0 & \cdots & 0 \\
0 & \lambda_2 & \ddots & \vdots \\
\vdots & \ddots & \ddots & 0 \\
0 & \cdots & 0 & \lambda_n
\end{pmatrix}
\end{align}
```

従って、対角化が行えます。

```math
N^{-1}MN = 
\begin{pmatrix}
\lambda_1 & 0 & \cdots & 0 \\
0 & \lambda_2 & \ddots & \vdots \\
\vdots & \ddots & \ddots & 0 \\
0 & \cdots & 0 & \lambda_n
\end{pmatrix}
```

行列$M$を対角化してみます。

```python
# 対角化
ml = np.linalg.inv(eig_vec).dot(m).dot(eig_vec)
print(round_np(ml)) # 小さな値は無視して表示
```

対角成分が固有値であることが分かります。