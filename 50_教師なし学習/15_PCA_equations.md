# Principal Component Analysis

$X[n, p]$: n data in p dimensions  
$\Sigma[p, p]$: Covariance matrix of $X$  
The components of the covariance matrix $\Sigma$ of $X$ are given by

```math
\sigma_{jk}=\frac{1}{n} \sum_{i=1}^{n}(x_{ij}-\bar{x}_j)(x_{ik}-\bar{x}_k)\\
j,k=1～p
```

$W[p, q]$: Projection matrix to convert from p-dimensional to q-dimensional  
$Y[n, q]$: n data reduced to q dimensions

```math
Y[n,q]=X[n,p]W[p,q]
```

$S[q,q]$: Covariance matrix of $Y$  
The components of the covariance matrix $S$ of $Y$ are given by

```math
s_{jk}=\frac{1}{n} \sum_{i=1}^{n}(y_{ij}-\bar{y}_j)(y_{ik}-\bar{y}_k)\\
j, k = 1～q
```

Thereafter, we use the contraction notation (same subscripts in the same term are summed).  
At this point, we obtain the following equation

```math
y_{ij} = \sum_{k=1}^{p}x_{ik}w_{kj}=x_{ik}w_{kj}
 \qquad i \leq n, \quad j \leq q
```

```math
\begin{align*}
\bar{y}_j
&=\frac{1}{n}\sum_{i=1}^n y_{ij}\\
&=\frac{1}{n}\sum_{i=1}^n x_{ik}w_{kj}
&=\bar{x}_{k} w_{kj}
\end{align*}
```

Again, consider the covariance matrix of $Y$.

```math
\begin{align*}
s_{jk}
&=\frac{1}{n} \sum_{i=1}^{n}(x_{il}w_{lj}-\bar{x}_{l}w_{lj})(x_{im}w_{mk}-\bar{x}_{m}w_{mk})\\
&=w_{lj}w_{mk} \; \frac{1}{n} \sum_{i=1}^{n} (x_{il}-\bar{x}_l) (x_{im}-\bar{x}_m)\\
&=w_{lj}w_{mk} \sigma_{lm}
\end{align*}
```

Eventually, the covariance matrix $S$ of $Y$ becomes the relation

```math
S[q,q]=W^T \Sigma W
```

Since $\Sigma$ is the covariance matrix of $X$, it is a real symmetric matrix and its eigenvectors are orthogonal to each other.  
In order of increasing eigenvalues of $\Sigma$, take q eigenvectors and define $W$ to be the column vector of the eigenvectors. And for simplicity, normalize $W$ as follows ($E$ is the unit matrix).

```math
W^TW=E
```

At this point, the following equation holds

```math
S[q,q] = 
\begin{pmatrix}
\lambda_0 & 0 & \cdots & 0\\
0 & \lambda_1 & \cdots & 0\\
\vdots & \vdots & \ddots & \vdots \\
0 & 0 & \cdots & \lambda_q \\
\end{pmatrix}
```

That is, each dimension (component) of $Y[n,q]$ of $n$ data reduced to $q$ dimensions by the $Y=XW$ relation is orthogonal (zero correlation coefficient) to each other and its variance is given by the eigenvalues of $\Sigma$.  
