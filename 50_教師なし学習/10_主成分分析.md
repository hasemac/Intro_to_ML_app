# 主成分分析

## 概要

主成分分析（Principal Component Analysis, PCS）の、主な目的はデータの次元削減です。  
一般的に、多くの場合において特徴量にはある程度の相関を含みます。  
例えば身長と体重のデータの散布図ですが、身長が分かると体重もある程度分かります。

![h_w](./images/height_weight.drawio.svg)

主成分分析では、データに線形変換を施し、互いに相関のない新たな特徴量を定義します。  
図で言えば、第一主成分を軸とした新たな特徴量と、それに直交する第二主成分を軸とした別の新たな特徴量が定義されます。
第一主成分の値から、おおよその身長と体重が求まり、第二主成分の値から平均からのズレ、つまり、やせ型かそうでないかが求まります。

特徴量の種類が多い場合に、主成分分析は、互いに相関の無い新しい特徴量に変換して、少ない種類でデータを説明するために用いられます。　　

## 要約

$X[n, p]$: $p$次元の$n$個のデータ
$\Sigma[p, p]$: $X$の共分散行列
に対して、
$\Sigma[p, p]$の$p$個の固有ベクトルの内、固有ベクトルを$q$個取り出し、それを列ベクトルに持つ射影行列を$W[p,q]$とします。
このとき$q$次元に削減された新しく定義されるデータ

```math
Y[n,q]=X[n,p]W[p,q]
```

は互いに相関の無いデータになります。

## 説明

線形変換をして新しい特徴量を計算する方法に関して説明します。  
記号の定義を次の通りにします。  

$X[n, p]$: $p$次元の$n$個のデータ
$\Sigma[p, p]$: $X$の共分散行列
$W[p, q]$: $p$次元から$q$次元に変換するための射影行列
$Y[n, q]$: $q$次元に削減されたn個のデータ
$S[q,q]$: $Y$の共分散行列

もともと$p$個の特徴量を持つデータ$X[n,p]$に対して、$q$個の特徴量に次元削減したデータ$Y[n,q]$が次の式で定義されます。  

```math
Y[n,q]=X[n,p]W[p,q]
```

このとき、$Y$の共分散行列$S$と$X$の共分散行列$\Sigma$には次の関係が成り立ちます(別ファイル参照)。  

```math
S[q,q]=W^T \Sigma W
```

$\Sigma[p,p]$は共分散行列であって実対称行列なので、$p$個の固有ベクトルをもち、それらは互いに直交します。  

$p$個の固有ベクトルの内、固有値の大きい順に固有ベクトルを$q$個取り出し、それを列ベクトルに持つ行列を$W[p,q]$とします。  

```math
W[p,q] = 
\begin{pmatrix}
\boldsymbol{v}_1 & \boldsymbol{v}_2 & \cdots & \boldsymbol{v}_q
\end{pmatrix}
```

ここで、$v_i$は$\Sigma$の固有値$\lambda_i$の固有ベクトルであって、$p$次元をもちます。

```math
\Sigma \boldsymbol{v}_i = \lambda_i \boldsymbol{v}_i
```

$q$個の固有ベクトルは互いに直交していますが、次の式になるよう正規化しておきます（$E$は単位行列）。

```math
W^TW=E
```

このとき、$Y$の共分散行列$S$は次のように式変形できます。

```math
\begin{align*}
S[q,q]
& =W^T \Sigma W \\
& = 
W^T
\Sigma
\begin{pmatrix}
\boldsymbol{v}_1 & \boldsymbol{v}_2 & \cdots & \boldsymbol{v}_q
\end{pmatrix}\\
& = 
\begin{pmatrix}
\boldsymbol{v}_1^T\\
\boldsymbol{v}_2^T\\
\vdots\\
\boldsymbol{v}_q^T
\end{pmatrix}
\begin{pmatrix}
\lambda_1 \boldsymbol{v}_1 & \lambda_2 \boldsymbol{v}_2 & \cdots & \lambda_q \boldsymbol{v}_q
\end{pmatrix}
\end{align*}
```

従って結局、次の式が成り立ちます。

```math
S[q,q] = 
\begin{pmatrix}
\lambda_1 & 0 & \cdots & 0\\
0 & \lambda_2 & \cdots & 0\\
\vdots & \vdots & \ddots & \vdots \\
0 & 0 & \cdots & \lambda_q \\
\end{pmatrix}
```

つまり、 $q$ 次元に削減された $n$ 個のデータの $Y[n,q]$ の各次元（成分）は互いに直交（共分散行列$S$の相関係数がゼロ）していて、その分散は $\Sigma$ の固有値で与えられていることがわかります。  

## 実際の計算手順

1. データセット$X$を標準化（平均ゼロ、標準偏差１）して$X_{std}[n, p]$を計算
2. $X_{std}$の共分散行列$\Sigma[p,p]$を計算
3. $\Sigma[p,p]$の固有値と固有ベクトルを算出
4. 固有値の大きい順に固有ベクトルを$q$個($<p$)とりだして射影行列$W[p,q]$を作成
5. q次元に削減された新しいデータセット$Y[n,q]=X[n,p]W[p,q]$を計算

次元削減した新しいデータセット$Y$が得られたら、これ以降はこのデータセットを用いて他の機械学習などをしていきます。  

ただし実際にはscikit-learnでこれらの手順をまとめて行う関数などが提供されています。  
