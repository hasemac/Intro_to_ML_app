# k-means法

k-means方は、クラスタリングを行う代表的な手法の一つです。そのアルゴリズムはシンプルであり、幅広い分野で使われています。

- クラスタリング  
  クラスタリングは、データ間の距離に基づいて、類似するデータを同じグループにまとめることです。  
  教師あり学習の分類と大きく異なる点は、正解ラベルが存在しない点です。  
  つまり、クラスタリングでまとまったグループは、どのような類似性でグループになったのか、データを人間が見て判断する必要があります。

## アルゴリズム

k個のクラスタに分類するとします。それぞれのクラスの代表的なデータ(重心)があったとして、その重心に近いデータをそのクラスタに所属させていく方法です。
具体的には次の通りになります。  

0. クラスタの数に等しい数の重心 (centroid) を設定します。  
   ここでは、３つのクラスタにまとめることを考えます。  

   ![km0](./images/km0.png)

1. 重心（centroid）に近いデータ点をそのクラスタにまとめます。
   ![km1](./images/km1.png)

2. クラスタ内のデータ点から新たな重心を算出します。
   ![km2](./images/km2.png)

    ```math
    \mu=\frac{x_1+x_2+\cdots+x_n}{n}
    ```

    以降、手順1. 2. を繰り返していきます。

   以上のような手順を踏み、重心がほとんど動かなくなったら収束したとして計算を終了します。  

## 具体例

scikit-learnの[KMeans](https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html)を用いて、k-means法を使用してみます。

scikit-learnの[make_blobs](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.make_blobs.html)を用いて、k-means法に適用するデータを作成して散布図として確認します。  
特徴量の数を２，ブロッブ数（クラスタ数に相当）を３に設定しています。  
クラスタの分布の範囲は`cluster_std`を調節することで行えます。

```python
# k-means用のデータを作成する。
from sklearn import datasets
X, y = datasets.make_blobs(
    n_samples=100, 
    n_features=2, 
    centers=3, # number of clusters
    #cluster_std=0.5, # standard dev of cluster
    random_state=0
    )
```

```python
# 作成したデータを散布図として表示
import matplotlib.pyplot as plt
plt.figure(figsize=(3, 3))
plt.scatter(X[:,0], X[:,1])
plt.show()
```

![km_scatter](./images/km_scatter.png)

scikit-learnの[KMeans](https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html)を用いてクラスタリングをしてみます。  

```python
# k-means法でクラスタリング
from sklearn.cluster import KMeans
KM = KMeans(
    n_clusters=3, # クラスターの数
    init = 'random', # 初期重心位置の設定方法：'random', 'k-means++', etc.
    n_init = 'auto', # 初期重心位置を変えて何回計算するか
    random_state=0
    )
KM.fit(X)
print(KM.predict(X))
```

可視化します。

```python
# 可視化
import sub
sub.draw_kmeans(X, KM)
```

![km_test](./images/km_test.png)

どの程度クラスタリングが良くできたかを測る指標として、クラスタ内誤差平方和（SSE, Sum of Squared Errors of predicition）があります。
クラスタ$i$の重心を$\mu^{(i)}$、そのクラスタに所属しているデータを$x^{(i)}_j$としたとき、SSEは次の式で得られます。  

```math
SSE = \sum_{i=1}^{k} \sum_{j=1}^{l}||x^{(i)}_j - \mu^{(i)}||^2
```

定義式から、SSEの値が小さいほど、良いクラスタリングということができます。  
SSEはクラスタの慣性（cluster inertia）とも呼ばれます。値を確認します。  

```python
# SSEの確認
print('SSE: ', KM.inertia_)
```

重心 (centroids) の位置も確認しておきます。

```python
# Confirmation of centroids
print('centroids:', KM.cluster_centers_)
```

## エルボー法

先ほどのSSEは、クラスタ数が大きくなればなるほど値が小さくなっていくことが予想できます。クラスタ数を増やしていってSSEの変化をみることで、適切なクラスタ数を決めるというエルボー法があります。

クラスタ数を変化させていった時のSSEの値を取得して、可視化します。

```python
# クラスタ数を1から10まで増やしていった時のSSEを取得
distortion = []
for i in range(1,11):
    KM = KMeans(n_clusters=i, random_state=0, n_init='auto')
    KM.fit(X)
    distortion.append(KM.inertia_)
```

```python
# 可視化
plt.figure(figsize=(4,3))
plt.plot(range(1,11), distortion, marker='o')
plt.xlabel('クラスタ数')
plt.ylabel('Distortion')
plt.show()
```

![elbow](./images/elbow.png)

エルボーとはその名の通り、ひじのように曲がった部分です。  
この曲線が大きく曲がったところ（クラスタ数３？）が、おおよそ適切なクラスタ数という事になります。

問題：
k-means用のデータ生成においてブロッブ数を2, 3, 4として、エルボー法で適切なクラスタ数を検討してください。  

## 初期値の問題

次は同じデータに対して、重心の初期位置を変えた場合の例です。  
ここでは分かりやすくするために初期位置を各々、固定しています。  

次の例は重心(centroid)の初期位置を(-1, 1)と(1, 1)にしています。クラスタリングしたあとは、それらの位置が(-1.4, 3.2)と(1.8, 2.6)とに移動しています。  

```python
centroids = [[-1, 1], [1,1]]
KM = KMeans(n_clusters=2, init=centroids, random_state=0, n_init='auto')
KM.fit(X)
sub.draw_kmeans(X, KM)
```

![km_init_1](./images/km_init_1.png)

次の例は重心(centroid)の初期位置を(0, 2)と(0, 1)にしています。  

```python
centroids = [[0,2], [0, 1]]
KM = KMeans(n_clusters=2, init=centroids, random_state=0, n_init='auto')
KM.fit(X)
sub.draw_kmeans(X, KM)
```

![km_init_2](./images/km_init_2.png)

クラスタリングの結果が異なっていることが分かります。  
k-means法では、重心の初期位置が変わると、クラスタリングの結果が変わることがあることに注意してください。  

## k-means++法

k-means法は、重心の初期値の取り方によって結果が異なる場合があるというものでした。  
k-means++法は、k-means法において重心の初期値の取り方を工夫したもので、初期の重心点は互いに離れている方が良いという考えに基づいています。  

0. ランダムに重心点を一つ設定する。
1. 全てのデータ点$x_i$に対して最近傍の重心点までの距離$D(x_i)$を計算する。  
2. データ点$x_i$が選ばれる確率を計算する。

   ```math
   \frac{D(x_i)^2}{\sum_j D(x_j)^2}
   ```

   重心点までの距離が短い場合は、選ばれる確率が小さくなっていることが分かります。  
   逆に遠い場合は、選ばれる確率が高くなっています。  
3. 確率に従って新たな重心点を決定する。  

以上のような操作を繰り返して、クラスタ数に等しい重心の初期値を求めていきます。  

実際にk-means++法で、初期値を決定してクラスタリングを行っています。  

```python
KM = KMeans(n_clusters=2, init='k-means++', random_state=0, n_init='auto')
KM.fit(X)
sub.draw_kmeans(X, KM)
```

![km++](./images/km++.png)

問題：
何回かk-means++法を実行してみて、クラスタリングが変わるか変わらないかを確認してください。（random_stateのオプションは無効にしてください。）  
