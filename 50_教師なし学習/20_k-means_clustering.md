# k-means clustering

The k-means clustering is one of the leading methods for clustering. Its algorithm is simple and is used in a wide range of fields.

- Clustering  
  Clustering is the grouping of similar data into the same group based on the distance between the data.  
  The major difference from supervised learning classification is that there are no correct labels.  
  This means that groups grouped together by clustering require a human to look at the data to determine by what similarity they were grouped.

## Algorithm

Suppose we want to classify the data into k clusters. Assuming that there is representative data (center of gravity) for each class, the method is to assign the data closest to the center of gravity to that cluster.
Specifically, the method is as follows.  

0. Set the number of centers of gravity (centroid) equal to the number of clusters.  
   In this example, we will consider grouping the clusters into three clusters.  

   ![km0](./images/km0.png)

1. Group the data points near the center of gravity (centroid) into that cluster.
   ![km1](./images/km1.png)

2. A new center of gravity is calculated from the data points in the cluster.
   ![km2](./images/km2.png)

    ```math
    \mu=\frac{x_1+x_2+\cdots+x_n}{n}
    ```

    Thereafter, repeat steps 1. 2.

   After following the above steps, the calculation is terminated when the center of gravity hardly moves at all, assuming convergence.  

## Example

Try using the k-means method with [KMeans](https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html) in scikit-learn.

Using scikit-learn's [make_blobs](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.make_blobs.html), create the data to be applied to the k-means method and check it as a scatter plot.  
The number of features is set to 2 and the number of blobs (corresponding to the number of clusters) to 3.  
The range of cluster distribution can be done by adjusting `cluster_std`.  

```python
# k-means用のデータを作成する。
from sklearn import datasets
X, y = datasets.make_blobs(
    n_samples=100, 
    n_features=2, 
    centers=3, # number of clusters
    #cluster_std=0.5, # standard dev of cluster
    random_state=0
    )
```

```python
# 作成したデータを散布図として表示
import matplotlib.pyplot as plt
plt.figure(figsize=(3, 3))
plt.scatter(X[:,0], X[:,1])
plt.show()
```

![km_scatter](./images/km_scatter.png)

Clustering using [KMeans](https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html) in scikit-learn.  

```python
# k-means法でクラスタリング
from sklearn.cluster import KMeans
KM = KMeans(
    n_clusters=3, # Number of clusters
    init = 'random', # How to set the initial center of gravity：'random', 'k-means++', etc.
    n_init = 'auto', # How many calculations with different initial center of gravity positions
    random_state=0
    )
KM.fit(X)
print(KM.predict(X))
```

Visualize the results.

```python
# 可視化
import sub
sub.draw_kmeans(X, KM)
```

![km_test](./images/km_test.png)

The Sum of Squared Errors of prediction (SSE) is a measure of how well the clustering was done.
When the center of gravity of a cluster $i$ is $\mu^{(i)}$ and the data belonging to that cluster is $x^{(i)}_j$, SSE is obtained by the following formula.  

```math
SSE = \sum_{i=1}^{k} \sum_{j=1}^{l}||x^{(i)}_j - \mu^{(i)}||^2
```

From the definition formula, the smaller the value of SSE, the better the clustering.  
SSE is also called cluster inertia. Check the value.  

```python
# SSEの確認
print('SSE: ', KM.inertia_)
```

The location of the center of gravity (centroids) should also be checked.

```python
# Confirmation of centroids
print('centroids:', KM.cluster_centers_)
```

## Elbow method

It can be expected that the value of SSE will decrease as the number of clusters increases. The elbow method is used to determine the appropriate number of clusters by increasing the number of clusters and observing the change in SSE.

Obtain and visualize the value of SSE as the number of clusters is varied.

```python
# クラスタ数を1から10まで増やしていった時のSSEを取得
distortion = []
for i in range(1,11):
    KM = KMeans(n_clusters=i, random_state=0, n_init='auto')
    KM.fit(X)
    distortion.append(KM.inertia_)
```

```python
# 可視化
plt.figure(figsize=(4,3))
plt.plot(range(1,11), distortion, marker='o')
plt.xlabel('クラスタ数')
plt.ylabel('Distortion')
plt.show()
```

![elbow](./images/elbow.png)

The elbow is, as the name implies, a curved part like an elbow.  
The point where this curve bends significantly (cluster number 3?) is the approximate appropriate number of clusters.

Question：
Consider the appropriate number of clusters using the elbow method with the number of blobs as 2, 3, or 4 in generating data for k-means.  

## Initial Value Issues

The following is an example of changing the initial position of the center of gravity for the same data.  
For the sake of clarity, the initial position is fixed in each case.  

The following example shows the initial positions of the center of gravity (centroid) at (-1, 1) and (1, 1). After clustering, their positions are moved to (-1.4, 3.2) and (1.8, 2.6).  

```python
centroids = [[-1, 1], [1,1]]
KM = KMeans(n_clusters=2, init=centroids, random_state=0, n_init='auto')
KM.fit(X)
sub.draw_kmeans(X, KM)
```

![km_init_1](./images/km_init_1.png)

The following example shows the initial position of the center of gravity (centroid) at (0, 2) and (0, 1).  

```python
centroids = [[0,2], [0, 1]]
KM = KMeans(n_clusters=2, init=centroids, random_state=0, n_init='auto')
KM.fit(X)
sub.draw_kmeans(X, KM)
```

![km_init_2](./images/km_init_2.png)

You can see that the clustering results are different.  
Note that in the k-means method, the clustering results can change when the initial position of the center of gravity changes.  

## k-means++ method

The k-means method was based on the idea that the results may differ depending on how the initial values of the centers of gravity are taken.  
The k-means++ method is a modified version of the k-means method, based on the idea that the initial center-of-gravity points should be farther apart from each other.  

0. set one center-of-gravity point at random.  
1. Calculate the distance $D(x_i)$ to the nearest neighbor center for all data points $x_i$.  
2. Calculate the probability that data point $x_i$ is selected.

   ```math
   \frac{D(x_i)^2}{\sum_j D(x_j)^2}
   ```

   We can see that if the distance to the center of gravity is short, the probability of being selected is smaller.  
   Conversely, when the distance is far, the probability of selection is higher.  

3. Determine the new center-of-gravity point according to probability.  

The above operations are repeated to find the initial value of the center of gravity equal to the number of clusters.  

The k-means++ method is actually used to determine initial values for clustering.  

```python
KM = KMeans(n_clusters=2, init='k-means++', random_state=0, n_init='auto')
KM.fit(X)
sub.draw_kmeans(X, KM)
```

![km++](./images/km++.png)

Question：
Run the k-means++ method several times and see if the clustering changes or not.  
(The random_state option must be disabled.)  
