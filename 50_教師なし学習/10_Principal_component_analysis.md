# Principal Component Analysis

## Summary

The main goal of Principal Component Analysis (PCA) is to reduce the dimensionality of the data.  
In general, in many cases, features include some degree of correlation.  
For example, in a scatter plot of height and weight data, if we know the height, we can also know the weight to some extent.

![h_w](./images/height_weight_en.drawio.svg)

In principal component analysis, a linear transformation is applied to the data to define new features that are uncorrelated with each other.  
In the figure, a new feature is defined around the first principal component and another new feature around the orthogonal second principal component.
The value of the first principal component gives the approximate height and weight, and the value of the second principal component gives the deviation from the average, that is, whether the person is thin or not.

When there are many types of features, principal component analysis is used to explain the data with fewer types by transforming them into new features that are uncorrelated with each other.　　

## Description

This section describes how to compute new features by linear transformation.  
The symbols are defined as follows  

$X[n, p]$: $n$ data in $p$ dimensions
$\Sigma[p, p]$: Covariance matrix of $X$.
$W[p, q]$: Projection matrix to convert from $p$-dimension to $q$-dimension
$Y[n, q]$: n data reduced to $q$ dimensions
$S[q,q]$: Covariance matrix of $Y$.

For data $X[n,p]$ originally having $p$ features, data $Y[n,q]$ with dimensionality reduced to $q$ features is defined by the following formula.  

```math
Y[n,q]=X[n,p]W[p,q]
```

In this case, the following relationship holds for the covariance matrix $S$ of $Y$ and the covariance matrix $\Sigma$ of $X$(See separate file).  

```math
S[q,q]=W^T \Sigma W
```

Since $\Sigma[p,p]$ is a covariance matrix and a real symmetric matrix, it has $p$ eigenvectors, which are orthogonal to each other.  

Among $p$ eigenvectors, take $q$ eigenvectors in order of increasing eigenvalue, and let $W[p,q]$ be the matrix whose column vectors are the eigenvectors.  

```math
W[p,q] = 
\begin{pmatrix}
\boldsymbol{v}_1 & \boldsymbol{v}_2 & \cdots & \boldsymbol{v}_q
\end{pmatrix}
```

where $v_i$ is the eigenvector of the eigenvalue $\lambda_i$ of $\Sigma$ and has dimension $p$.

```math
\Sigma \boldsymbol{v}_i = \lambda_i \boldsymbol{v}_i
```

The $q$ eigenvectors are orthogonal to each other, but we normalize them so that we have the following equation ($E$ is the unit matrix).

```math
W^TW=E
```

In this case, the covariance matrix $S$ of $Y$ can be transformed into the formula as follows

```math
\begin{align*}
S[q,q]
& =W^T \Sigma W \\
& = 
W^T
\Sigma
\begin{pmatrix}
\boldsymbol{v}_1 & \boldsymbol{v}_2 & \cdots & \boldsymbol{v}_q
\end{pmatrix}\\
& = 
\begin{pmatrix}
\boldsymbol{v}_1^T\\
\boldsymbol{v}_2^T\\
\vdots\\
\boldsymbol{v}_q^T
\end{pmatrix}
\begin{pmatrix}
\lambda_1 \boldsymbol{v}_1 & \lambda_2 \boldsymbol{v}_2 & \cdots & \lambda_q \boldsymbol{v}_q
\end{pmatrix}
\end{align*}
```

Thus, we end up with the following equation

```math
S[q,q] = 
\begin{pmatrix}
\lambda_1 & 0 & \cdots & 0\\
0 & \lambda_2 & \cdots & 0\\
\vdots & \vdots & \ddots & \vdots \\
0 & 0 & \cdots & \lambda_q \\
\end{pmatrix}
```

That is, each dimension (component) of $Y[n,q]$ of $n$ data reduced to $q$ dimensions is orthogonal to each other (zero correlation coefficient in the covariance matrix $S$) and its variance is given by the eigenvalues of $\Sigma$.  

## Actual calculation procedure

1. Calculate $X_{std}[n, p]$ by standardizing (mean zero, standard deviation 1) the data set $X$.
2. Compute the covariance matrix $\Sigma[p,p]$ of $X_{std}$.
3. $\Sigma[p,p]$の固有値と固有ベクトルを算出
4. Take $q$ eigenvectors ($<p$) in order of increasing eigenvalue and create a projection matrix $W[p,q]$.
5. Compute a new dataset $Y[n,q]=X[n,p]W[p,q]$ reduced to q dimensions

Once a new dataset $Y$ with dimensionality reduction is obtained, this dataset will be used for other machine learning and so on.  

In practice, however, scikit-learn provides functions to perform these procedures together.  
