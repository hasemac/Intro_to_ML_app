# Principal Component Analysis

## Classification of wines using PCA

Here, using scikit-learn's Principal Component Analysis [PCA](https://scikit-learn.org/stable/modules/generated/sklearn.decomposition.PCA.html), we apply Principal Component Analysis to the data to perform dimensionality reduction, and then use a support vector machine to classify wines on the dimensionality reduced data.  

The wine dataset is taken out and the features and targets are obtained.  
There are 13 features in total.  

```python
from sklearn import datasets
import pandas as pd
# wine
wine = datasets.load_wine() # wineのデータを辞書型で取得
# 辞書型の変数からDataFrameを作成
df = pd.DataFrame(data = wine['data'], columns=wine['feature_names'])
df['target'] = wine['target']

features = df.columns[:-1] # 最後のカラム以外を特徴量とする
# 特徴量とターゲットを取得
X = df[features].values 
y = df['target'].values
```

Split into training and test data.  

```python
# トレーニングデータとテストデータに分割
from sklearn.model_selection import train_test_split
random_state=10
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=random_state)
```

Standardize the features (mean zero, standard deviation one) for each of the training and test data.  
Note that **the test data is standardized by the statistics of the training data**.  

```python
# 特徴量の標準化
from sklearn.preprocessing import StandardScaler
SC = StandardScaler() # 標準化のインスタンス作成
SC.fit(X_train) # 訓練データの統計量取得
X_train_std = SC.transform(X_train) # 訓練データの標準化
X_test_std = SC.transform(X_test) # テストデータの標準化
```

Principal component analysis is applied. Here, the dimensionality is reduced from 13 features to 2 features.  
Note that **the test data is also transformed using the projection matrix obtained from the training data**.  

```python
# 主成分分析（PCA）を実行
from sklearn.decomposition import PCA
Pca = PCA(n_components=2) # PCAのインスタンスを作成, 射影先の特徴量の数を2に設定
Pca.fit(X_train_std) # 訓練データから射影行列Wを計算
X_train_pca = Pca.transform(X_train_std) # 訓練データをPCAで変換
X_test_pca = Pca.transform(X_test_std) # テストデータをPCAで変換
```

Check the projection matrix.

```python
# 射影行列の確認
Pca.components_
```

```python
array([[ 0.1687399 , -0.25828114,  0.02289513, -0.25933973,  0.14665826,
         0.38720993,  0.41972155, -0.28155737,  0.30974284, -0.06159091,
         0.29064335,  0.35706719,  0.31376888],
       [-0.4629211 , -0.22739021, -0.30710458,  0.01411108, -0.34077597,
        -0.06521239,  0.04465878, -0.05299909, -0.05692405, -0.53375317,
         0.27934865,  0.21252392, -0.32088363]])
```

Supervised learning with support vector machines using data that has been reduced in dimensionality to two dimensions.  

```python
# 主成分分析したデータで学習
from sklearn.svm import SVC # サポートベクトルマシンを用いるためのクラス
SVM = SVC(kernel='linear', random_state=random_state)
SVM.fit(X_train_pca, y_train)
y_train_predict = SVM.predict(X_train_pca)
```

Check the percentage of correct answers.  
Although we have reduced the dimension to 2 dimensions, we can see that the prediction is quite accurate.  

```python
# 正解率
print('正解率(訓練データ)： ', SVM.score(X_train_pca, y_train))
print('正解率(テストデータ)： ', SVM.score(X_test_pca, y_test))
```

Let's check with a boundary diagram.

```python
# 境界線図で確認
import sub
df_p = pd.DataFrame(X_train_pca, columns=['pc_1st', 'pc_2nd'])
df_p['target'] = y_train
sub.draw_boundary_map(df_p, SVM)
```

![pca_wine](./images/pca_wine.png)

Just to be sure, let's take two features from the original data without PCA and predict them using a support vector machine.  

```python
# PCAを使わずに学習してみる
col_num = [1, 2] # もとのデータから抜き出すカラム番号
cols = [True if e in col_num else False for e in range(13) ]
X_train_std_dr = X_train_std[:,cols]
X_test_std_dr = X_test_std[:, cols]

SVM_o = SVC(kernel='linear', random_state=random_state)
SVM_o.fit(X_train_std_dr, y_train)
y_train_predict = SVM.predict(X_train_std_dr)
```

Let us check the percentage of correct answers in this case.  
Both data are 2-dimensional, but we can see that the percentage of correct answers is quite poor when no PCA is applied.  

```python
# 正解率
print('正解率(訓練データ)： ', SVM_o.score(X_train_std_dr, y_train))
print('正解率(テストデータ)： ', SVM_o.score(X_test_std_dr, y_test))
```

Let's check with a boundary diagram.  
We find that it is not very well classified.  

```python
# 境界線図で確認
df_o = pd.DataFrame(X_train_std_dr, columns=[f'col{e}' for e in col_num])
df_o['target'] = y_train
sub.draw_boundary_map(df_o, SVM_o)
```

![no_pca_wine](./images/pca_wine_no_pca.png)

## Confirmation of covariance matrix

Let us check the covariance matrix for the data before and after applying PCA.  
First, we apply PCA without dimensionality reduction and obtain the data before and after PCA.

```python
# 次元削減をしないで、主成分分析（PCA）を実行
PCA_all = PCA() # PCAのインスタンスを作成, 次元削減なし
PCA_all.fit(X_train_std) # 訓練データから射影行列Wを計算
X_train_pca = PCA_all.transform(X_train_std) # 訓練データをPCAで変換
```

Visualize the covariance matrix of the data before applying PCA.  
We can see that each component is correlated.  

```python
# PCAを施す前の元データの共分散行列
import numpy as np
import seaborn as sns
sns.heatmap(np.cov(X_train_std.T))
```

![cov_0](./images/cov_0.png)

Visualize the covariance matrix of the data from PCA.  
The covariance of the off-diagonal components is zero, indicating that the components are uncorrelated with each other.  
The variance (diagonal elements) of each component is also found to be in order of increasing value by scikit-learn's PCA.  

```python
# PCAを施したデータの共分散行列
sns.heatmap(np.cov(X_train_pca.T))
```

![cov_1](./images/cov_1.png)

Specific values can be obtained from  

```python
# 各成分の分散
print('分散： ', PCA_all.explained_variance_)
```

## Contribution

For wine classification, we have reduced the dimension to two dimensions.
The degree to which each principal component reflects the original data is determined from the **contribution ratio**.  

The variance (eigenvalue) of each principal component of PCA, the larger this value is, the more it is said to reflect the original data. Therefore, the contribution of the $i$th principal component is given by  

```math
\frac{\lambda_i}{\sum_{k=1}^{p} \lambda_k}
```

In determining which principal components to employ, we often consider the cumulative contribution ratio, which is the sum of the contribution ratios of each component.

```python
# 各主成分の寄与率と累積寄与率
evr = PCA_all.explained_variance_ratio_
ccr = np.cumsum(evr)
print('寄与率：', evr)
print('累積寄与率： ', ccr)
```

Visualize each of these.  

```python
# 寄与率をグラフで確認
import matplotlib.pyplot as plt
plt.figure(figsize=(6, 4))
plt.bar(range(13), evr, color='blue', label='各主成分の寄与率')
plt.plot(range(13), ccr, color='red', marker='s', label='累積寄与率')
plt.xlabel('主成分のインデックス')
plt.ylabel('寄与率')
plt.legend(loc='best')
plt.show()
```

![exp_val](./images/exp_val_ratio_en.png)

Question:
Find the percentage of correct answers when the number of principal components is 1, 2, 3, 4,,,,,.  
