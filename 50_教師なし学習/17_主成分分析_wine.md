# 主成分分析

## 主成分分析を用いたワインの分類

ここでは、scikit-learnの主成分分析[PCA](https://scikit-learn.org/stable/modules/generated/sklearn.decomposition.PCA.html)を用いて、データに主成分分析を施して次元削減を行い、次元削減したデータに対してサポートベクトルマシンを用いてワインの分類をしてみます。  

ます、ワインのデータセットを取り出し、特徴量とターゲットを取得します。  
特徴量は全部で１３個あります。

```python
from sklearn import datasets
import pandas as pd
# wine
wine = datasets.load_wine() # wineのデータを辞書型で取得
# 辞書型の変数からDataFrameを作成
df = pd.DataFrame(data = wine['data'], columns=wine['feature_names'])
df['target'] = wine['target']

features = df.columns[:-1] # 最後のカラム以外を特徴量とする
# 特徴量とターゲットを取得
X = df[features].values 
y = df['target'].values
```

トレーニングデータとテストデータに分割します。

```python
# トレーニングデータとテストデータに分割
from sklearn.model_selection import train_test_split
random_state=10
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=random_state)
```

トレーニングデータとテストデータそれぞれに対して、特徴量を標準化（平均ゼロ、標準偏差１）します。  
テストデータは、訓練データの統計量で標準化することに注意してください。  

```python
# 特徴量の標準化
from sklearn.preprocessing import StandardScaler
SC = StandardScaler() # 標準化のインスタンス作成
SC.fit(X_train) # 訓練データの統計量取得
X_train_std = SC.transform(X_train) # 訓練データの標準化
X_test_std = SC.transform(X_test) # テストデータの標準化
```

主成分分析を施します。ここでは、１３個の特徴量から２個の特徴量へと次元削減しています。  
訓練データから求めた射影行列をもちいて、テストデータも変換していることに注意してください。  

```python
# 主成分分析（PCA）を実行
from sklearn.decomposition import PCA
Pca = PCA(n_components=2) # PCAのインスタンスを作成, 射影先の特徴量の数を2に設定
Pca.fit(X_train_std) # 訓練データから射影行列Wを計算
X_train_pca = Pca.transform(X_train_std) # 訓練データをPCAで変換
X_test_pca = Pca.transform(X_test_std) # テストデータをPCAで変換
```

射影行列を確認します。

```python
# 射影行列の確認
Pca.components_
```

```python
array([[ 0.1687399 , -0.25828114,  0.02289513, -0.25933973,  0.14665826,
         0.38720993,  0.41972155, -0.28155737,  0.30974284, -0.06159091,
         0.29064335,  0.35706719,  0.31376888],
       [-0.4629211 , -0.22739021, -0.30710458,  0.01411108, -0.34077597,
        -0.06521239,  0.04465878, -0.05299909, -0.05692405, -0.53375317,
         0.27934865,  0.21252392, -0.32088363]])
```

２次元へと次元削減したデータを用いて、サポートベクトルマシンによる教師あり学習をします。

```python
# 主成分分析したデータで学習
from sklearn.svm import SVC # サポートベクトルマシンを用いるためのクラス
SVM = SVC(kernel='linear', random_state=random_state)
SVM.fit(X_train_pca, y_train)
y_train_predict = SVM.predict(X_train_pca)
```

正解率を確認してみます。  
２次元まで次元削減しましたが、かなりの精度で予測できていることが分かります。  

```python
# 正解率
print('正解率(訓練データ)： ', SVM.score(X_train_pca, y_train))
print('正解率(テストデータ)： ', SVM.score(X_test_pca, y_test))
```

境界線図を用いて確認してみます。  

```python
# 境界線図で確認
import sub
df_p = pd.DataFrame(X_train_pca, columns=['pc_1st', 'pc_2nd'])
df_p['target'] = y_train
sub.draw_boundary_map(df_p, SVM)
```

![pca_wine](./images/pca_wine.png)

念のため、PCAを行わないで、元々のデータから特徴量二つを取り出して、サポートベクトルマシンを用いて予測してみます。

```python
# PCAを使わずに学習してみる
col_num = [1, 2] # もとのデータから抜き出すカラム番号
cols = [True if e in col_num else False for e in range(13) ]
X_train_std_dr = X_train_std[:,cols]
X_test_std_dr = X_test_std[:, cols]

SVM_o = SVC(kernel='linear', random_state=random_state)
SVM_o.fit(X_train_std_dr, y_train)
y_train_predict = SVM.predict(X_train_std_dr)
```

このときの正解率を確認してみます。  
どちらも２次元のデータですが、PCAを施さない場合は正解率がかなり悪いことが分かります。

```python
# 正解率
print('正解率(訓練データ)： ', SVM_o.score(X_train_std_dr, y_train))
print('正解率(テストデータ)： ', SVM_o.score(X_test_std_dr, y_test))
```

境界線図を用いて確認してみます。  
あまり分類できていないことが分かります。

```python
# 境界線図で確認
df_o = pd.DataFrame(X_train_std_dr, columns=[f'col{e}' for e in col_num])
df_o['target'] = y_train
sub.draw_boundary_map(df_o, SVM_o)
```

![no_pca_wine](./images/pca_wine_no_pca.png)

## 共分散行列の確認

PCAを施す前のデータと、PCAを施した後のデータについて共分散行列を確認してみます。  
まずは次元削減をしないでPCAを施して、PCA前後のデータを取得します。

```python
# 次元削減をしないで、主成分分析（PCA）を実行
PCA_all = PCA() # PCAのインスタンスを作成, 次元削減なし
PCA_all.fit(X_train_std) # 訓練データから射影行列Wを計算
X_train_pca = PCA_all.transform(X_train_std) # 訓練データをPCAで変換
```

PCAを施す前のデータの共分散行列を可視化します。  
各々の成分に相関があることが分かります。  

```python
# PCAを施す前の元データの共分散行列
import numpy as np
import seaborn as sns
sns.heatmap(np.cov(X_train_std.T))
```

![cov_0](./images/cov_0.png)

PCAを施したデータの共分散行列を可視化します。  
非対角成分の共分散はゼロであり、互いの成分に相関が無いことが分かります。  
また、各成分の分散（対角要素）は、scikit-learnのPCAによって、値の大きい順に並んでいることが分かります。

```python
# PCAを施したデータの共分散行列
sns.heatmap(np.cov(X_train_pca.T))
```

![cov_1](./images/cov_1.png)

具体的な値は、次から求めることができます。  

```python
# 各成分の分散
print('分散： ', PCA_all.explained_variance_)
```

## 寄与率

ワインの分類では２次元まで次元削減しました。
各々の主成分が、もとのデータをどの程度反映しているかは**寄与率**から求めます。  

PCAの各主成分の分散（固有値）ですが、この値が大きいほど、もとのデータを反映していると言われています。従って、$i$番目の主成分の寄与率は次の式で与えられます。  

```math
\frac{\lambda_i}{\sum_{k=1}^{p} \lambda_k}
```

どの主成分まで採用すれば良いかを判断する際に、各成分の寄与率を足していった累積寄与率を考えることも多くあるそうです。

```python
# 各主成分の寄与率と累積寄与率
evr = PCA_all.explained_variance_ratio_
ccr = np.cumsum(evr)
print('寄与率：', evr)
print('累積寄与率： ', ccr)
```

それぞれを視覚化します。  

```python
# 寄与率をグラフで確認
import matplotlib.pyplot as plt
plt.figure(figsize=(6, 4))
plt.bar(range(13), evr, color='blue', label='各主成分の寄与率')
plt.plot(range(13), ccr, color='red', marker='s', label='累積寄与率')
plt.xlabel('主成分のインデックス')
plt.ylabel('寄与率')
plt.legend(loc='best')
plt.show()
```

![exp_val](./images/exp_val_ratio.png)

問題：
主成分の数を１，２，３，４, ,,,としていった時の正解率を求めてください。  
