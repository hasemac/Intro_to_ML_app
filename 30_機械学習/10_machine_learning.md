# Machine Learning

## Machine Learning scope

![ai](./images/ai_en.drawio.svg)

- Artificial Intelligence, AI  
"**Artificial Intelligence (AI) is a broad field** and refers to the use of technology to build machines and computers with the ability to mimic the cognitive functions associated with human intelligence. For example, they understand and respond to something in spoken or written form, analyze data, and make recommendations."([Google cloud](https://cloud.google.com/learn/artificial-intelligence-vs-machine-learning?hl=ja))  

- rule-based  
  It is also classified as artificial intelligence if a human explicitly gives it rules to behave like a human.

- Machine Learning, ML  
  "**Machine learning is a subset of artificial intelligence (AI)** that allows a machine or system to learn from experience and make improvements automatically.Instead of explicit programming, machine learning uses algorithms to analyze large amounts of data, learn from the analyzed information, and then make informed decisions."([Google cloud](https://cloud.google.com/learn/artificial-intelligence-vs-machine-learning?hl=ja))  

## Basic Machine Learning Processes

There are two main basic processes in machine learning: **learning** and **inference**.  
The first step is "**Learning**". In **Learning**, the features of the data are learned from the training data to create a **Predictive Model** that makes predictions about the data.  
Next comes **inference**. In **inference**, predictions are made on the data using the prediction model created in **learning**.  

## Supervised learning

For example, consider the case where you want to predict the rent based on the square footage and age of the property. Or consider the case where you want to predict the type of fruit based on a photographic image of the fruit.  
The number (rent) or label (name of the fruit) that you want to predict is called the **target** (**objective variable**).  
The data used to make the prediction (the square footage of the property, the age of the property, or a picture of the fruit) are called **features** (**explanatory variables**).  
The set of targets and their features is then called **training data**.  

![data](./images/data_en.drawio.svg)

Supervised learning is machine learning when there is **training data** that contains the target data.  
In particular, predicting labels, such as types of fruit (apples and bananas), is called **classification**, and predicting numerical values, such as rent, is called **regression**.  

## Unsupervised learning

Unsupervised learning is machine learning in the absence of supervised data.  
It is machine learning when there is no correct data (target), such as numbers or labels to predict, and it finds and summarizes similarities between data.  

For example, consider a case in which there is no correct label, such as apples or bananas, but there are pictures of numerous fruits. In unsupervised learning, the fruits would be summarized by finding similarities based on their color, shape, etc.  

Primary application examples are **clustering** and **feature extraction**.

- Clustering  
  Clustering is the grouping of similar data into the same group based on the distance between the data.  
  The major difference from supervised learning classification is that there are no correct labels.  
  This means that groups grouped together by clustering require a human to look at the data to determine by what similarity they were grouped.

- Feature extraction  
  Feature extraction may be performed as a preliminary preparation for supervised learning.  
  The number of features is reduced (**dimension reduction**) by converting the data set (features) while retaining as much information as possible from the original data set.  
  Dimensionality reduction is expected to improve computational efficiency and prediction performance.  
  However, interpretation of the converted features may be difficult.  

## Reinforcement learning

"**reinforcement learning** (**RL**) is a branch of machine learning in which an intelligent **agent** in an **environment** observes its current **state** and determines what **actions** it should take in order to maximize the cumulative **reward** obtained. Reinforcement learning is one of the three basic machine learning paradigms, along with supervised and unsupervised learning."（[Wikipedia](https://ja.wikipedia.org/wiki/%E5%BC%B7%E5%8C%96%E5%AD%A6%E7%BF%92)）

![rl](./images/rl.drawio.svg)
