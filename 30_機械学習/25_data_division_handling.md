# Data Division and Handling

## Data Division

When a data set is given and the data is used to build, train, or evaluate a model, the data is divided according to its intended use, such as for training or evaluation.

![split_data](./images/split_data_for_train_vali_test_en.drawio.svg)

The general division is as follows

1. split the dataset into " learning data" and "test data  
2. split the learning data into "training data" and "validation data  

|||
|:---|:---|
|Learning data| Create a learning model from this data. |
|Test data|Final evaluation of the created learning model. |
|Training data|Data for training (learning) models |
|Validation data|Data to evaluate training (learning) |

The model is trained using training data and evaluated with validation data, while at the same time adjusting **hyperparameters**.  
Finally, test data is used to evaluate the final generalization performance, as artificial hyperparameter optimization may only be valid with its validation data.  

![split_with_model](./images/split_data_model_en.drawio.svg)

**Hyperparameters** : Parameters that need to be artificially adjusted. Such as learning rate, number of layers in a neural network, etc.

## Verification Method

### Holdout validation

This is a simple method in which learning data is divided into training data and validation data for evaluation.  
However, note that when the number of validation data is small, the evaluation may differ greatly depending on how the validation data is taken.

![validation_holdout](./images/split_data_validation_holdout_en.drawio.svg)

### K-fold Cross-Validation

This method divides the data into k subsets and creates k data sets, one as validation data and the rest as training data, then evaluates all of them and takes the average of them as the overall evaluation.
This method suppresses variation due to the way the validation data is taken, but it requires more calculations and procedures.

![validation_k](./images/split_data_validation_k_en.drawio.svg)