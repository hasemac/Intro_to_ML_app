# Evaluation of Classification Problems

Several indicators are presented to evaluate the accuracy and error of the created classification models.

Contents：

- [Evaluation of Classification Problems](#evaluation-of-classification-problems)
  - [Confusion matrix](#confusion-matrix)
  - [Calculation of the confusion matrix](#calculation-of-the-confusion-matrix)
  - [Indicators such as the accuracy](#indicators-such-as-the-accuracy)
    - [Accuracy](#accuracy)
    - [Precision](#precision)
    - [Recall](#recall)
    - [F value](#f-value)
  - [Calculation of indicators](#calculation-of-indicators)
  - [Predicted Probability](#predicted-probability)
  - [ROC curve and AUC](#roc-curve-and-auc)
    - [(1) If the decision to be positive is made very strictly](#1-if-the-decision-to-be-positive-is-made-very-strictly)
    - [(2) If the decision to be positive is made very loosely](#2-if-the-decision-to-be-positive-is-made-very-loosely)
    - [(3) If moderately positive but no study is made](#3-if-moderately-positive-but-no-study-is-made)
    - [(4) When predictions can be made perfectly](#4-when-predictions-can-be-made-perfectly)
    - [AUC (Area Under the Curve)](#auc-area-under-the-curve)
  - [Drawing ROC Curves](#drawing-roc-curves)

## Confusion matrix

The confusion matrix is often used to evaluate the goodness of a binary classification model.  
It is represented in the following figure  

![conf_mat](./images/confusion_matrix_en.drawio.svg)

- True Positive
  the prediction was positive and the actual was also positive.
- False Negative
  the prediction was negative and the actual was positive.
- False Positive
  the prediction was positive and the actual was negative.
- True Negative
  the prediction was negative and the actual was also negative.

## Calculation of the confusion matrix

Let's classify using random forests for the [breast cancer dataset](https://scikit-learn.org/stable/datasets/toy_dataset.html#breast-cancer-dataset) and create a confusion matrix.  
Take the dataset and get the features and targets.  

```python
from sklearn import datasets
import pandas as pd
# breast cancer
breast = datasets.load_breast_cancer()
# 辞書型の変数からDataFrameを作成
df = pd.DataFrame(data = breast['data'], columns=breast['feature_names'])
df['target'] = 1 - breast['target'] # 悪性で１，良性でゼロにする場合

features = df.columns[:-1] # 最後のカラム以外を特徴量とする
# 特徴量とターゲットを取得
X = df[features].values 
y = df['target'].values
```

Classify and predict using random forests.  

```python
from sklearn.ensemble import RandomForestClassifier # ランダムフォレストを実行するためのクラス
# ランダムフォレストを用いて学習
RFC = RandomForestClassifier(max_depth=2, random_state=0)
RFC.fit(X, y) 

y_pred = RFC.predict(X) # 分類の予測結果
```  

The result is displayed as a confusion matrix.  
To create the confusion matrix, use scikit-learn's [confusion_matrix](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.confusion_matrix.html).  

```python
from sklearn.metrics import confusion_matrix
print(confusion_matrix(y, y_pred)) # 混同行列を表示
```

```python
[[349   8]
 [ 15 197]]
```

The number of benign data is 357 and the number of malignant data is 212.  
The first line corresponds to the actual classification as benign and the second line corresponds to malignant.  

Note:
The output of scikit-learn's confusion_matrix is in the following order.  
The values are displayed together with the values of the matrices above.

![conf_mat_sciket](./images/conf_mat_sklearn_en.drawio.svg)

Here, benign is zero and malignant is one, so for example, a true negative (TN), which is both actual and predicted to be benign, would be 349 cases.

## Indicators such as the accuracy

### Accuracy

The accuracy is the percentage of data that is correctly predicted relative to the overall prediction result.

<img src="./images/confusion_matrix.drawio.svg" width="200px">

```math
accuracy = \frac{TP+TN}{TP+TN+FP+FN}
```

### Precision

The precision is the percentage of data that is actually positive relative to what is predicted to be positive.  

<img src="./images/conf_precision.drawio.svg" width="200px">

```math
precision = \frac{TP}{TP+FP}
```

### Recall

Recall is the ratio of data predicted to be positive to those that were actually positive.

<img src="./images/conf_recall.drawio.svg" width="200px">

```math
recall=\frac{TP}{TP+FN}
```

### F value

For example, models that are cautious about testing positive often test negative, and vice versa.  
In other words, there is a trade-off between the precision and the recall. The F-value is a balanced evaluation of these two indicators.
The F value is the harmonic mean of the precsion and recall.  

```math
F value = \frac{2 \times precision \times recall}{precision + recall}
```

Harmonic average: A method of calculating the average by taking the reciprocal, calculating the average, and then taking the reciprocal again.

## Calculation of indicators

The accuracy_score, precision_score, recall_score, and f1_score of scikit-learn are used to calculate the accuracy, precision, recall, and F value.

```python
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score

print('正解率：', accuracy_score(y, y_pred))
print('適合率：', precision_score(y, y_pred))
print('再現率：', recall_score(y, y_pred))
print('F値：', f1_score(y, y_pred))
```

**Problem:**
Calculate the accuracy, precision, recall, and F value from the definition formula and verify that they are the same as the scikit-learn results.  

## Predicted Probability

Whether the class is classified as Class 0 or Class 1 depends on the probability of belonging to each class.  
Check the predicted class and the predicted probability.

```python
# 15番目から24番目までの予測確率の確認
print('予測クラス')
print(y_pred[15:25])
print('予測確率')
print(RFC.predict_proba(X)[15:25])
```

The probabilities are displayed in the format [(probability of being in class zero), (probability of being in class one)].  
Usually, the class with the highest probability is chosen. On the other hand, there are cases where it is necessary to pay attention to the probability itself rather than a simple binary classification, for example, if the probability that the mass is malignant exceeds 30%, a second medical examination is recommended.  

## ROC curve and AUC

For binary classification models, a 50% probability is usually the threshold for judgment.
The ROC curve (Receiver Operating Characteristic) is a visualization of what the prediction will be when this judgment threshold is varied.  

The ROC curve is the curve drawn when the horizontal axis is the false positive rate and the vertical axis is the true positive rate (recall rate), as shown in the figure below.
The false positive rate is the percentage predicted to be positive in the second row of the table.  
The true positive rate is the percentage predicted to be positive in the first row of the table.  

 ![roc](./images/roc.drawio.svg)

As an example, suppose the actual data shows 50 positive and 50 negative cases each.  
Let us divide the cases in the following cases.  

### (1) If the decision to be positive is made very strictly

At this time, all data are considered negative. The table is as follows.  

||Positive (predicted)|Negative (predicted)|
|---|---:|---:|
|**Positive (actual)**|0|50|
|**Negative (actual)**|0|50|

The true positive rate is zero and the false positive rate is also zero, which corresponds to point (1) in Figure.

### (2) If the decision to be positive is made very loosely

Suppose that all data are determined to be positive.  

||Positive (predicted)|Negative (predicted)|
|---|---:|---:|
|**Positive (actual)**|50|0|
|**Negative (actual)**|50|0|

Both the true positive rate and the false positive rate will be 1, which is the point (2) in Figure.

### (3) If moderately positive but no study is made

When making a decision with an unlearned model, the same percentage of actual positives and negatives are predicted to be positive. Let us assume that out of 50 pieces of data each, about 10 are predicted to be positive together.  

||Positive (predicted)|Negative (predicted)|
|---|---:|---:|
|**Positive (actual)**|10|40|
|**Negative (actual)**|10|40|

In this case, the true positive rate and the false positive rate have the same value.  
The straight line (3) in Figure corresponds to this case.

### (4) When predictions can be made perfectly

||Positive (predicted)|Negative (predicted)|
|---|---:|---:|
|**Positive (actual)**|50|0|
|**Negative (actual)**|0|50|

Since the true positive rate is 1 and the false positive rate is 0, which corresponds to point (4) in Figure.

Given the above, the ROC curve is usually the curve that is drawn in the upper left oblique region.

![roc](./images/roc_hist_en.drawio.svg)

### AUC (Area Under the Curve)

The blue area in the ROC curve diagram is called the Area Under the Curve (AUC).  
AUC is also an indicator of the accuracy of the classification model.
The maximum value of AUC is 1, and the larger the value, the more accurate the classification.  

## Drawing ROC Curves

Use scikit-learen's [roc_curve](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.roc_curve.html) to calculate the ROC curve data.

The `roc_curve` is passed the actual value's positive and negative classification data and the probability of being classified as positive as arguments.
Three results are output: fpr: false positive rate, tpr: true positive rate, and thresholds: threshold values.  

```python
from sklearn.metrics import roc_curve
proba = RFC.predict_proba(X)
# roc_curveのデータ取得
fpr, tpr, thresholds = roc_curve(y, proba[:,1])
# fpr: 偽陽性率, tpr: 真陽性率, thresholds: 閾値
# proba[:,1] :１と判定される確率
```

Draws ROC curves.

```python
# ROC曲線を描画
import matplotlib.pyplot as plt
plt.figure(figsize=(4, 4))
plt.plot(fpr, tpr, label="ROC curve")
plt.plot([0,1],[0,1], linestyle='--', label='random')
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.legend(loc='lower right')
plt.gca().set_aspect('equal')
plt.show()
```

![roc](./images/roc.png)

The AUC is also calculated.  

```python
# AUCの計算
from sklearn.metrics import auc
print('AUC: ', auc(fpr, tpr))
```
