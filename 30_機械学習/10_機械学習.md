# 機械学習

## 機械学習の全体像

![ai](./images/ai.drawio.svg)

- 人工知能（Artificial Intelligence, AI）  
"**人工知能（AI）は幅広い分野**であり、人間の知能に関連する認知機能を模倣する能力を持つ機械やコンピュータを構築するための技術の使用を指します。たとえば、話し言葉や文字で何かを理解してそれに応え、データを分析し、推奨事項を提案します。"([Google cloud](https://cloud.google.com/learn/artificial-intelligence-vs-machine-learning?hl=ja)より)  

- ルールベース  
  人間が明示的にルールを与えることで人間のようにふるまうのも人工知能に分類されます。  

- 機械学習（Machine Learning, ML）  
  "**機械学習は、人工知能（AI）のサブセット**で、機械またはシステムが経験から学習して改善を自動的に行えるようにします。明示的なプログラミングの代わりに、機械学習はアルゴリズムを使用して大量のデータを分析し、分析情報から学習してから、情報に基づいた意思決定を行います。"([Google cloud](https://cloud.google.com/learn/artificial-intelligence-vs-machine-learning?hl=ja)より)  

## 機械学習の基本的プロセス

機械学習の基本的プロセスは主に「**学習**」と「**推論**」の二つがあります。  
最初に「**学習**」を行います。「**学習**」では学習データからデータの特徴を学習して、データの予測を行う**予測モデルの作成**をします。  
次に「**推論**」を行います。「**推論**」では、学習で作成した予測モデルを用いて、**データに対して予測**を行います。  

## 教師あり学習

例えば、家賃を物件の面積や築年数から予測したい場合を考えます。または、果物の種類を果物の写真の画像から予測したい場合を考えます。
このとき、予測したい数値（家賃）やラベル（果物の名称）のことを**ターゲット**（**目的変数**）と言います。  
予測するのに用いたデータ（物件の面積や築年数、または果物の写真）を**特徴量**（**説明変数**）と言います。  
そして、ターゲットとその特徴量のセットを**教師データ**と言います。  

![data](./images/data.drawio.svg)

教師あり学習は、正解のデータ（ターゲット）を含んでいる**教師データ**がある場合の機械学習です。  
特に、果物の種類（リンゴやバナナ）などラベルを予測するのを**分類**とよび、家賃など数値を予測するのを**回帰**と呼びます。

## 教師なし学習

教師なし学習は、教師データがない場合の機械学習です。  
予測する数値やラベルなど正解のデータ（ターゲット）が無い場合の機械学習で、データ間の類似性をみつけてまとめていきます。

たとえば、リンゴやバナナなどの正解ラベルはないものの、数多くの果物の写真がある場合を考えます。教師なし学習では、その果物の色や形状などから類似性をみつけてまとめていくことになります。  

主な用途例は、**クラスタリング**と**特徴量抽出**です。  

- クラスタリング  
  クラスタリングは、データ間の距離に基づいて、類似するデータを同じグループにまとめることです。教師あり学習の分類と大きく異なる点は、正解ラベルが存在しない点です。  
  つまり、クラスタリングでまとまったグループは、どのような類似性でグループになったのか、データを人間が見て判断する必要があります。

- 特徴量抽出  
教師あり学習をする事前準備として特徴量抽出を行う場合があります。データセット(特徴量)に変換をして、もともとのデータセットの情報をできるだけ保持しつつ、特徴量の個数を減らします（**次元削減**）。  
次元削減を行うことで、計算効率や予測性能の向上などが期待されます。ただし、変換された特徴量の解釈が困難である場合があります。  

## 強化学習

"**強化学習**（きょうかがくしゅう、英: reinforcement learning、**RL**）は、ある**環境**内における知的**エージェント**が、現在の**状態**を観測し、得られる累積**報酬**（英語版）を最大化するために、どのような**行動**（英語版）をとるべきかを決定する機械学習の一分野である。強化学習は、教師あり学習、教師なし学習と並んで、3つの基本的な機械学習パラダイムの一つである。"（wikipediaの[強化学習](https://ja.wikipedia.org/wiki/%E5%BC%B7%E5%8C%96%E5%AD%A6%E7%BF%92)より）

![rl](./images/rl.drawio.svg)