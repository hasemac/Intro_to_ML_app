# Data Set

Contents

- [Data Set](#data-set)
  - [scikit](#scikit)
  - [Iris (Classification, 150 data)](#iris-classification-150-data)
  - [Wine (Classification, 178 data)](#wine-classification-178-data)
  - [Breast cancer (Classification, 569 data)](#breast-cancer-classification-569-data)
  - [Diabetes mellitus (regression, 442 data)](#diabetes-mellitus-regression-442-data)
  - [California home prices (regression, 20640 data)](#california-home-prices-regression-20640-data)

## scikit

Several [datasets](https://scikit-learn.org/stable/datasets.html) are available on [scikit-learn](https://scikit-learn.org/stable/index.html).

Please run the code below to import modules, etc. in advance.

```python
from sklearn import datasets
import pandas as pd
```

## Iris (Classification, 150 data)

This [dataset](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.load_iris.html#sklearn.datasets.load_iris) is for classification of three types of iris based on a total of four features: length and width of petal and sepal of iris.  

```python
# iris
iris = datasets.load_iris()
df = pd.DataFrame(data = iris['data'], columns=iris['feature_names'])
df['target'] = iris['target']
```

## Wine (Classification, 178 data)

The [dataset](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.load_wine.html#sklearn.datasets.load_wine) for classifying wine types based on 13 features such as alcohol content and component concentration.  
Thirteen features are used to classify three types of wine.  

target

- Wine type
  class_0, class_1, class_2

Feature value

- alcohol
- malic_acid
- ash
- alcalinity_of_ash
- magnesium
- total_phenols
- flavanoids
- nonflavanoid_phenols
- proanthocyanins
- color_intensity
- hue
- od280/od315_of_diluted_wines：degree of dilution
- proline

```python
# wine
wine = datasets.load_wine() # wineのデータを辞書型で取得
# 辞書型の変数からDataFrameを作成
df = pd.DataFrame(data = wine['data'], columns=wine['feature_names'])
df['target'] = wine['target']
```

## Breast cancer (Classification, 569 data)

According to the description of the [breast cancer dataset](https://scikit-learn.org/stable/datasets/toy_dataset.html#breast-cancer-dataset), features include mass size calculated from digitized fine needle aspiration (FNA) images of breast masses.  
The target is the diagnosis of each subject (0 malignant, 1 benign).
Malignant (0) was 212 cases and benign (1) was 357 cases.  

```python
# breast cancer
breast = datasets.load_breast_cancer()
# 辞書型の変数からDataFrameを作成
df = pd.DataFrame(data = breast['data'], columns=breast['feature_names'])
df['target'] = breast['target'] # デフォルトは悪性のときゼロ
#df['target'] = 1 - breast['target'] # 悪性で１，良性でゼロにする場合
```

## Diabetes mellitus (regression, 442 data)

According to the description of the [diabetes dataset](https://scikit-learn.org/stable/datasets/toy_dataset.html#diabetes-dataset), the features and other information are as follows.

target

- a quantitative measure of disease progression one year after baseline  

feature values

- age age in years
- sex
- bmi body mass index
- bp average blood pressure
- s1 tc, total serum cholesterol
- s2 ldl, low-density lipoproteins
- s3 hdl, high-density lipoproteins
- s4 tch, total cholesterol / HDL
- s5 ltg, possibly log of serum triglycerides level
- s6 glu, blood sugar level

```python
# diabetes
diabetes = datasets.load_diabetes()
# 辞書型の変数からDataFrameを作成
df = pd.DataFrame(data = diabetes['data'], columns=diabetes['feature_names'])
df['target'] = diabetes['target']
```

## California home prices (regression, 20640 data)

According to the description of the [California home price dataset](https://scikit-learn.org/stable/datasets/real_world.html#california-housing-dataset), the features and other data are as follows.  

target

- MedHouseVal: Home price (median)

特徴量

- 'MedInc': Income (median)
- 'HouseAge': Age of the house
- 'AveRooms': Number of rooms
- 'AveBedrms': Number of bedrooms
- 'Population'
- 'AveOccup': Total number of households
- 'Latitude'
- 'Longitude'
  
```python
# california
california = datasets.fetch_california_housing()
# 辞書型の変数からDataFrameを作成
df = pd.DataFrame(data = california['data'], columns=california['feature_names'])
df['target'] = california['target']
```
