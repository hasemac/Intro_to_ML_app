# Data Preprocessing

内容：

- [Data Preprocessing](#data-preprocessing)
  - [overview](#overview)
  - [Preliminary Preparation of Wine Dataset](#preliminary-preparation-of-wine-dataset)
  - [Normalization](#normalization)
  - [Standardization](#standardization)

## overview

For example, when classifying wine, one of the feature quantities, alcohol content, takes a value of about 10 to 15, while wine hue is about 0.5 to 1.5.  
Thus, the scale of a feature quantity is often different for each feature quantity.  

On the other hand, in many cases, such as machine learning, it is assumed that all features have the same scale.
For this reason, the scales of each feature must be aligned in advance.  This process is called **pre-processing data**, and this scaling of data is called **normalization**.

Two typical examples are shown below.  

- Min-Max scaling  
  This is useful when the minimum and maximum values of data are predetermined.  
  A method that puts the range of a variable within a specific range, usually in the interval 0 to 1.  
- standardization  
  This method is used to set the mean of the data to 0 and the standard deviation to 1.
  
In many cases, normalization often refers to Min-Max scaling.  

## Preliminary Preparation of Wine Dataset

Prepare a wine data set as test data for normalization and standardization.  

```python
from sklearn import datasets
import pandas as pd
# wine
wine = datasets.load_wine() # wineのデータを辞書型で取得
# 辞書型の変数からDataFrameを作成
df = pd.DataFrame(data = wine['data'], columns=wine['feature_names'])
df['target'] = wine['target']

print(df[:3])

# 最後のカラム'target'を除いて、データを取得
X = df[df.columns[:-1]].values
```

## Normalization

A method to fit a range of data into a specific range, usually in the interval 0 to 1.  
When the minimum value of data is $x_{min}$ and the maximum value is $x_{max}$, the value $x$ is normalized by the following formula.  

```math
x'=\frac{x-x_{min}}{x_{max}-x_{min}}
```

- Suitable when the maximum and minimum values of data are clear.  
- Not suitable when large outliers are present.  

[MinMaxScaler](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.MinMaxScaler.html) from scikit-learn is used for normalization.

```python
# 正規化
from sklearn.preprocessing import MinMaxScaler

MMS = MinMaxScaler() # インスタンスを作成

MMS.fit(X) # データを入力して統計量を取得
X_mms = MMS.transform(X) # データXをMin-Maxスケーリング

# 値の確認
print('original: ')
print(X[:3])
print()
print('scaled: ')
print(X_mms[:3])
```

Ensure that it is normalized.  

```python
# 正規化されていることの確認
import numpy as np
print('min: ', np.min(X_mms, axis=0))
print('max: ', np.max(X_mms, axis=0))
```

Inverse transform the scaled variable `X_mms` and verify that it returns to its original value.  

```python
# 逆変換
X_mms_inv = MMS.inverse_transform(X_mms)
print(X_mms_inv[:3])
```

The next two lines, which get the statistics and also output the normalized variables, have a function `fit_transform` that does this all together.  

```python
MMS.fit(X) # データを入力して統計量を取得
X_mms = MMS.transform(X) # データXをMin-Maxスケーリング
```

```python
# 統計量の取得、かつスケーリングした変数作成
X_mms = MMS.fit_transform(X)
```

Note that this function varies the statistics.  
For example, in general, **normalization of validation data is done using the statistics of the training data.**
But if you do as shown below, you will be using the statistics of the validation data.

```python
# Wrong case
X_train_mms = MMS.fit_transform(X_train)
X_test_mms = MMS.fit_transform(X_test)
```

The correct preprocessing is as follows.
It sets the `x_train` statistic to instance MMS and then normalizes the training and validation data.

```python
# Correct (case1)
MMS.fit(X_train)
X_train_mms = MMS.transform(X_train)
X_test_mms = MMS.transform(X_test)
```

The following example normalizes the training data while obtaining its statistics, and then uses those statistics to normalize the validation data.

```python
# Correct (case2)
X_train_mms = MMS.fit_transform(X_train)
X_test_mms = MMS.transform(X_test)
```

## Standardization

Standardization is a transformation method that makes the mean of the data to be 0 and the standard deviation to be 1.  
With the mean of the data as $\mu$ and the standard deviation as $\sigma$, the value $x$ is normalized by the following equation  

```math
x'=\frac{x-\mu}{\sigma}
```

At this time $x'$ has mean zero and standard deviation one.

- It is particularly effective when the data distribution follows a normal distribution.
  (It can also be used when the distribution is not normal.)

[StandardScaler](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.StandardScaler.html) from scikit-learn is used for standardization.  

```python
# 標準化
from sklearn.preprocessing import StandardScaler

SS = StandardScaler() # インスタンスを作成
SS.fit(X) # データXの統計量を取得
X_ss = SS.transform(X) # データXを標準化

print('scaled: ')
print(X_ss[:3])
```

Ensure that they are standardized.  

```python
# 標準化されていることを確認
print('mean:')
print(np.mean(X_ss, axis=0))
print('std_dev:')
print(np.std(X_ss, axis=0))
```

Inverse transform to confirm the return to the original value.  

```python
# 標準化の逆変換
X_ss_inv = SS.inverse_transform(X_ss)
print(X_ss_inv[:3])
```

This standardization also has a function `fit_transform` that simultaneously obtains the statistic and the standardized variable.  
As mentioned in the normalization section, `fit_transform` changes the statistics. Please be careful when using it.  

Question：  

- Check the minimum and maximum value of each feature from the attribute of [MinMaxScaler](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.MinMaxScaler.html).  
- Check the mean and standard deviation of each feature from the attributes of the [StandardizedStandardScaler](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.StandardScaler.html).  
