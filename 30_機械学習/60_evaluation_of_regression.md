# Evaluation in Regression Problems

Some metrics for evaluating the accuracy and error of regression models.  

Contents：

- [Evaluation in Regression Problems](#evaluation-in-regression-problems)
  - [Coefficient of determination（$R^2$）](#coefficient-of-determinationr2)
  - [Root mean square error（RMSE）](#root-mean-square-errorrmse)
  - [Mean absolute error（MAE）](#mean-absolute-errormae)
  - [Root mean squared logarithmic error（RMSLE）](#root-mean-squared-logarithmic-errorrmsle)

## Coefficient of determination（$R^2$）

The coefficient of determination $R^2$ is often used as a measure of the goodness of fit of a regression model. The `score` function in `scikit-learn` regression is also calculated from this value.

It is defined by the following equation  

```math
R^2=1-\frac{\sum_{i=1}^n (y_i-\hat{y_i})^2}{\sum_{i=1}^n (y_i-\bar{y})^2}
```

$y_i$：observed values
$\hat{y_i}$：Predictions by regression model
$\bar{y}$：Average of observed values

Basically, the coefficient of determination $R^2$ takes values between 0 and 1, although it can be negative.
The closer to 1, the better the regression model is fitting.

- If each prediction is in perfect agreement with the observations ($\hat{y_i}=y_i$), the numerator is zero and the coefficient of determination is 1.  
- If each prediction is equal to the mean ($\hat{y_i}=\bar{y}$), that is, almost no prediction is made, the numerator and denominator will be equal and the coefficient of determination will be zero.  
- As can be seen from the definition formula, it is a dimensionless quantity.

## Root mean square error（RMSE）

Root Mean Squared Error (RMSE) is defined by the following equation.

```math
RMSE = \sqrt{\frac{1}{n} \sum_{i=1}^n (y_i-\hat{y_i})^2}
```

The closer the value is to zero (smaller error), the better the regression model is fitting.  
The range of values depends on the scale of the data.  

- squared, and therefore is strongly affected if the predicted value is far off.
- It is a physical quantity with dimension.

## Mean absolute error（MAE）

Mean Absolute Error (MAE) is defined by the following equation  

```math
MAE=\frac{1}{n} \sum_{i=1}^n |y_i-\hat{y_i}|
```

The closer the value is to zero (smaller error), the better the regression model is fitting.
The range of values depends on the scale of the data.  

- Since it is not squared, it is considered less susceptible to outliers.
- It is a physical quantity with dimension.

## Root mean squared logarithmic error（RMSLE）

Root Mean Squared Logarithmic Error (RMSLE) is defined by the following equation  

```math
RMSLE = \sqrt{\frac{1}{n} \sum_{i=1}^n (
    \log(y_i+1)-\log(\hat{y_i}+1)
)^2}
```

This indicator is used when the value is positive and the range is wide (the number of digits in the data is broad).  

- The logarithm reduces the effect of large values on the scale.
- We add 1 in $\log$ so that we can calculate even when $Y$ is zero.
- Otherwise, it is the same as the RMSE formula.  
