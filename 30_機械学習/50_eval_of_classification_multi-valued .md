# Evaluation of Classification Problems

We have dealt with confusion matrices in binary classification, but we can also deal with confusion matrices in multilevel classification.  
Using the wine dataset (3 classifications) as an example, we will create a confusion matrix.

Acquire wine dataset to obtain features and targets.  

```python
from sklearn import datasets
import pandas as pd

# wine
wine = datasets.load_wine() # wineのデータを辞書型で取得
# 辞書型の変数からDataFrameを作成
df = pd.DataFrame(data = wine['data'], columns=wine['feature_names'])
df['target'] = wine['target']

#features = df.columns[:-1] # 最後のカラム以外を特徴量とする
features = ['proline', 'hue']
# 特徴量とターゲットを取得
X = df[features].values 
y = df['target'].values
```

Classify using a random forest.

```python
from sklearn.ensemble import RandomForestClassifier # ランダムフォレストを実行するためのクラス
# ランダムフォレストを用いて学習
RFC = RandomForestClassifier(max_depth=1, random_state=0)
RFC.fit(X, y) 

y_pred = RFC.predict(X) # 分類の予測結果
```

Outputs a confusion matrix.

```python
from sklearn.metrics import confusion_matrix
print(confusion_matrix(y, y_pred)) # 混同行列を表示
```

```python
[[57  2  0]
 [ 4 62  5]
 [ 0  8 40]]
```

Visualize using seaboarn's [heatmap](https://seaborn.pydata.org/generated/seaborn.heatmap.html).  

```python
import seaborn as sns
sns.heatmap(confusion_matrix(y, y_pred))
```

![conf_mat_multi](./images/conf_mat_multi.png)