# Loss function

The loss function is a function that takes as input the correct answer label and the predicted label (classification) or the correct answer value and the predicted value (regression) and expresses how well the correct answer and the prediction match. In general, the smaller the value of the loss function, the better the agreement between the correct answer and the prediction.  
This section describes the loss function in the case of classification and regression.  

- [Loss function](#loss-function)
  - [Loss function for classification problems](#loss-function-for-classification-problems)
    - [Cross entropy](#cross-entropy)
    - [Softmax function](#softmax-function)
    - [Differentiation of loss functions](#differentiation-of-loss-functions)
  - [Loss function for regression problems](#loss-function-for-regression-problems)
    - [Squared error](#squared-error)
    - [Absolute error](#absolute-error)

## Loss function for classification problems

Cross-entropy is used as the loss function for multiclass classification. Before calculating cross-entropy, a softmax function is applied to convert predictions to probabilities.

### Cross entropy

The **cross entropi** $L(y, t)$ used as the loss function for multiclass classification is given by

```math
L(y, t) = - \sum_{i=1}^k t_i \log y_i
```

$k$ : Number of classes  
$i$ : The $i$-th class ($i = 1, 2,,,k$)  
$t_i$: Correct answer label (Correct answer label: 1, Wrong answer label: 0)  
$y_i$: Predicted probability (probability of belonging to the $i$-th class)

The $t_i$ is a **one-hot vector** where only one component is 1 and the others are zero.  
For example, in the classification problem (dog, pheasant, monkey), if the correct answer is pheasant, the correct label is (0, 1, 0); if the correct answer is monkey, the correct label is (0, 0, 1).

The $y_i$ is a probability. Thus, the possible values range from zero to one, and their sum is one.  

```math
0 \le y_i \le 1, \: \sum_{i=1}^k y_i = 1
```

Question:  
Find the cross-entropy for each of the two cases where the correct answer label is (0, 0, 1) and the predicted probabilities are (0.3, 0.3, 0.4) and (0.2, 0.2, 0.6) in a 3-class classification problem.  
Also, determine which case has the smaller loss (i.e., closer to the correct answer and prediction).

```python
#（0.3, 0.3, 0.4）の場合
-(0 * np.log(0.3) + 0 * np.log(0.3) + 1 * np.log(0.4))
```

```python
#（0.2, 0.2, 0.6）の場合
-(0 * np.log(0.2) + 0 * np.log(0.2) + 1 * np.log(0.6))
```

### Softmax function

In the previous section, we assumed that the predicted value $y_i$ is a probability. We use the softmax function to convert the input $x_i$ to a probability. The softmax function is defined by the following equation

```math
y_i = \frac{\exp (x_i)}{\sum_j^k \exp (x_j)}
```

We can then see that the following two equations (probability ranges from zero to one, all cases add up to one) for being indeed probabilities.

```math
0 \le y_i \le 1
```

```math
\sum_{i=1}^k y_i = 1
```

Incidentally, the derivative of the softmax function can be calculated as in the following equation.

```math
\frac{\partial y_i}{\partial x_j} = y_i (\delta_{ij} - y_j)
```

$\delta_{ij}$ is the Kronecker delta.

```math
\delta_{ij} = \left \{
    \begin{array}{ll}
    1 & (i=j) \\
    0 & (i \ne j)
    \end{array}
    \right.
```

Question:  
Verify that the derivative of the softmax function can be expressed by the following equation  

```math
\frac{\partial y_i}{\partial x_j} = y_i (\delta_{ij} - y_j)
```

### Differentiation of loss functions

![calc_graph](./images/softmax_cross_entropy.drawio.svg)

The cross-entropy is as follows

```math
L(y, t) = - \sum_{i=1}^k t_i \log y_i
```

This derivative is given by

```math
\frac{\partial L}{\partial y_j} = -\frac{t_j}{y_j}
```

The gradient we want to find is $\partial L/\partial x_i$.  
Along the way, we will substitute the derivative of cross-entropy and the derivative of the softmax function to organize.

```math
\begin{align}
\frac{\partial L}{\partial x_i} &= \sum_j \frac{\partial L}{\partial y_j} \frac{\partial y_j}{\partial x_i} \notag \\
&= \sum_j (-\frac{t_j}{y_j}) (y_j (\delta_{ij}-y_i)) \notag \\
&= \sum_j (-t_j \delta_{ij} + t_j y_i) \notag \\
&= y_i (\sum_j t_j) - t_i \notag \\
\end{align}
```

The $t_j$ is a one-hot vector (only the correct label is 1, all other components are zero).
Therefore, $\sum t_j =1$. Eventually, we obtain the following equation

```math
\frac{\partial L}{\partial x_i} = y_i - t_i
```

From the above, we can see that the gradient in the softmax function and cross-entropy is directly expressed as the error between the predictions and the correct label.  
For example, when $t_i=0$, $0 \leq y_i \leq 1$, so its slope is $\partial L/\partial x_i > 0$. Since the new $x_i$ is $x_i \leftarrow x_i-\eta (\partial L/\partial x_i)$ ($\eta$: learning rate), it works toward lowering the value of $x_i$, which corresponds to the probability of being classified in the $i$th class, more. When $t_i=1$, the opposite is true: it works in the direction of increasing the probability of being classified into the $i$-th class.

## Loss function for regression problems

The loss function of a regression problem that predicts a specific number has a squared error or absolute error.

### Squared error

In many cases, such as when the distribution of the observation error can be assumed to be [normal distribution](https://ja.wikipedia.org/wiki/%E6%AD%A3%E8%A6%8F%E5%88%86%E5%B8%83), this loss function is used.

```math
L(y, t)=\sum_{i=1}^k (y_i -t_i)^2 
```

### Absolute error

Loss function used when the distribution of the observation error is [Laplace distribution](https://ja.wikipedia.org/wiki/%E3%83%A9%E3%83%97%E3%83%A9%E3%82%B9%E5%88%86%E5%B8%83).

```math
L(y, t)=\sum_{i=1}^k |y_i -t_i| 
```

If $y_i$ has outliers that differ significantly from the correct values, the squared error is strongly affected. Which loss function to use should be decided after carefully understanding the nature of the data.
