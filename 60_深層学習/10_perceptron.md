
# perceptron

- [perceptron](#perceptron)
  - [What is a neuron?](#what-is-a-neuron)
  - [What is the Perceptron?](#what-is-the-perceptron)
  - [Representation of logical operations by a simple perceptron](#representation-of-logical-operations-by-a-simple-perceptron)
  - [Representation of Logical Operations with the Multilayer Perceptron](#representation-of-logical-operations-with-the-multilayer-perceptron)

## What is a neuron?

![neuron](https://www.visiblebody.com/hubfs/learn/assets/ja/nervous/%E3%83%8B%E3%83%A5%E3%83%BC%E3%83%AD%E3%83%B3-%E6%A7%8B%E9%80%A0.jpg)

"**Neuron** are the cells that make up the nervous system. Their function is specific to information processing and transmission, and is unique to animals. ...Neurons are divided into three main parts: the cell body, which contains the cell nucleus; **dendrites**, which receive input from other cells; and **axons**, which output to other cells."　([Wikipediaより](https://ja.wikipedia.org/wiki/%E7%A5%9E%E7%B5%8C%E7%B4%B0%E8%83%9E))  

1. It receives signals from several other neurons via dendrites.
2. The cell body processes those signals with weights and generates new signals.
3. The signal is output to other neurons via axons.

## What is the Perceptron?

The perceptron is a numerical model of the processing of neurons.
Given a signal $x_i$ from the other $i$th neuron and a weight $w_i$ for the processing of that signal, the output $y$ of the perceptron is represented by the following equation.

```math
y =  \left\{
\begin{array}{ll}
1 & (z \geq 0) \\
0 & (z \lt 0)
\end{array}
\right.
```

```math
z = \sum_i w_i x_i + b
```

$x_i$：Signal from the $i$-th neuron
$w_i$：Weight for that signal (weighting factor)
$b$：bias term

Define the step function $h(z)$ as follows

```math
h(z) =  \left\{
\begin{array}{ll}
1 & (z \geq 0) \\
0 & (z \lt 0)
\end{array}
\right.
```

The output of the **simple perceptron** is then obtained by the following equation

```math
y = h(\sum_i w_i x_i + b)
```

A graphical representation of the simple perceptron for the two-input case is shown below.

![perceptron](./images/perceptron.drawio.svg "Graphical representation of a simple perceptron with two inputs")

**Input layer**: the layer to which input signals are input  
**Output layer**: the layer to which the output signal is output
**Node**: represented as 〇
**Edge**: arrows connected to the node

A node corresponds to a neuron.
The numbers written on the edges are multiplied by the numbers of the node and transmitted to the next node.

## Representation of logical operations by a simple perceptron

Logical operations include AND, OR, XOR, etc., and are the operations shown in the table below. We will consider expressing these operations using the perceptron.

![論理演算](./images/logic_operation.drawio.svg)

Represents a simple perceptron using a class.

```python
class Perceptron:

    def __init__(self, w1, w2, b):
        self.w1 = w1
        self.w2 = w2
        self.b = b

    def out(self, x1, x2):
        z = self.w1 * x1 + self.w2 * x2 + self.b
        if z >= 0:
            return 1
        return 0
```

Functions to evaluate logical operations are also defined.

```python
def evaluate_func(fnc):
    dat = [[0, 0], [0, 1], [1, 0], [1, 1]]
    for x1, x2 in dat:
        print(f'(x1, x2): ({x1}, {x2}) out: {fnc(x1, x2)}')
```

Let us define and evaluate a simple perceptron that represents the logical operation AND.

```python
# AND回路の作成
p_and = Perceptron(0.5, 0.5, -0.75)
```

```python
# AND回路の評価
evaluate_func(p_and.out)
```

```python
(x1, x2): (0, 0) out: 0
(x1, x2): (0, 1) out: 0
(x1, x2): (1, 0) out: 0
(x1, x2): (1, 1) out: 1
```

You can see that the AND circuit is represented.  
Similarly, the OR circuit is also checked.

```python
# OR回路の作成
p_or = Perceptron(0.5, 0.5, -0.25)
```

```python
# OR回路の評価
evaluate_func(p_or.out)
```

```python
(x1, x2): (0, 0) out: 0
(x1, x2): (0, 1) out: 1
(x1, x2): (1, 0) out: 1
(x1, x2): (1, 1) out: 1
```

We can see that the AND and OR circuits can be separated by a single straight line. Such a circuit that can be separated by a single straight line is called **linearly separable**. In contrast, the XOR circuit requires two or more straight lines and is not linearly separable.

Question:  
Represent Negative OR (NOR) and Negative AND (NAND) using the perceptron.  

## Representation of Logical Operations with the Multilayer Perceptron

Consider multiple perceptrons connected together, with the output of a simple perceptron as the input to the next perceptron. A **multilayer perceptron** is one that has a **hidden layer** between the input and output layers, as shown in the figure. A structure in which all the perceptrons between each layer are connected to each other is called a **fully connected multilayer perceptron**, and a model with more than one intermediate layer is often called a **deep learning model**.

![multi_layer_perceptron](./images/multi_layer_perceptron_en.drawio.svg)

The multilayer perceptron is highly expressive and can represent arbitrary logic operations.  
Here we will consider representing an XOR circuit with a multilayer perceptron. Let us create an XOR circuit with the following graph diagram.  

![xor](./images/xor.drawio.svg)

```python
# 多層パーセプトロンによるXOR回路の作成
def mn_xor(x1, x2):
    p11 = Perceptron(0.5, 0.5, -0.75)
    p12 = Perceptron(0.5, 0.5, -0.25)
    p21 = Perceptron(-0.5, 0.5, -0.25)
    
    y1 = p11.out(x1, x2)
    y2 = p12.out(x1, x2)

    y_ans = p21.out(y1, y2)
    return y_ans
```

```python
# XOR回路の評価
evaluate_func(mn_xor)
```

```python
(x1, x2): (0, 0) out: 0
(x1, x2): (0, 1) out: 1
(x1, x2): (1, 0) out: 1
(x1, x2): (1, 1) out: 0
```

You can see that the XOR circuit can be represented using the multilayer perceptron.
