# Back propagation

In order to make accurate prediction, it is important to adjust the weights appropriately through training. In this section, we explain the error back propagation method in order to understand how to adjust the weights.

- [Back propagation](#back-propagation)
  - [Computational Graph](#computational-graph)
  - [What is the back propagation method?](#what-is-the-back-propagation-method)

## Computational Graph

Neural networks and deep learning demonstrate high performance, but their internals are composed of a collection of simple arithmetic operations such as addition, multiplication, and exponentiation. To visually understand the calculation process, a **computational graph** is sometimes used. A computational graph is a diagram consisting of **edges** and **nodes**, where the edges correspond to values and the nodes represent arithmetic operations such as multiplication.
One element of a neural network is represented by a computational graph.

![計算グラフ](./images/calculation_graph.drawio.svg)

```math
\begin{align}
z &= w x \notag \\
y &= \phi (z) \notag \\
\end{align}
```

The arrows in the figure are edges and the circles are nodes.
The input is $x$ and the weight is $w$. The multiplied value is $z\:(=wx)$, and the activation function applied is $y \:(=\phi (z))$.

## What is the back propagation method?

In a neural network, we need to adjust the weights so that the value of the loss function $L(y)$ becomes smaller. To do so, we need to find the gradient $(\partial L/\partial w)$ of the loss function with respect to the weights. If the gradient is positive, the weights need to be smaller, and if it is negative, they need to be larger.

**Back Propagation Method** is used to find the gradient for the loss weights.

Suppose we have the following computational graph of a neural network. To adjust the weights, we need to find its gradient $(\partial L/ \partial w_0)$ and $(\partial L/ \partial w_1)$.  

![誤差逆伝搬](./images/back_propagation_en.drawio.svg)

```math
\begin{align}
d L &= \frac{\partial L}{\partial y} d y \notag \\
&=\frac{\partial L}{\partial y} \frac{\partial y}{\partial z_1} d z_1 \notag \\
&=\frac{\partial L}{\partial y} \frac{\partial y}{\partial z_1} (\frac{\partial z_1}{\partial x_1} d x_1 +\frac{\partial z_1}{\partial w_1} d w_1) \notag \\
\end{align}
```

From this, the gradient of $(\partial L/ \partial w_1)$ can be obtained as

```math
\frac{\partial L}{\partial w_1} = \frac{\partial L}{\partial y} \frac{\partial y}{\partial z_1} \frac{\partial z_1}{\partial w_1}
```

Similarly, we will find the gradient of $(\partial L/ \partial w_0)$.  

```math
\begin{align}
d L &= \frac{\partial L}{\partial y} \frac{\partial y}{\partial z_1} \frac{\partial z_1}{\partial x_1} d x_1 \notag \\
& = \frac{\partial L}{\partial y} \frac{\partial y}{\partial z_1} \frac{\partial z_1}{\partial x_1} \frac{d x_1}{d z_0} d z_0 \notag \\
& = \frac{\partial L}{\partial y} \frac{\partial y}{\partial z_1} \frac{\partial z_1}{\partial x_1} \frac{d x_1}{d z_0} (\frac{\partial z_0}{\partial x_0} d x_0 + \frac{\partial z_0}{\partial w_0} d w_0) \notag \\
\end{align}
```

From the above, the gradient of $(\partial L/ \partial w_0)$ can be obtained as

```math
\frac{\partial L}{\partial w_0} = \frac{\partial L}{\partial y} \frac{\partial y}{\partial z_1} \frac{\partial z_1}{\partial x_1} \frac{d x_1}{d z_0} \frac{\partial z_0}{\partial w_0}
```

As can be seen from the formulas for finding the gradients $(\partial L/ \partial w_0)$ and $(\partial L/ \partial w_1)$, **the gradient is obtained by following the computational graph in reverse direction and applying the derivative at the nodes in turn**.

This corresponds to the [chain rule](https://ja.wikipedia.org/wiki/%E9%80%A3%E9%8E%96%E5%BE%8B) in differentiation, which states that when differentiating a composite function, its derivative is given by the product of its respective derivatives.

Incidentally, since $Z_0 = W_0 X_0$ and $Z_1 = W_1 X_1$, the following equation holds.

```math
\frac{\partial z_1}{\partial x_1} = w_1, \: \frac{\partial z_0}{\partial w_0} = x_0
```

Furthermore, assuming ReLU is used in the activation function, if $z_0, z_1 > 0$, its derivative is 1 because the activation function is ReLU.

```math
\frac{\partial y}{\partial z_1}, \: \frac{\partial x_1}{\partial z_0} = 1
```

From the above, for this condition, the slope $(\partial L/ \partial w_0)$ becomes a simple expression as follows.

```math
\frac{\partial L}{\partial w_0} = \frac{\partial L}{\partial y} w_1 x_0
```
