# 誤差逆伝搬

精度の良い予測を行うためには、学習を通じて重みを適切に調節していくことが重要になります。ここでは重みをどのように調節していくのかを理解するために、誤差逆伝搬法について説明します。

- [誤差逆伝搬](#誤差逆伝搬)
  - [計算グラフ](#計算グラフ)
  - [誤差逆伝搬法](#誤差逆伝搬法)

## 計算グラフ

ニューラルネットワーク、深層学習は高い性能を発揮しますが、その内部は足し算、掛け算、指数計算など単純な演算処理の集まりとして構成されています。その計算過程を視覚的に理解するために、**計算グラフ**が用いられることがあります。計算グラフは**辺**（**エッジ**）と**頂点**（**ノード**）からなる図であって、辺（エッジ）は値に対応して、頂点（ノード）は掛け算などの演算処理を表します。
ニューラルネットワークの一つの要素を計算グラフで表してみます。

![計算グラフ](./images/calculation_graph.drawio.svg)

```math
\begin{align}
z &= w x \\
y &= \phi (z)
\end{align}
```

図の矢印が辺（エッジ）で、丸が頂点（ノード）です。
入力が$x$で、重みが$w$です。掛け合わせたものが$z\:(=wx)$で、活性化関数を施したものが$y \: (=\phi (z))$です。

## 誤差逆伝搬法

ニューラルネットワークでは、損失関数$L(y)$の値が小さくなるように重みを調節する必要があります。そのためには**損失関数の重みに対する勾配**$(\partial L/\partial w)$を求める必要があります。勾配が正であれば重みは小さくする必要がありますし、負であれば大きくする必要があります。

**誤差逆伝搬法**は、損失の重みに対する勾配を求める際に用いられます。

次のようなニューラルネットワークの計算グラフがあったとします。重みを調節するためには、その勾配$(\partial L/ \partial w_0)$と$(\partial L/ \partial w_1)$を求める必要があります。  

![誤差逆伝搬](./images/back_propagation.drawio.svg)

```math
\begin{align}
d L &= \frac{\partial L}{\partial y} d y \\
&=\frac{\partial L}{\partial y} \frac{\partial y}{\partial z_1} d z_1 \\
&=\frac{\partial L}{\partial y} \frac{\partial y}{\partial z_1} (\frac{\partial z_1}{\partial x_1} d x_1 +\frac{\partial z_1}{\partial w_1} d w_1)
\end{align}
```

このことから、$(\partial L/ \partial w_1)$の勾配は、次の式として得られます。

```math
\frac{\partial L}{\partial w_1} = \frac{\partial L}{\partial y} \frac{\partial y}{\partial z_1} \frac{\partial z_1}{\partial w_1}
```

同様に$(\partial L/ \partial w_0)$の勾配を求めていきます。  

```math
\begin{align}
d L &= \frac{\partial L}{\partial y} \frac{\partial y}{\partial z_1} \frac{\partial z_1}{\partial x_1} d x_1 \\
& = \frac{\partial L}{\partial y} \frac{\partial y}{\partial z_1} \frac{\partial z_1}{\partial x_1} \frac{d x_1}{d z_0} d z_0 \\
& = \frac{\partial L}{\partial y} \frac{\partial y}{\partial z_1} \frac{\partial z_1}{\partial x_1} \frac{d x_1}{d z_0} (\frac{\partial z_0}{\partial x_0} d x_0 + \frac{\partial z_0}{\partial w_0} d w_0)
\end{align}
```

以上から、$(\partial L/ \partial w_0)$の勾配は、次の式として得られます。

```math
\frac{\partial L}{\partial w_0} = \frac{\partial L}{\partial y} \frac{\partial y}{\partial z_1} \frac{\partial z_1}{\partial x_1} \frac{d x_1}{d z_0} \frac{\partial z_0}{\partial w_0}
```

勾配$(\partial L/ \partial w_0)$と$(\partial L/ \partial w_1)$を求める式から分かるように、その**勾配を求めるためには計算グラフを逆向きにたどり、頂点（ノード）の箇所の微分を順番にかけていく**ことで得られます。

これは合成関数の微分をするときに、その導関数はそれぞれの導関数の積で与えられるという微分における[**連鎖律**（**れんさりつ**, **chain rule**）](https://ja.wikipedia.org/wiki/%E9%80%A3%E9%8E%96%E5%BE%8B)に相当します。

ちなみに、$z_0 = w_0 x_0$、$z_1 = w_1 x_1$なので、次の式が成り立ちます。

```math
\frac{\partial z_1}{\partial x_1} = w_1, \: \frac{\partial z_0}{\partial w_0} = x_0
```
さらに、活性化関数でReLUを用いたとして、$z_0, z_1 > 0$のとき、その微分は活性化関数がReLUであることから1になります。

```math
\frac{\partial y}{\partial z_1}, \: \frac{\partial x_1}{\partial z_0} = 1
```

以上から、この条件の場合、勾配$(\partial L/ \partial w_0)$は次のように簡単な表式になります。

```math
\frac{\partial L}{\partial w_0} = \frac{\partial L}{\partial y} w_1 x_0
```

