# Optimization technique

- [Optimization technique](#optimization-technique)
  - [Stochastic Gradient Descent](#stochastic-gradient-descent)
  - [Momentum](#momentum)
  - [Nesterov's Momentum](#nesterovs-momentum)
  - [AdaGrad](#adagrad)
  - [RMSProp](#rmsprop)
  - [Adam](#adam)

Of these, AdaGrad, RMSProp, and Adam can be viewed as adaptively changing the learning rate using hyperparameters. Adaptive change means that in the optimization process, the learning rate is large at the beginning and the search is done roughly, but as the optimization progresses, the learning rate becomes smaller and the search becomes more detailed.

## Stochastic Gradient Descent

Update formula for parameter $\theta$ of Stochastic Gradient Descent (SGD)

```math
\theta_{t+1} = \theta_t - \eta \frac{\partial L}{\partial \theta_t}
```

$\eta$ : Learning Rate  
$L$ : Loss Functions

## Momentum

```math
\begin{align}
v_{t+1} &= \alpha v_t - \eta \frac{\partial L}{\partial \theta_t} \notag \\
\theta_{t+1} &= \theta_t + v_{t+1} \notag \\
\end{align}
```

$\alpha$ : Momentum Coefficient

## Nesterov's Momentum

```math
\begin{align}
v_{t+1} &= \alpha v_t - \eta \frac{\partial L}{\partial (\theta_t + \alpha v_t)} \notag \\
\theta_{t+1} &= \theta_t + v_{t+1} \notag \\
\end{align}
```

The actual implementation would be the following equation

```math
\Theta_{t+1} = \theta_{t+1} + \alpha v_{t+1}
```

Define $\Theta_{t+1}$ as in the above equation. Then follow the following equation

```math
\begin{align}
v_{t+1} &= \alpha v_t - \eta \frac{\partial L}{\partial \Theta_t} \notag \\
\Theta_{t+1} &= \Theta_t + \alpha^2 v_t - (1+\alpha) \eta \frac{\partial L}{\partial \Theta_t} \notag \\
\end{align}
```

## AdaGrad

We take the second-order moments and add them up.  
As they are added, we see that the update rate of the learning becomes progressively smaller.  

```math
\begin{align}
h_{t+1} &= h_t + \frac{\partial L}{\partial \theta_t} \odot \frac{\partial L}{\partial \theta_t} \notag \\
\theta_{t+1} &= \theta_t - \eta \frac{1}{\epsilon + \sqrt{h_{t+1}}} \odot \frac{\partial L}{\partial \theta_t} \notag \\
\end{align}
```

$\epsilon$ : Small constant to prevent division by zero  
$\odot$ : Hadamard product (element-wise product)

## RMSProp

Taking second-order moments is the same as in AdaGrad, but they are added using hyperparameters.  
As in AdaGrad, the learning update rate is progressively smaller, but can be adjusted with hyperparameters.  

```math
\begin{align}
h_{t+1} &= \rho h_t + (1-\rho) \frac{\partial L}{\partial \theta_t} \odot \frac{\partial L}{\partial \theta_t} \notag \\
\theta_{t+1} &= \theta_t - \eta \frac{1}{\epsilon + \sqrt{h_{t+1}}} \odot \frac{\partial L}{\partial \theta_t} \notag \\
\end{align}
```

## Adam

We take the first and second order moments and add them together using the hyperparameters $\rho_1$ and $\rho_2$. We also perform a bias correction.

```math
\begin{align}
m_{t+1} &= \rho_1 m_t + (1-\rho_1) \frac{\partial L}{\partial \theta_t} \notag \\
v_{t+1} &= \rho_2 v_t + (1-\rho_2) \frac{\partial L}{\partial \theta_t} \odot \frac{\partial L}{\partial \theta_t} \notag \\
\end{align}
```

Equation for bias correction

```math
\begin{align}
\hat{m}_{t+1} &= \frac{m_{t+1}}{1-\rho_1^t} \notag \\
\hat{v}_{t+1} &= \frac{v_{t+1}}{1-\rho_2^t} \notag \\
\end{align}
```

$t$ : 学習ステップ  

From the above, we have the following equation

```math
\theta_{t+1} = \theta_t - \eta \frac{1}{\sqrt{\hat{v}_{t+1}} + \epsilon} \odot \hat{m}_{t+1}
```

[徹底攻略ディープラーニングE資格エンジニア問題集 第2版](https://book.impress.co.jp/books/1120101184), p. 213参照