# Overlearning and Regularization

This section describes overlearning, which degrades generalization performance, and describes three types of regularization to deal with it: parameter norm penalty, early termination, and dropout.  

- [Overlearning and Regularization](#overlearning-and-regularization)
  - [Overlearning](#overlearning)
  - [Regularization](#regularization)
    - [Parameter norm penalty](#parameter-norm-penalty)
      - [$L^2$ regularization](#l2-regularization)
      - [$L^1$ egularization](#l1-egularization)
  - [Early stopping](#early-stopping)
    - [Dropout](#dropout)

## Overlearning

"**Overfitting**, or **overtraining** refers to a state in statistics and machine learning in which training data is trained on, but unknown data (test data) is not fitted or generalized. It refers to the state of not being able to adapt or generalize to unknown data (test data). It is caused by a lack of generalization ability.

One reason for this is that the model is too complex and has too many degrees of freedom compared to the number of training data, such as too many mediating variables in the fit to the statistical model. An unreasonable and erroneous model may be a perfect fit if it is too complex compared to the available data.

Synonyms are **underfitting** and **underlearning**."([Wikipedia](https://ja.wikipedia.org/wiki/%E9%81%8E%E5%89%B0%E9%81%A9%E5%90%88))

- Too much conformity to training data and not enough conformity to unknown data (test data).
- A model with too many degrees of freedom is prone to overlearning.

![train_valid](./images/split_data_validation_holdout_en.drawio.svg)

We have previously mentioned that the model is trained using training data and evaluated using validation data.  

As training continues, the following occurs during the first epoch
__1. Losses in both training and validation data decrease.__
After further learning, the following will occur  
__2. The loss of training data decreases, but the loss of validation data increases.__

![over_fitting](./images/over_fitteng_en.drawio.svg)

This state of increasing loss of validation data due to over-fitting the training data is called **over-learning**.  
Since an over-trained model has reduced performance (generalization performance) on unknown data, measures to avoid over-training are necessary.  

## Regularization

Regularization is the general term for fulfilling some constraint on parameters with the goal of controlling over-fitting. Three types of regularization are discussed here: parameter norm penalties, early termination, and dropout.  

### Parameter norm penalty

One measure to avoid over-training is a parameter norm penalty. This is a method of penalizing the loss function so that the parameters of the model do not become too complex to fit the training data. By increasing the penalty, the complexity is limited and generalization performance is improved.  
The loss function with regularization $\tilde{J}(\theta ; X, y)$ is given by

```math
\tilde{J}(\theta ; X, y) = J(\theta ; X, y) + \alpha \Omega(\theta )
```

$\theta $ : Model Parameters
$X, y$ : Inputs and Outputs
$J(\theta ; X, y)$ : Original loss function
$\alpha$ : Hyperparameter that controls the strength of regularization (> 0, positive value)
$\Omega$ : regularization term

The model parameter $\theta$ contains the weights $w$ and the bias, but in general the penalty is applied to the weights and not to the bias. This is because the bias sometimes needs to be large.  

#### $L^2$ regularization

Also called Ridge regression. It is a simple and general regularization that penalizes the square of the parameter. The regularization term is given by

```math
\Omega (w) = \frac{1}{2} ||w||_2^2 = \frac{1}{2} \sum w_i^2
```

#### $L^1$ egularization

Also called Lasso regression. It is a regularization that penalizes the absolute values of the parameters. The regularization term is given by  

```math
\Omega (w) = ||w||_1 = \sum |w_i|
```

The $L^1$ regularization is characterized by the ease of setting some of the weights $w$ to zero, and also by dimensionality reduction and other effects.

## Early stopping

**early stopping** is a method of monitoring the losses in the validation data while iterating through the epochs and terminating the calculation when the losses start to increase.  
When the loss increases for a predetermined number of consecutive epochs, the calculation is terminated and the model with the lowest loss is adopted, for example.  
However, increasing the default number of times may result in even smaller losses, so the changes in losses in the validation data should be carefully observed when making such settings.  

The graph below shows an example of early stopping.  
(The curves are not identical because they were run with different random seeds.)

![overfitting_early_stopping](./images/over_fitting_early_stop_en.drawio.svg)

### Dropout

One measure to avoid overlearning is dropout. Dropout randomly zeros the output of a unit by a certain percentage (dropout_ratio) at every training step during training. It also uses all units during prediction, but uniformly multiplies all parameters by the percentage of units not eliminated (1-dropout_ratio).dropout_ratio is a hyperparameter during model design.  

![dropout](./images/dropout.drawio.svg)

This is expected to regularize the model because it avoids the large influence of certain units during forecasting.  
With dropouts, it can also be said that the model is trained on various networks depending on the combination of units being dropped. Thus, the final result is a single network, but the prediction results may be interpreted as an ensemble average of multiple networks.  
