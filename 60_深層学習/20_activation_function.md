# Activation function

In the neural network, the input signals from multiple nodes are weighted, and the sum of the inputs is obtained. Furthermore, the value is converted into an output signal using the sum of inputs. This conversion function is called the **activation function**. The perceptron's activation function is a step function. Here we will discuss some activation functions.  

```math
y = h(\sum_i w_i x_i + b)
```

$x_i$: Input signal from the $i$-th node
$w_i$: Its weight element
$h(z)$: Activation function

## step function

```math
h(x) =  \left \{
\begin{array}{ll}
1 & (x \geq 0) \\
0 & (x \lt 0)
\end{array}
\right.
```

Graph of a step function
![step](./images/func_step.png)

## Sigmoid function

```math
h(x) = \frac{1}{1+e^{-x}}
```

Graph of sigmoid function
![sigmoid](./images/func_sigmoid.png)

Differentiation of sigmoidal functions

```math
h'(x)=\frac{e^{-x}}{(1+e^{-x})^2}= h(x) (1-h(x))
```

Graph of the derivative of a sigmoid function

![sigmoid_diff](./images/func_sigmoid_diff.png)

## Hyperbolic tangent function

```math
h(x) = \tanh(x)=\frac{e^x-e^{-x}}{e^x+e^{-x}}
```

Graph of tanh

![tanh_func](./images/func_tanh.png)

tanh関数の微分

```math
h'(x) = 1 - (\tanh (x))^2
```

Graph of the derivative of the tanh function

![tanh_diff](./images/func_tanh_diff.png)

## ReLU Function

ReLU (Rectified Linear Unit)

```math
h(x) =  \left \{
\begin{array}{ll}
x & (x \geq 0) \\
0 & (x \lt 0)
\end{array}
\right.
```

Graph of ReLU function
![ReLU_func](./images/func_ReLU.png)

Graph of the derivative of the ReLU function
![ReLu_func_diff](./images/func_ReLU_diff.png)

## Comparison of activation functions and vanishing gradient

The characteristics of each activation function are summarized in the table below.

|function name|value range|gradient value range|
|---|---|---|
|step function| 0, 1| 0|
|sigmoid|(0, 1)|(0, 0.25]|
|tanh|(-1, 1)|(0, 1.0]|
|ReLU|[0,$\infty$)|0, 1|

To optimize the weights, the gradient must be determined.
The sigmoid and tanh functions show that their gradients approach zero for large values of input. This is known as the **gradient vanishing problem** and suggests that the search for weights may no longer be performed properly.
ReLU, on the other hand, does not decay its gradient as the input increases.

For these reasons, the **ReLU function seems to be more widely used** than the step or sigmoid functions.
