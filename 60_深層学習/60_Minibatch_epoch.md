# Minibatch Algorithms and Epochs

- [Minibatch Algorithms and Epochs](#minibatch-algorithms-and-epochs)
  - [Minibatch Algorithm](#minibatch-algorithm)
    - [Stochastic Gradient Descent](#stochastic-gradient-descent)
  - [Epoch](#epoch)
    - [operation check](#operation-check)

## Minibatch Algorithm

The method of evaluating the loss function collectively using all the data in a data set is called a **batch algorithm**.  
The method of evaluating each piece of data in turn is called an **online algorithm**.  
An intermediate method, in which multiple pieces of data are acquired and evaluated, is called a **mini-batch algorithm**.  

|Algorithm Name|Evaluation Method|
|:---|:---|
|Batch Algorithm|Evaluate all data together|
|Online Algorithm|Evaluated one data at a time|
|Minibatch Algorithm |Evaluating multiple pieces of data together|

In many cases, a **mini-batch algorithm** is employed, which is computationally faster.  
In the minibatch algorithm, a group of data is called a **mini-batch** and the number of data in the mini-batch is called the **batch size**.

---
Batch Algorithm

![batch_all](./images/batch_all_en.drawio.svg)

---
Online Algorithm

![batch_online](./images/batch_online_en.drawio.svg)

---
Minibatch Algorithm

![batch_mini](./images/batch_mini_en.drawio.svg)

### Stochastic Gradient Descent

We have introduced three algorithms: the batch algorithm, the online algorithm, and the mini-batch algorithm, each of which has a corresponding gradient descent method.  

**Steepest Descent** is one of the gradient descent algorithms for optimization problems. It evaluates the gradient using **all data**. Because it evaluates the gradient using all data, its computation is time-consuming.

**Stochastic Gradient Descent** (**SGD**) is a method of updating parameters by evaluating the gradient from **a single piece of data** taken at random. Since the gradient is evaluated from a single data set, it may not always be the most appropriate gradient, but the computation time is faster.

**Mini-Batch SGD** is a method that lies between the steepest descent and stochastic gradient descent methods and evaluates gradients from **multiple randomly acquired data** and updates parameters. Since the gradient is evaluated from multiple data, it calculates a more appropriate gradient than the stochastic gradient descent method, and since it evaluates from a subset of the data, the calculation time is faster than the steepest descent method.

|Descent method|Data used|Merits and demerits|
|:---|:---|:---|
|Steepest Descent|All data|（☓）It takes time to compute. <br> （〇）Gradient evaluation is accurate. <br> （△）Prone to local solutions|
|SGD|One data at random|（〇）Computation is fast. <br> （☓）Not always the optimal gradient. <br> （〇）Local solution may be avoided.|
|Mini-Batch SGD|Multiple data at random|Advantages and disadvantages of intermediate|

## Epoch

An epoch is the number of times the training data has been used. For example, if there are 100 training data, training using all 100 data means that one epoch has been completed. When training is completed again using all 100 data, 2 epochs have been completed.

### operation check

Using a simple program in PyTorch, we will see how the mini-batch and epochs actually change.

Import the required modules.

```python
import torch
from torch.utils.data import TensorDataset
from torch.utils.data import DataLoader
```

Prepare a set of features and target data with 50 data. For simplicity, we use the same values of 0 to 49 for the features and the target data.

```python
X= torch.arange(0, 50, 1)
y = torch.arange(0, 50, 1)
```

Define the data set and data loader. The batch size is set to 16 and the data is set to be retrieved randomly.

```python
dataset = TensorDataset(X, y)
dataloader = DataLoader(dataset, batch_size=16, shuffle=True)
```

We will run the mini-batch algorithm with epochs as 10.

```python
for epoch in range(10):
    print(f'start of epoch: {epoch}')
    
    for x_batch, y_batch in dataloader:
        print('x_batch:', x_batch)
```

```python
start of epoch: 0
x_batch: tensor([11, 20, 10, 36, 25, 24, 13, 40, 47, 41, 21, 33, 12, 16, 49, 34])
y_batch: tensor([11, 20, 10, 36, 25, 24, 13, 40, 47, 41, 21, 33, 12, 16, 49, 34])
x_batch: tensor([ 9, 48, 44, 30, 45, 35, 18, 22,  4, 17,  5, 27,  8, 37,  7,  3])
y_batch: tensor([ 9, 48, 44, 30, 45, 35, 18, 22,  4, 17,  5, 27,  8, 37,  7,  3])
x_batch: tensor([14, 31, 29,  6, 38, 26, 39, 19, 15, 43, 46,  0, 32, 42,  1,  2])
y_batch: tensor([14, 31, 29,  6, 38, 26, 39, 19, 15, 43, 46,  0, 32, 42,  1,  2])
x_batch: tensor([23, 28])
y_batch: tensor([23, 28])
start of epoch: 1
x_batch: tensor([12, 42, 30,  9, 41,  5, 45,  1, 10, 34, 17,  3, 26, 15, 20, 25])
y_batch: tensor([12, 42, 30,  9, 41,  5, 45,  1, 10, 34, 17,  3, 26, 15, 20, 25])
x_batch: tensor([46, 32, 31, 43,  4, 24, 36, 33, 14, 13, 37, 29, 11, 19, 39,  7])
y_batch: tensor([46, 32, 31, 43,  4, 24, 36, 33, 14, 13, 37, 29, 11, 19, 39,  7])
x_batch: tensor([ 0, 22,  6, 48, 23, 47, 35, 38, 21, 18, 16, 28, 44,  8, 27, 40])
y_batch: tensor([ 0, 22,  6, 48, 23, 47, 35, 38, 21, 18, 16, 28, 44,  8, 27, 40])
x_batch: tensor([ 2, 49])
y_batch: tensor([ 2, 49])
start of epoch: 2
x_batch: tensor([14,  5, 25, 27, 31, 46,  9, 22, 15, 26, 37,  1, 13, 49, 23, 17])
y_batch: tensor([14,  5, 25, 27, 31, 46,  9, 22, 15, 26, 37,  1, 13, 49, 23, 17])
x_batch: tensor([21,  7, 38, 16, 28, 29,  4, 40, 42,  8, 36, 48, 20, 34, 10, 39])
y_batch: tensor([21,  7, 38, 16, 28, 29,  4, 40, 42,  8, 36, 48, 20, 34, 10, 39])
x_batch: tensor([35, 43, 44, 30, 32,  2, 45, 11, 12,  3, 33, 41, 24, 47, 19,  6])
y_batch: tensor([35, 43, 44, 30, 32,  2, 45, 11, 12,  3, 33, 41, 24, 47, 19,  6])
...
x_batch: tensor([ 6, 17, 32, 20, 38, 36,  7, 19, 21, 46, 26, 40,  4, 18, 28, 24])
y_batch: tensor([ 6, 17, 32, 20, 38, 36,  7, 19, 21, 46, 26, 40,  4, 18, 28, 24])
x_batch: tensor([48, 45])
y_batch: tensor([48, 45])
Output is truncated. View as a scrollable element or open in a text editor. Adjust cell output settings...
```

The results of this output show the following

- The batch size is 16, as per the settings.
- The mini batches are taken at random (not in order).
- The last mini-batch is the one with 2 data, since there are 3 mini-batches with 16 data for 50 data.
- After all 50 data have been processed, the next epoch will be processed.

Question:  
Please check the behavior when varying batch size or number of data, or when shuffling is disabled.  
