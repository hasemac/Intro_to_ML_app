# Stride and Padding

Stride and padding can be specified when performing convolution.

## Stride

Stride specifies the width of the image to be scanned. For example, setting the stride to 3 means that convolution will be performed every 3 pixels. Here is an example when the stride is set to $(2 \times 2)$.  

![stride](./images/stride.drawio.svg)

Strides are coarser traversed by the kernel, but reduce computational cost.

$d_H \times d_W$ : Vertical and horizontal stride

input： $H_I \times W_I \times C_I$
kernel： $H_K \times W_K \times C_I \times N_K$

The output will be in the following format

output ：  

```math
\left( \frac{H_I - H_K}{d_H}+1 \right) 
\times 
\left( \frac{W_I - W_K}{d_W}+1 \right)
\times N_K
```

## Padding

When convolution was performed, the size of the output was smaller than the size of the input. Below is a case where a kernel of $3 \times 3$ in size is applied to an input of $5 \times 5$ in size, and you can see that the output size is $3 \times 3$ smaller than the input.  

![padding](./images/padding_1.drawio.svg)  

Padding adds a margin around the input. This allows the output to remain the same size.  
Below is the case of a $3 \times 3$ kernel, but by adding $1 \times 1$ of padding, you can see that the output is $5 \times 5$, the same size as the input.  

![padding](./images/padding_2.drawio.svg)  

$d_H \times d_W$ : Vertical and horizontal stride
$p_H \times p_W$ : Vertical and horizontal padding  

input： $H_I \times W_I \times C_I$
kernel： $H_K \times W_K \times C_I \times N_K$

If both stride and padding are considered, the output is as follows  

output：  

```math
\left( \frac{H_I + 2 p_H - H_K}{d_H}+1 \right) 
\times 
\left( \frac{W_I + 2 p_W - W_K}{d_W}+1 \right)
\times N_K
```  

The factor $2$ is to create margins at the top, bottom, left and right respectively.  
