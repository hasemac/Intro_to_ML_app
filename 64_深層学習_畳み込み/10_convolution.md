# Convolution

## convolutional neural network

**Convolutional Neural Network** (**CNN**) refers to a neural network that includes **convolutional processing**. Just as the layer that performs full-connection process is called the fully-connectd layer, the layer that performs the convolution process is called the **convolution layer**.  

## What is convolution?

---

**Convolution** is a binary operation that adds a function $g$ to a function $f$ while translating.

### Continuous

The convolution $f ∗ g$ of continuous functions $f$ and $g$ is defined as follows

```math
(f*g)(t)=\int f(\tau)g(t-\tau) d \tau
```

It is also called convolution integral, **composite product**, or **overlap integral** because it combines two functions using an integral.

### Discrete

The convolution $f ∗ g$ of discrete signals $f$ and $g$ is defined as follows

```math
(f*g)(m) = \sum f(n)g(m-n)
```

It is defined in the same way using the sums instead of integrals. Therefore, it is also called **convolution sum** and **overlap sum**.
([Wikipedia](https://ja.wikipedia.org/wiki/%E7%95%B3%E3%81%BF%E8%BE%BC%E3%81%BF))

---

- Example of convolution
  Assuming that the impulse response $g(t)$ of a linear system is known, when an arbitrary input signal $x(t)$ is added to the system, its output signal $y(t)$ is given by the following convolution integral  
  
  ```math
  y(t)=\int_0^t x(\tau)g(t-\tau) d \tau
  ```

  $x(t)$ : input signal
  $g(t)$ : Impulse response of the system
  $y(t)$ : output signal

Consider applying this convolution to a 2-dimensional image. If the input is a 2-dimensional image $I$ and the convolution function is $K$, the output $S(i, j)$ of the 2-dimensional convolution can be expressed by the following equation  

```math
S(i, j) = (I*K)(i, j) = \sum_m \sum_n I(m, n) K(i-m, j-n)
```

$I(m, n)$ denotes the value of image $I$ in the $m$-th row and $n$-th column.　　
$K$ is called a **kernel**. Also, a multidimensional array such as $I$ and $K$ will be referred to as a **tensor** from now on.  
The convolution computation is commutative and can be expressed as  

```math
S(i, j) = (K*I)(i, j) = \sum_m \sum_n I(i-m, j-n) K(m, n)
```

In this equation, image $I$ and kernel $K$ are inverted and used in the calculation. On the other hand, many neural networks use the following equation as **cross-correlation**.  

$I$ : Input image (tensor)
$K$ : Kernel (tensor)
$S$ : Output image (tensor)

```math
S(i, j) = (K*I)(i, j) = \sum_m \sum_n I(i+m, j+n) K(m, n)
```

### Calculation example

![example_of_CNN](./images/ex_cnn.drawio.svg)

### Advantages of convolution

The following is a case where a diagonal-shaped (＼) kernel is acted on a cross-shaped (X) input image.

![cnn_pattern](./images/cnn_pattern.drawio.svg)

We can see that the value is high at the points that have a similar shape to the kernel. In other words, we can say that the convolution process extracts kernel-like features from the input.  
In a convolutional neural network, this **kernel is a parameter that can be learned** to determine what features to extract from the input.  
Kernels in convolution are typically said to be $3 \times 3$ or $5 \times 5$ in size. A single convolution process extracts the local structure of the image, but multiple iterations of that convolution process can result in learning **complex patterns**.  

## Convolution with multiple kernels

If there is more than one kernel, then as many outputs as there are kernels.
Below is an example of three kernels acting on one input, resulting in three outputs.  

![kernels](./images/kernels.drawio.svg)

$H_I$ : Input image height
$W_I$ : Input image width
$H_K$ : Kernel height
$W_K$ : Kernel width
$N_K$ : Number of kernels

input：$H_I \times W_I$
output：$(H_I - H_K +1) \times (W_I - W_K +1) \times N_K$

## Convolution with multiple channel inputs

In practice, there is not necessarily only one input. For example, in a color image in RGB format, there will be three inputs, one for each of the colors red, green, and blue. In this case, we would prepare a kernel that acts on each input to perform the convolution process.  
Below is an example of preparing a kernel for each input to obtain a single output.

![channels](./images/channels.drawio.svg)

A single pixel may have multiple information such as colors and features, which are called **channels**; a color image in RGB format will have three channels.

$C_I$ : Number of input image channels

If the format of the input is $H_I \times W_I \times C_I$ and the format of the kernel is $H_K \times W_K \times C_I \times N_K$, then the format of the input and output is as follows. The output will have $N_K$ channels.　　

入力：$H_I \times W_I \times C_I$
出力：$(H_I - H_K +1) \times (W_I - W_K +1) \times N_K$
