# Pooling

## What is Pooling?

Pooling is a method of calculating a certain statistic for a defined region. Commonly used methods include **max pooling** and **average pooling**. As the name implies, they calculate the maximum and average values in a region, respectively.  
The stride and padding ideas introduced in the convolution process also apply here.  

$d_H \times d_W$ : Vertical and horizontal stride
$p_H \times p_W$ : padding  

input： $H_I \times W_I \times C_I$

The output at this time is as follows  
output：  

```math
\left( \frac{H_I + 2 p_H - H_K}{d_H}+1 \right) 
\times 
\left( \frac{W_I + 2 p_W - W_K}{d_W}+1 \right)
\times C_I
```  

## Examples of Pooling

Pooling size: $2 \times 2$
stride: $2 \times 2$
padding: zero
An example of max pooling is shown below.

![pooling](./images/pooling_1.drawio.svg)

Pooling features and other information are as follows.

- A commonly used pooling size is $2 \times 2$.  
- It has no parameters to be studied.
- The number of channels before and after processing does not change.
- Pooling allows for feature concentration while reducing vertical and horizontal size.
- It is invariant to minute changes in input position.

The following example illustrates max pooling when the input is shifted to the right by one pixel.

![pooling](./images/pooling_2.drawio.svg)  

It does not mean that the two results are exactly the same, but we can see that they are similar.  
