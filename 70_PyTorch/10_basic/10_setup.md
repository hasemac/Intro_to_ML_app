# セットアップ

## PyTorchのインストール

公式サイト[PyTorch](https://pytorch.org/)を参考にセットアップしてください。

Webページ中で、自分の環境にあったものを選択すると下部にインストールするためのコマンドが表示されます。

![setup](./images/setup.PNG)

安定版, windows, pip, Python, CPUという条件の場合、次のコマンドでインストールできるようです。

```shell
pip3 install torch torchvision torchaudio
```

モジュールをインポートしてエラーが発生しないことを確認します。

```python
import torch
```

- 次のエラーが発生した場合
OSError: [WinError 126] 指定されたモジュールが見つかりません。 Error loading "c:\code\Intro_to_ML_app\.venv\Lib\site-packages\torch\lib\fbgemm.dll" or one of its dependencies.
この場合は[OSError: [WinError 126] Error loading “foo\bar\fbgemm.dll” or one of its dependencies.　の対処法](https://knowledge-oasis.net/fix/fix-winerror-126/)を参考にして[Microsoft Visual C++ 再頒布可能パッケージ](https://learn.microsoft.com/ja-jp/cpp/windows/latest-supported-vc-redist?view=msvc-170#latest-microsoft-visual-c-redistributable-version)をインストールします。  
これでも改善しない場合は、[Visual Studio Community](https://visualstudio.microsoft.com/ja/vs/community/)の特に"C++によるデスクトップ開発"をインストールします。このインストールにより`c:\windows\system32`の箇所に、`libomp140.x86_64.dll`がインストールされます。
![community](./images/VS_community.png)

## tensorboardのインストール

学習した履歴などをグラフに表示してくれるソフトウェアです。

```shell
pip install tensorboard
```
