# GPU版PyTorch環境の構築

- [GPU版PyTorch環境の構築](#gpu版pytorch環境の構築)
  - [1. CPU版pytorchがインストールできることを確認](#1-cpu版pytorchがインストールできることを確認)
  - [2. GPUのドライバのインストール](#2-gpuのドライバのインストール)
  - [3. CUDA Toolkitをインストール](#3-cuda-toolkitをインストール)
  - [4. cuDNNをインストール](#4-cudnnをインストール)
  - [5. PyTorchをインストール](#5-pytorchをインストール)
  - [6. PyTorchがGPUを認識していることの確認](#6-pytorchがgpuを認識していることの確認)
  - [7. CPUとGPUの切り替え](#7-cpuとgpuの切り替え)

## 1. CPU版pytorchがインストールできることを確認

[pytorch](https://pytorch.org/)のページからコマンドをコピーしてインストールしてみます。

```shell
pip3 install torch torchvision torchaudio
```

インポートしてエラーが発生しないことを確認します。

```python
import torch
```

- 次のエラーが発生した場合
OSError: [WinError 126] 指定されたモジュールが見つかりません。 Error loading "c:\code\Intro_to_ML_app\.venv\Lib\site-packages\torch\lib\fbgemm.dll" or one of its dependencies.
この場合は[OSError: [WinError 126] Error loading “foo\bar\fbgemm.dll” or one of its dependencies.　の対処法](https://knowledge-oasis.net/fix/fix-winerror-126/)を参考にして[Microsoft Visual C++ 再頒布可能パッケージ](https://learn.microsoft.com/ja-jp/cpp/windows/latest-supported-vc-redist?view=msvc-170#latest-microsoft-visual-c-redistributable-version)をインストールします。  
これでも改善しない場合は、[Visual Studio Community](https://visualstudio.microsoft.com/ja/vs/community/)の特に"C++によるデスクトップ開発"をインストールします。このインストールにより`c:\windows\system32`の箇所に、`libomp140.x86_64.dll`がインストールされます。
![community](./images/VS_community.png)

## 2. GPUのドライバのインストール

あらかじめGPUボードが付属しているPCを購入した場合は、すでにドライバがインストールされているものと思いますが、新たにGPUボードを購入した場合やドライバの更新があった場合は、GPUのドライバをインストールします。  
ターミナルで`nvidia-smi`と入力してドライバのバージョンと、**インストール可能なCUDA Toolkitのバージョンを確認**します。下の例ではCUDA Toolkitは`12.4`のバージョンまでがインストール可能であることがわかります。  

```shell
PS C:\code\Intro_to_ML_app> nvidia-smi
Fri Aug  2 12:54:44 2024       
+-----------------------------------------------------------------------------------------+
| NVIDIA-SMI 552.23                 Driver Version: 552.23         CUDA Version: 12.4     |
|-----------------------------------------+------------------------+----------------------+
| GPU  Name                     TCC/WDDM  | Bus-Id          Disp.A | Volatile Uncorr. ECC |
| Fan  Temp   Perf          Pwr:Usage/Cap |           Memory-Usage | GPU-Util  Compute M. |
|                                         |                        |               MIG M. |
|=========================================+========================+======================|
|   0  NVIDIA T1000                 WDDM  |   00000000:01:00.0  On |                  N/A |
| 34%   37C    P8             N/A /   50W |     828MiB /   4096MiB |     10%      Default |
|                                         |                        |                  N/A |
+-----------------------------------------+------------------------+----------------------+

+-----------------------------------------------------------------------------------------+
| Processes:                                                                              |
|  GPU   GI   CI        PID   Type   Process name                              GPU Memory |
|        ID   ID                                                               Usage      |
|=========================================================================================|
|    0   N/A  N/A      1200    C+G   ...wekyb3d8bbwe\XboxGameBarWidgets.exe      N/A      |
|    0   N/A  N/A      1844    C+G   ...Programs\Microsoft VS Code\Code.exe      N/A      |
|    0   N/A  N/A      3724    C+G   ...oogle\Chrome\Application\chrome.exe      N/A      |
|    0   N/A  N/A      4924    C+G   ...siveControlPanel\SystemSettings.exe      N/A      |
|    0   N/A  N/A      6868    C+G   ...42.0_x64__8wekyb3d8bbwe\GameBar.exe      N/A      |
|    0   N/A  N/A      7268    C+G   C:\Windows\System32\svchost.exe             N/A      |
|    0   N/A  N/A      7744    C+G   ...ell\Dell Peripheral Manager\DPM.exe      N/A      |
|    0   N/A  N/A      8264    C+G   C:\Windows\explorer.exe                     N/A      |
|    0   N/A  N/A     10056    C+G   ...nt.CBS_cw5n1h2txyewy\SearchHost.exe      N/A      |
|    0   N/A  N/A     10080    C+G   ...2txyewy\StartMenuExperienceHost.exe      N/A      |
|    0   N/A  N/A     11988    C+G   ...t.LockApp_cw5n1h2txyewy\LockApp.exe      N/A      |
|    0   N/A  N/A     13680    C+G   ...ekyb3d8bbwe\PhoneExperienceHost.exe      N/A      |
|    0   N/A  N/A     15856    C+G   ...crosoft\Edge\Application\msedge.exe      N/A      |
|    0   N/A  N/A     16188    C+G   ...les\Microsoft OneDrive\OneDrive.exe      N/A      |
|    0   N/A  N/A     16892    C+G   ...CBS_cw5n1h2txyewy\TextInputHost.exe      N/A      |
|    0   N/A  N/A     17616    C+G   ...n\126.0.2592.113\msedgewebview2.exe      N/A      |
|    0   N/A  N/A     21768    C+G   ...5n1h2txyewy\ShellExperienceHost.exe      N/A      |
|    0   N/A  N/A     24716    C+G   ...t Visual Studio\Installer\setup.exe      N/A      |
|    0   N/A  N/A     27384    C+G   ...8bbwe\SnippingTool\SnippingTool.exe      N/A      |
+-----------------------------------------------------------------------------------------+
PS C:\code\Intro_to_ML_app>
```

## 3. CUDA Toolkitをインストール

CUDA Toolkitは、NVIDIA社製GPUカードを汎用的な数値計算目的で利用したい場合に必要となるソフトウェアです。インストールする前にPyTorchのインストールコマンドの画面を開き、利用可能なCUDA Toolkitのバージョンを確認します。下の例では`11.8`、`12.1`、`12.4`が利用可能ということがわかります。

![pytorch](./images/pytorch.png)

[CUDA Toolkit Archive](https://developer.nvidia.com/cuda-toolkit-archive)から必要とするバージョンのToolkitをダウンロードしてインストールします。インストール後、ターミナルで`nvcc -V`などコマンドを入力して、適切にインストールされたことを確認します。下の例では`12.4`がインストールされていることがわかります。  

```shell
PS C:\code\Intro_to_ML_app> nvcc -V
nvcc: NVIDIA (R) Cuda compiler driver
Copyright (c) 2005-2024 NVIDIA Corporation
Built on Tue_Feb_27_16:28:36_Pacific_Standard_Time_2024
Cuda compilation tools, release 12.4, V12.4.99
Build cuda_12.4.r12.4/compiler.33961263_0
PS C:\code\Intro_to_ML_app>
```

## 4. cuDNNをインストール

CUDA Deep Neural Network (cuDNN) は、ディープラーニング向けのアルゴリズムを高速化するためのライブラリです。[cuDNN Archive](https://developer.nvidia.com/cudnn-archive)などからzipファイルをダウンロードして`C:\Program Files\NVIDIA GPU Computing Toolkit`などの適切な場所に解凍します。次に**システム環境変数のPath**にディレクトリへのパスを追加します。下の例では、  
`C:\Program Files\NVIDIA GPU Computing Toolkit\cuDNN\bin`
というPathを追加していることがわかります。

![path](./images/cudnn_path.png)

**VS codeのターミナルではなくて**コマンドプロンプトにて`where cudnn64*.dll`などと入力して、下のように却ってきたらパスが通っていることになります。  

```shell
C:\Users\hasegawa>where cudnn64_*.dll
C:\Program Files\NVIDIA GPU Computing Toolkit\cuDNN\bin\cudnn64_8.dll

C:\Users\hasegawa>
```

## 5. PyTorchをインストール

すでにCPU版のPyTorchがインストールされている場合はそれらを削除します。

```shell
pip3 uninstall torch torchvision torchaudio
```

GPUを含んだPyTorchのインストールコマンドを確認して実行します。  

![pytorch_install](./images/pytorch_gpu.png)

```shell
pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu124
```

## 6. PyTorchがGPUを認識していることの確認

下のコマンドを実行して、次のような結果が得られれば認識しています。

```shell
import torch
print("torch: ", torch.__version__)
print('available: ', torch.cuda.is_available())
print('device_name: ', torch.cuda.get_device_name())
print('device_count: ', torch.cuda.device_count())
print('current_device: ', torch.cuda.current_device())
print('device_capability: ', torch.cuda.get_device_capability())
```

```shell
torch:  2.4.0+cu124
available:  True
device_name:  NVIDIA T1000
device_count:  1
current_device:  0
device_capability:  (7, 5)
```

## 7. CPUとGPUの切り替え

GPUが使用可能な時はGPUを使用するように設定しておきます。

```python
# 環境に応じてGPU/CPUを切り替えるには、torch.cuda.is_available()を使用する。
device = torch.device('cuda:0') if torch.cuda.is_available() else torch.device('cpu')

t = torch.tensor([0.1, 0.2], device=device)

net = torch.nn.Linear(2, 2)
net.to(device)

print(net(t))
```
