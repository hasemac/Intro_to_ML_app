# ワインの分類

深層学習モデルを用いて、１３の特徴量を持つワインのテストデータからワインを分類してみます。最後に、コードを実行した後、tensorboardを起動して、結果の確認をします。

分類の例の手順を示します。

1. モジュールのインポート
   特に、tensorflowのモジュールをインポートしておきます。

   ```python
   import tensorflow as tf
   ```

2. 各パラメータの定義
   バッチサイズ'batch_size'や、学習率'learning_rate'など、機械学習で用いるパラメータを一か所にまとめて書いておくと、パラメータの編集がしやすくなります。  

   ```python
   # 各パラメータの定義
   random_seed = 1
   test_size = 0.7
   num_epochs = 500
   batch_size = 10
   learning_rate = 0.001
   patience = 7
   dt_start = datetime.datetime.now().strftime("%y%m%d-%H%M%S")
   log_dir = f"logs/{dt_start}"
   ```

   <br>

3. 学習結果に再現性を持たせるためのランダムシード設定
   機械学習では、例えば、データを訓練用とテスト用に分割する際に乱数を使用します。  
   ランダムシードの設定をしておくと、乱数は生成されますが、その順序が固定されます。これに伴い学習結果も固定され、学習結果に再現性が得られるようになります。  
   当然、結果に再現性が必要ないときは、指定する必要も在りません。  
   tensorflowでランダムシードを固定する際は少々複雑です。こんなものだと思っておけば良いでしょう。  

   ```python
   # 学習の再現を行うために乱数シードを固定するためのコード
   import os
   import random as rn
   from tensorflow.compat.v1.keras import backend as K

   os.environ['PYTHONHASHSEED'] = '0'
   np.random.seed(0)
   rn.seed(0)

   session_conf = tf.compat.v1.ConfigProto(
      intra_op_parallelism_threads=1,
      inter_op_parallelism_threads=1
   )

   tf.compat.v1.set_random_seed(0)
   sess = tf.compat.v1.Session(graph=tf.compat.v1.get_default_graph(), config=session_conf)
   K.set_session(sess)
   ```

   <br>

4. データセットの取得と訓練、テストデータへの分割
   wineのデータセットを取得して、訓練データとテストデータに分割します。  

   ```python
   # データセットの取得と訓練、テストデータへの分割

   # wine
   wine = datasets.load_wine() # wineのデータを辞書型で取得
   # 辞書型の変数からDataFrameを作成
   # df = pd.DataFrame(data = wine['data'], columns=wine['feature_names'])
   # df['target'] = wine['target']
   X, y = wine['data'], wine['target']
   X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=random_seed)
   ```

   <br>

5. 訓練データの標準化
   各特徴量のスケールが異なっているので標準化をします。  
   テストデータの標準化には、訓練データの統計量を用いることに注意してください。  

   ```python
   # 訓練データの標準化

   # 訓練データの統計量(平均と標準偏差)を保持しておく
   SSx = StandardScaler()
   SSx.fit(X_train) 
   # 入力データの標準化, テストデータの標準化は訓練データの統計量を用いる。
   X_train_ss = SSx.transform(X_train)
   X_test_ss = SSx.transform(X_test)
   ```

   <br>

6. ニューラルネットワークのモデル作成
   ニューラルネットワークの層数、それぞれの層のノード数、使用する活性化関数を定義しています。
   ワインのデータセットは13個の特徴量をもっているので、入力層は13個のノードを持つことになります。また3種類のワインを分類しているので、出力層は3個のノードとしています。
   隠れ層のノード数は16として、隠れ層の活性化関数をReLUにしています。分類問題なので、出力層の活性化関数はソフトマックスです。
   入力層(ノード数１３個) ⇒ 隠れ層(ノード数１６個, ReLU) ⇒ 出力層(ノード数３個, ソフトマックス)

   ```python
   # ニューラルネットワークのモデル作成
   input_size = len(X_train[0]) # 特徴量の個数
   hidden_size = 16
   output_size = len(set(y_train))

   model = keras.Sequential()
   model.add(keras.layers.Dense(hidden_size, activation=tf.nn.relu, input_shape=[input_size])) # ReLUの活性化関数、hidden_size個の出力
   model.add(keras.layers.Dense(output_size, activation=tf.nn.softmax)) # softmaxの活性化関数、output_size個の出力
   ```

   <br>

7. modelのコンパイル
   損失関数、最適化方法、評価方法を指定してモデルのコンパイルをします。ワインの種類である教師データのターゲット変数は0, 1, 2と整数で与えられているので、ワンホットベクトル（(0,0,1)や(0,1,0)の形式）で与える必要のある`CategoricalCrossentropy()`でなく、整数で与えることのできる`SparseCategoricalCrossentropy()`を選択します。

   ```python
   # modelのコンパイル

   # 損失関数
   # https://www.tensorflow.org/api_docs/python/tf/keras/losses
   # loss_fnc = keras.losses.CategoricalCrossentropy() # 正解ラベル：ワンホットベクトル
   # loss_fnc = keras.losses.SparseCategoricalCrossentropy() # 正解ラベル：整数
   loss_fnc='sparse_categorical_crossentropy'

   # 最適化方法
   # https://www.tensorflow.org/api_docs/python/tf/keras/optimizers
   optimizer = keras.optimizers.Adam(learning_rate=learning_rate) # 関数で指定も可能
   #optimizer = 'adam' # 文字列で指定も可能

   # 評価方法
   # https://www.tensorflow.org/api_docs/python/tf/keras/metrics
   # metrics = [keras.metrics.SparseCategoricalAccuracy()] # 関数で指定も可能
   # 文字列'accuracy'にすると適切なmetricsが自動で選択される。
   metrics = ['accuracy'] # 文字列で指定も可能

   model.compile(
      loss = loss_fnc,
      optimizer = optimizer,
      metrics=metrics
   )
   ```

   <br>

8. コールバック関数の定義
   コールバック関数とは、関数（子関数）自体が別の関数（親関数）の引数として与えられ、親関数側から子関数を呼び出すことができるような関数を指します。ここでは、早期終了を判定するコールバック関数と、tensorboardに出力するためのコールバック関数を定義しています。  

   ```python
   # コールバック関数の定義

   # 早期終了の定義
   cb_early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', patience=patience)
   # tensorboard用の定義
   cb_tensorboard = keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
   ```

   <br>

9. モデルの学習
   検証データに使うデータの割合`validation_split`、バッチサイズ`batch_size`などを指定して、学習を実施します。学習をした際の出力を示します。  

   ```python
   # モデルの学習
   history = model.fit(
      x = X_train_ss, 
      y = y_train,
      batch_size = batch_size,
      epochs = num_epochs,
      validation_split = 0.2, # trainデータを更にこの割合で分割。検証に用いる。
      #callbacks = [cb_tensorboard], # early stop無効
      callbacks = [cb_early_stop, cb_tensorboard], # early stop有効
      verbose = 1,
      )
   ```

```python
Epoch 101/500
5/5 [==============================] - 0s 24ms/step - loss: 0.0463 - accuracy: 1.0000 - val_loss: 0.3320 - val_accuracy: 0.9091
Epoch 102/500
5/5 [==============================] - 0s 26ms/step - loss: 0.0454 - accuracy: 1.0000 - val_loss: 0.3314 - val_accuracy: 0.9091
Epoch 103/500
5/5 [==============================] - 0s 25ms/step - loss: 0.0445 - accuracy: 1.0000 - val_loss: 0.3309 - val_accuracy: 0.9091
Epoch 104/500
5/5 [==============================] - 0s 25ms/step - loss: 0.0437 - accuracy: 1.0000 - val_loss: 0.3315 - val_accuracy: 0.9091
Epoch 105/500
5/5 [==============================] - 0s 26ms/step - loss: 0.0428 - accuracy: 1.0000 - val_loss: 0.3317 - val_accuracy: 0.9091
Epoch 106/500
5/5 [==============================] - 0s 24ms/step - loss: 0.0420 - accuracy: 1.0000 - val_loss: 0.3318 - val_accuracy: 0.9091
Epoch 107/500
5/5 [==============================] - 0s 26ms/step - loss: 0.0412 - accuracy: 1.0000 - val_loss: 0.3314 - val_accuracy: 0.8182
Epoch 108/500
5/5 [==============================] - 0s 25ms/step - loss: 0.0404 - accuracy: 1.0000 - val_loss: 0.3313 - val_accuracy: 0.8182
Epoch 109/500
5/5 [==============================] - 0s 25ms/step - loss: 0.0396 - accuracy: 1.0000 - val_loss: 0.3312 - val_accuracy: 0.8182
Epoch 110/500
5/5 [==============================] - 0s 24ms/step - loss: 0.0389 - accuracy: 1.0000 - val_loss: 0.3319 - val_accuracy: 0.8182
```

`epochs`を500に設定していましたが、110回目でEarlyStopがはたらき学習が終了したことが分かります。これは103回目のエポックの検証データに対する損失`val_loss`でもっとも小さい値`0.3309`になっていますが、それ以降の計算で`patience`で指定された回数`7`に達しても改善されなかったために終了したことが分かります。  
<br>

---

最後に`tensorboard`で結果の確認をします。
ターミナルを起動して、所定のディレクトリに移動した後、起動コマンドを入力します。  
具体的なディレクトリ名などは各自、読み違えてください。  

```shell
PS C:\home\lecture\Intro_to_ML_app> cd .\70_Keras\10_classification_wine\
PS C:\home\lecture\Intro_to_ML_app\70_Keras\10_classification_wine> tensorboard --logdir ./logs/
```

下のようなリンクが表示されたら、そのリンクにWebブラウザでアクセスします。
(VS codeの場合は、Ctrlキーを押しながらクリック)

```shell
I0530 10:47:14.655237 20996 plugin.py:429] Monitor runs begin
Serving TensorBoard on localhost; to expose to the network, use a proxy or pass --bind_all
TensorBoard 2.15.2 at http://localhost:6006/ (Press CTRL+C to quit)
```

![tensorboard](./images/TensorBoard.png)

学習が進んでいく過程や、損失の履歴などが閲覧できます。  
