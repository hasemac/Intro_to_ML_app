# Wine Classification

Using a deep learning model, we will try to classify wines from a dataset of 13 wine features. Finally, after running the code, we will launch tensorboard to check the results.

Here is an example procedure for classification.

1. Importing Modules
   In particular, import the tensorflow module.

   ```python
   import tensorflow as tf
   ```

2. Definition of each parameter
   It is easier to edit parameters such as batch size and learning rate if they are written in one place.  

   ```python
   # 各パラメータの定義
   random_seed = 1
   test_size = 0.7
   num_epochs = 500
   batch_size = 10
   learning_rate = 0.001
   patience = 7
   dt_start = datetime.datetime.now().strftime("%y%m%d-%H%M%S")
   log_dir = f"logs/{dt_start}"
   ```

   <br>

3. Random seed setting for reproducibility of training results
   In machine learning, for example, random numbers are used to split data into training and testing.  
   With the random seed setting, random numbers are generated, but in a fixed order. This in turn fixes the training results and makes the training results reproducible.  
   Of course, if the results are not reproducible, there is no need to specify it.  
   Fixing a random seed in tensorflow is a bit more complicated. It is best to think of it as something like this.  

   ```python
   # 学習の再現を行うために乱数シードを固定するためのコード
   import os
   import random as rn
   from tensorflow.compat.v1.keras import backend as K

   os.environ['PYTHONHASHSEED'] = '0'
   np.random.seed(0)
   rn.seed(0)

   session_conf = tf.compat.v1.ConfigProto(
      intra_op_parallelism_threads=1,
      inter_op_parallelism_threads=1
   )

   tf.compat.v1.set_random_seed(0)
   sess = tf.compat.v1.Session(graph=tf.compat.v1.get_default_graph(), config=session_conf)
   K.set_session(sess)
   ```

   <br>

4. Acquisition of data sets and splitting into training and test data
   Take the WINE data set and split it into training and test data.  

   ```python
   # データセットの取得と訓練、テストデータへの分割

   # wine
   wine = datasets.load_wine() # wineのデータを辞書型で取得
   # 辞書型の変数からDataFrameを作成
   # df = pd.DataFrame(data = wine['data'], columns=wine['feature_names'])
   # df['target'] = wine['target']
   X, y = wine['data'], wine['target']
   X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=random_seed)
   ```

   <br>

5. Standardization of training data
   Standardize each feature since they are scaled differently.  
   Note that the statistics of the training data are used to standardize the test data.  

   ```python
   # 訓練データの標準化

   # 訓練データの統計量(平均と標準偏差)を保持しておく
   SSx = StandardScaler()
   SSx.fit(X_train) 
   # 入力データの標準化, テストデータの標準化は訓練データの統計量を用いる。
   X_train_ss = SSx.transform(X_train)
   X_test_ss = SSx.transform(X_test)
   ```

   <br>

6. Creating a model for a neural network
   It defines the number of layers of the neural network, the number of nodes in each layer, and the activation function to be used.
   The wine dataset has 13 features, so the input layer will have 13 nodes. Also, since we are classifying 3 types of wine, the output layer will have 3 nodes.
   The number of nodes in the hidden layer is assumed to be 16 and the activation function of the hidden layer is ReLU. Since this is a classification problem, the activation function of the output layer is softmax.
   Input layer (13 nodes) ⇒ Hidden layer (16 nodes, ReLU) ⇒ Output layer (3 nodes, Softmax)

   ```python
   # ニューラルネットワークのモデル作成
   input_size = len(X_train[0]) # 特徴量の個数
   hidden_size = 16
   output_size = len(set(y_train))

   model = keras.Sequential()
   model.add(keras.layers.Dense(hidden_size, activation=tf.nn.relu, input_shape=[input_size])) # ReLUの活性化関数、hidden_size個の出力
   model.add(keras.layers.Dense(output_size, activation=tf.nn.softmax)) # softmaxの活性化関数、output_size個の出力
   ```

   <br>

7. Compiling the model
   Compile the model by specifying the loss function, optimization method, and evaluation method. Since the target variables of the wine type, the teacher data, are given as integers (0, 1, 2), instead of `CategoricalCrossentropy()`, which must be given as one-hot vectors (in the form (0,0,1) or (0,1,0)), we use `SparseCategoricalCrossentropy()`, which can be given as an integer.

   ```python
   # modelのコンパイル

   # 損失関数
   # https://www.tensorflow.org/api_docs/python/tf/keras/losses
   # loss_fnc = keras.losses.CategoricalCrossentropy() # 正解ラベル：ワンホットベクトル
   # loss_fnc = keras.losses.SparseCategoricalCrossentropy() # 正解ラベル：整数
   loss_fnc='sparse_categorical_crossentropy'

   # 最適化方法
   # https://www.tensorflow.org/api_docs/python/tf/keras/optimizers
   optimizer = keras.optimizers.Adam(learning_rate=learning_rate) # 関数で指定も可能
   #optimizer = 'adam' # 文字列で指定も可能

   # 評価方法
   # https://www.tensorflow.org/api_docs/python/tf/keras/metrics
   # metrics = [keras.metrics.SparseCategoricalAccuracy()] # 関数で指定も可能
   # 文字列'accuracy'にすると適切なmetricsが自動で選択される。
   metrics = ['accuracy'] # 文字列で指定も可能

   model.compile(
      loss = loss_fnc,
      optimizer = optimizer,
      metrics=metrics
   )
   ```

   <br>

8. Callback Function Definitions
   A callback function is a function in which the function (child function) itself is given as an argument to another function (parent function) and the child function can be called from the parent function side. Here, we define a callback function to determine early stopping and a callback function to output to the tensorboard.  

   ```python
   # コールバック関数の定義

   # 早期終了の定義
   cb_early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', patience=patience)
   # tensorboard用の定義
   cb_tensorboard = keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
   ```

   <br>

9. Model Learning
   The training is performed by specifying the percentage of data to be used for validation data `validation_split`, batch size `batch_size`, etc. The following is the output of the training.  

   ```python
   # モデルの学習
   history = model.fit(
      x = X_train_ss, 
      y = y_train,
      batch_size = batch_size,
      epochs = num_epochs,
      validation_split = 0.2, # trainデータを更にこの割合で分割。検証に用いる。
      #callbacks = [cb_tensorboard], # early stop無効
      callbacks = [cb_early_stop, cb_tensorboard], # early stop有効
      verbose = 1,
      )
   ```

```python
Epoch 101/500
5/5 [==============================] - 0s 24ms/step - loss: 0.0463 - accuracy: 1.0000 - val_loss: 0.3320 - val_accuracy: 0.9091
Epoch 102/500
5/5 [==============================] - 0s 26ms/step - loss: 0.0454 - accuracy: 1.0000 - val_loss: 0.3314 - val_accuracy: 0.9091
Epoch 103/500
5/5 [==============================] - 0s 25ms/step - loss: 0.0445 - accuracy: 1.0000 - val_loss: 0.3309 - val_accuracy: 0.9091
Epoch 104/500
5/5 [==============================] - 0s 25ms/step - loss: 0.0437 - accuracy: 1.0000 - val_loss: 0.3315 - val_accuracy: 0.9091
Epoch 105/500
5/5 [==============================] - 0s 26ms/step - loss: 0.0428 - accuracy: 1.0000 - val_loss: 0.3317 - val_accuracy: 0.9091
Epoch 106/500
5/5 [==============================] - 0s 24ms/step - loss: 0.0420 - accuracy: 1.0000 - val_loss: 0.3318 - val_accuracy: 0.9091
Epoch 107/500
5/5 [==============================] - 0s 26ms/step - loss: 0.0412 - accuracy: 1.0000 - val_loss: 0.3314 - val_accuracy: 0.8182
Epoch 108/500
5/5 [==============================] - 0s 25ms/step - loss: 0.0404 - accuracy: 1.0000 - val_loss: 0.3313 - val_accuracy: 0.8182
Epoch 109/500
5/5 [==============================] - 0s 25ms/step - loss: 0.0396 - accuracy: 1.0000 - val_loss: 0.3312 - val_accuracy: 0.8182
Epoch 110/500
5/5 [==============================] - 0s 24ms/step - loss: 0.0389 - accuracy: 1.0000 - val_loss: 0.3319 - val_accuracy: 0.8182
```

The `epochs` was set to 500, but you can see that the EarlyStop was triggered at the 110th epoch and the training was terminated. This is because the loss `val_loss` for the validation data for the 103rd epoch was the smallest value `0.3309`, but the subsequent calculations did not improve after reaching the number `7` specified in `patience`, so the training ended.  
<br>

---

Finally, check the results with `tensorboard`.
Start a terminal, go to the given directory, and then enter the startup command.  
The specific directory name, etc. should be appropriate for each person.  

```shell
PS C:\home\lecture\Intro_to_ML_app> cd .\70_Keras\10_classification_wine\
PS C:\home\lecture\Intro_to_ML_app\70_Keras\10_classification_wine> tensorboard --logdir ./logs/
```

When you see a link like the one below, access that link with your web browser.
(For VS code, hold down the Ctrl key while clicking)

```shell
I0530 10:47:14.655237 20996 plugin.py:429] Monitor runs begin
Serving TensorBoard on localhost; to expose to the network, use a proxy or pass --bind_all
TensorBoard 2.15.2 at http://localhost:6006/ (Press CTRL+C to quit)
```

![tensorboard](./images/TensorBoard.png)

You can view the process of learning as it progresses and the history of losses.  
